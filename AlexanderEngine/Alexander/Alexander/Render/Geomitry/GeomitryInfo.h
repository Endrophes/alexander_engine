#pragma once

#ifndef GEOMITRYINFO_H
#define GEOMITRYINFO_H

#include <ExportHeader.h>
#include <gl\glew.h>

namespace Render
{

	class ENGINE_SHARED GeomitryInfo
	{
	public:
		GeomitryInfo();
		~GeomitryInfo();

		GLuint VertexArrayObjectID;
		GLuint vertexDataByteOffSet;
		GLuint IndecesDataByteOffSet;
		GLuint bufferID;
		unsigned int vertexBufferSize;
		unsigned int indexBufferSize;
		unsigned int numIndices;
		unsigned int numVerts;
		void* vertsData;
		void* indicesData;

	};

}

#endif


#pragma once

#ifndef IMAGEINFO_H
#define IMAGEINFO_H

namespace Render
{

	class imageInfo
	{
	public:
		int height;
		int width;
		unsigned char* bits;
	};

}

#endif
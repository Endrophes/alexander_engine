#pragma once

#ifndef BUFFERINFO_H
#define BUFFERINFO_H

#include <ExportHeader.h>
#include <gl\glew.h>

namespace Render
{
	class ENGINE_SHARED BufferInfo
	{
	public:
		//Max size 1 mb
		GLuint bufferID;
		GLuint remaingSpace;
		GLsizeiptr offSet;

		BufferInfo();

		~BufferInfo();
	};
}

#endif

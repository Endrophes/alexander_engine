#pragma once

#ifndef VERTEX_H
#define VERTEX_H

#include <ExportHeader.h>
#include <glm.hpp>

namespace Render
{
	class ENGINE_SHARED Vertex
	{
		glm::vec3 position;
		glm::vec3 normal;
		glm::vec2 uv;
	};
}

#endif
#pragma once

#ifndef VERTEX_TB_H
#define VERTEX_TB_H

#include <ExportHeader.h>
#include <glm.hpp>

namespace Render
{
	class ENGINE_SHARED VertexTB
	{
		glm::vec3 position;
		glm::vec3 normal;
		glm::vec2 uv;
		glm::vec3 tanget;
		glm::vec3 bitanget;
	};
}

#endif
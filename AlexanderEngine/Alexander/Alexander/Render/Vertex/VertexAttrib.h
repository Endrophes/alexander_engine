#pragma once

#ifndef VERTEX_ATTRIB_H
#define VERTEX_ATTRIB_H

#include <ExportHeader.h>
#include <gl\glew.h>
#include <glm.hpp>

namespace Render
{
	class ENGINE_SHARED VertexAttrib
	{
		GLint size;
		GLenum type;
		GLboolean normalize;
		GLsizei stride;
		unsigned int offset;
	};
}

#endif
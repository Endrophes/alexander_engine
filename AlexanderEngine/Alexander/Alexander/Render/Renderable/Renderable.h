#pragma once

#ifndef RENDERABLE_H
#define RENDERABLE_H

#include <ExportHeader.h>
#include <gl\glew.h>
#include <glm.hpp>

namespace Render
{
	class GeomitryInfo;
	class shaderInfo;

	class ENGINE_SHARED Renderable
	{
	public:
		Renderable();
		~Renderable();

		GeomitryInfo* gemomitry;
		shaderInfo* shadder;
		GLenum DrawMode;

		glm::mat4 modelToWorldMatrix;
		glm::mat4 scaleMatrix;

		glm::mat4 rotationMatrix;
		glm::mat4 translationMatrix;

		glm::vec3 direction;
		glm::vec3 Position;
		glm::vec3 scale;

		float alpha;

		int texture;
		int normalMap;
		int alphaMap;
		int layer;

		bool visable;
	};
}

#endif

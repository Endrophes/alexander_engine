#pragma once

#ifndef RENDERER_H
#define RENDERER_H

#include <ExportHeader.h>
#include <glm.hpp>
#include <map>

#include <Render\Buffer\BufferInfo.h>
#include <Render\Geomitry\GeomitryInfo.h>
#include <Render\Renderable\Renderable.h>
#include <Render\Shader\ShaderInfo.h>
#include <Render\Texture\imageInfo.h>
#include <Render\Texture\Texture.h>
#include <Render\Vertex\Vertex.h>
#include <Render\Vertex\VertexAttrib.h>
#include <Render\Vertex\VertexTB.h>

namespace Render
{

	class ENGINE_SHARED Renderer
	{

		std::map<std::string, Renderable> renderableStorage;
		std::map<std::string, GeomitryInfo> geomitryStorage;
		std::map<std::string, shaderInfo> shaderStorage;
		std::map<GLuint, BufferInfo> BufferStorage;

	public:
		Renderer();
		~Renderer();

		GeomitryInfo loadShape(char* filePath, int numVertexAttrib, Render::VertexAttrib bufferData[]);

	};


}

#endif

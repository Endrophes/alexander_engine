#pragma once

#ifndef UNIFORM_H
#define UNIFORM_H

#include <sstream>

namespace uni
{

	template <class T>
	class Uniform
	{
	public:
		int location;
		std::string nameInUniform;
		T value;
	};

}
#endif
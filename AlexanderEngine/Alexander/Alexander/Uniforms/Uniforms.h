#pragma once

#ifndef UNIFORMS_H
#define UNIFORMS_H

#include <Uniforms\Uniform.h>
#include <glm.hpp>

using glm::vec2;
using glm::vec3;
using glm::vec4;
using glm::mat3;
using glm::mat4;

namespace uni
{

	class FloatUniform : public Uniform < float >
	{
	public:
	};

	class IntUniform : public Uniform < int >
	{
	public:
	};

	class Vec2Uniform : public Uniform < vec2 >
	{
	public:
	};

	class Vec3Uniform : public Uniform < vec3 >
	{
	public:
	};

	class Vec4Uniform : public Uniform < vec4 >
	{
	public:
	};

	class Mat3Uniform : public Uniform < mat3 >
	{
	public:
		bool traspose;
	};

	class Mat4Uniform : public Uniform < mat4 >
	{
	public:
		bool traspose;
	};

}
#endif
#pragma once

#ifndef UNIFORMHANDLER_H
#define UNIFORMHANDLER_H

#include <ExportHeader.h>
#include <glm.hpp>
#include <GL\glew.h>
#include <gtx\transform.hpp>
#include <sstream>
#include <Uniforms\Uniforms.h>
#include <map>

using glm::vec2;
using glm::vec3;
using glm::vec4;
using glm::mat3;
using glm::mat4;

namespace uni
{

	class ENGINE_SHARED UniformHandler
	{

		static UniformHandler instance;

		std::map < std::string, FloatUniform >  FloatUniformHolder;
		std::map < std::string, IntUniform   >  IntUniformHolder;
		std::map < std::string, Vec2Uniform  >  Vec2UniformHolder;
		std::map < std::string, Vec3Uniform  >  Vec3UniformHolder;
		std::map < std::string, Vec4Uniform  >  Vec4UniformHolder;
		std::map < std::string, Mat3Uniform  >  Mat3UniformHolder;
		std::map < std::string, Mat4Uniform  >  Mat4UniformHolder;

		//std::map < std::string, Vec4Uniform  >  UniformHolder;

	public:
		UniformHandler();
		~UniformHandler();

		void addFloatUniform(std::string locationName);
		void addIntUniform(std::string locationName);
		void addVec2Uniform(std::string locationName);
		void addVec3Uniform(std::string locationName);
		void addVec4Uniform(std::string locationName);
		void addMat3Uniform(std::string locationName, bool trasposeMatrix = false);
		void addMat4Uniform(std::string locationName, bool trasposeMatrix = false);


		void setFloatValue(std::string name, float value);
		void setIntValue(std::string name, int value);
		void setVec2Value(std::string name, vec2 value);
		void serVec3Value(std::string name, vec3 value);
		void setVec4Value(std::string name, vec4 value);
		void setMat3Value(std::string name, mat3 value, bool trasposeMatrix = false);
		void setMat4Value(std::string name, mat4 value, bool trasposeMatrix = false);

		void getUniformLocations(GLuint programID);
		void sendUniforms(GLuint programID);

		static UniformHandler& getInstance();
	};

#define UniformManager uni::UniformHandler::getInstance()
}


#endif
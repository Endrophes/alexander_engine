#include <Uniforms\UniformManager\UniformHandler.h>

namespace uni
{
	void UniformHandler::addFloatUniform(std::string locationName)
	{
		FloatUniform temp;
		temp.nameInUniform = locationName;
		temp.value = 0.0f;
		temp.location = -1;

		FloatUniformHolder[locationName] = (temp);
	}

	void UniformHandler::addIntUniform(std::string locationName)
	{
		IntUniform temp;
		temp.nameInUniform = locationName;
		temp.value = 0;
		temp.location = -1;

		IntUniformHolder[locationName] = (temp);
	}
	
	void UniformHandler::addVec2Uniform(std::string locationName)
	{
		Vec2Uniform temp;
		temp.nameInUniform = locationName;
		temp.value = vec2(0.0f, 0.0f);
		temp.location = -1;

		Vec2UniformHolder[locationName] = (temp);
	}
	
	void UniformHandler::addVec3Uniform(std::string locationName)
	{
		Uniform<Vec3Uniform> test;

		Vec3Uniform temp;
		temp.nameInUniform = locationName;
		temp.value = vec3(0.0f, 0.0f, 0.0f);
		temp.location = -1;

		Vec3UniformHolder[locationName] = (temp);
	}
	
	void UniformHandler::addVec4Uniform(std::string locationName)
	{
		Vec4Uniform temp;
		temp.nameInUniform = locationName;
		temp.value = vec4(0.0f, 0.0f, 0.0f, 0.0f);
		temp.location = -1;

		Vec4UniformHolder[locationName] = (temp);
	}
	
	void UniformHandler::addMat3Uniform(std::string locationName, bool trasposeMatrix = false)
	{
		Mat3Uniform temp;
		temp.nameInUniform = locationName;
		temp.value = mat3();
		temp.location = -1;
		temp.traspose = trasposeMatrix;

		Mat3UniformHolder[locationName] = (temp);
	}
	
	void UniformHandler::addMat4Uniform(std::string locationName, bool trasposeMatrix = false)
	{
		Mat4Uniform temp;
		temp.nameInUniform = locationName;
		temp.value = mat4();
		temp.location = -1;
		temp.traspose = trasposeMatrix;

		Mat4UniformHolder[locationName] = (temp);
	}

	void UniformHandler::setFloatValue(std::string name, float value)
	{
		FloatUniform* temp = &FloatUniformHolder[name];
		
		if (temp != NULL)
		{
			temp->value = value;
		}
		else
		{
			//Error
		}
		
	}

	void UniformHandler::setIntValue(std::string name, int value)
	{
		IntUniform* temp = &IntUniformHolder[name];

		if (temp != NULL)
		{
			temp->value = value;
		}
		else
		{
			//Error
		}
	}

	void UniformHandler::setVec2Value(std::string name, vec2 value)
	{
		Vec2Uniform* temp = &Vec2UniformHolder[name];

		if (temp != NULL)
		{
			temp->value = value;
		}
		else
		{
			//Error
		}
	}

	void UniformHandler::serVec3Value(std::string name, vec3 value)
	{
		Vec3Uniform* temp = &Vec3UniformHolder[name];

		if (temp != NULL)
		{
			temp->value = value;
		}
		else
		{
			//Error
		}
	}

	void UniformHandler::setVec4Value(std::string name, vec4 value)
	{
		Vec4Uniform* temp = &Vec4UniformHolder[name];

		if (temp != NULL)
		{
			temp->value = value;
		}
		else
		{
			//Error
		}
	}

	void UniformHandler::setMat3Value(std::string name, mat3 value, bool trasposeMatrix = false)
	{
		Mat3Uniform* temp = &Mat3UniformHolder[name];

		if (temp != NULL)
		{
			temp->value = value;
		}
		else
		{
			//Error
		}
	}

	void UniformHandler::setMat4Value(std::string name, mat4 value, bool trasposeMatrix = false)
	{
		Mat4Uniform* temp = &Mat4UniformHolder[name];

		if (temp != NULL)
		{
			temp->value = value;
		}
		else
		{
			//Error
		}
	}

	#define FloatHolderIterator std::map < std::string, FloatUniform >::iterator
	#define IntHolderIterator   std::map < std::string, IntUniform   >::iterator
	#define Vec2HolderIterator  std::map < std::string, Vec2Uniform  >::iterator
	#define Vec3HolderIterator  std::map < std::string, Vec3Uniform  >::iterator
	#define Vec4tHolderIterator std::map < std::string, Vec4Uniform  >::iterator
	#define Mat3HolderIterator  std::map < std::string, Mat3Uniform  >::iterator
	#define Mat4HolderIterator  std::map < std::string, Mat4Uniform  >::iterator

	#define getLocationsOnMap(iterator, map , ID) for(iterator steper = map.begin(); steper != map.end(); steper++){steper->second.location = glGetUniformLocation(ID, steper->second.nameInUniform.c_str());}

	void UniformHandler::getUniformLocations(GLuint programID)
	{
		//// V 1
		//for (FloatHolderIterator FloatSteper = FloatUniformHolder.begin(); FloatSteper != FloatUniformHolder.end(); FloatSteper++)
		//{
		//	FloatUniform* currentFloatUniform = &FloatSteper->second;
		//	currentFloatUniform->location = glGetUniformLocation(programID, currentFloatUniform->nameInUniform.c_str());
		//}

		// V 2
		getLocationsOnMap(FloatHolderIterator, FloatUniformHolder, programID);
		getLocationsOnMap(IntHolderIterator, IntUniformHolder, programID);
		getLocationsOnMap(Vec2HolderIterator,  Vec2UniformHolder, programID);
		getLocationsOnMap(Vec3HolderIterator,  Vec3UniformHolder, programID);
		getLocationsOnMap(Vec4tHolderIterator, Vec4UniformHolder, programID);
		getLocationsOnMap(Mat3HolderIterator, Mat3UniformHolder, programID);
		getLocationsOnMap(Mat4HolderIterator, Mat4UniformHolder, programID);
	}

	#define forLoopMap(iterator, map) for(iterator steper = map.begin(); steper != map.end(); steper++)

	void UniformHandler::sendUniforms(GLuint programID)
	{
		getUniformLocations(programID);

		forLoopMap(FloatHolderIterator, FloatUniformHolder)
		{
			glUniform1f(steper->second.location, steper->second.value);
		}

		forLoopMap(IntHolderIterator, IntUniformHolder)
		{
			glUniform1i(steper->second.location, steper->second.value);
		}

		forLoopMap(Vec2HolderIterator, Vec2UniformHolder)
		{
			glUniform2fv(steper->second.location, 1, &steper->second.value[0]);
		}

		forLoopMap(Vec3HolderIterator, Vec3UniformHolder)
		{
			glUniform3fv(steper->second.location, 1, &steper->second.value[0]);
		}

		forLoopMap(Vec4tHolderIterator, Vec4UniformHolder)
		{
			glUniform4fv(steper->second.location, 1, &steper->second.value[0]);
		}

		forLoopMap(Mat3HolderIterator, Mat3UniformHolder)
		{
			glUniformMatrix3fv(steper->second.location, 1, steper->second.traspose, &steper->second.value[0][0]);
		}

		forLoopMap(Mat4HolderIterator, Mat4UniformHolder)
		{
			glUniformMatrix4fv(steper->second.location, 1, steper->second.traspose, &steper->second.value[0][0]);
		}
	}

	UniformHandler& UniformHandler::getInstance()
	{
		return instance;
	}

}
#pragma once

#ifndef ENGINE_MOVECAM_H
#define ENGINE_MOVECAM_H

#include <Camera\Camera.h>


class ENGINE_SHARED MoveCam : public Camera
{
	friend class Camera;

	glm::vec2 oldMousePosition;
	glm::vec3 moveDirection;
	float MOVEMENT_SPEED;
	bool cameraMove;

public:
	MoveCam(void);
	~MoveCam(void);

	void moveForward();
	void moveBack();
	void moveLeft();
	void moveRight();
	void moveUp();
	void moveDown();

	void setMoveSpeed(float newMoveSpeed);

	void mouseUpdate(const glm::vec2& newMousePosition);
};

#endif
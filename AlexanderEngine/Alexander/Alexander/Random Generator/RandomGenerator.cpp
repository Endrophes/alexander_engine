#include <Random Generator\RandomGenerator.h>

const float TWO_PIE = (float)(2 * 3.14159265);

RandomGenerator instance;

inline int RandomGenerator::randomInt(){
	return rand();
}

inline double RandomGenerator::randomDouble(){
	return ((double)rand()) / RAND_MAX;
}

inline float RandomGenerator::randomFloat(){
	return (float)rand() / RAND_MAX;
}
	
inline int RandomGenerator::randomIntInRange(int min, int max){
	return (rand()%(max - min) + min);
}

inline double RandomGenerator::randomDoubleInRange(double min, double max){
	return (randomDouble()*(max - min) + min);
}

inline float RandomGenerator::randomFloatInRange(float min, float max){
	return (randomFloat()*(max - min) + min);
}

inline vec2 RandomGenerator::randomVector2(){
	float angle = randomFloat()*TWO_PIE;
	vec2 V(cos(angle), sin(angle));
	return V;
}

inline vec2 RandomGenerator::randomVector2InRange(vec2 min, vec2 max)
{
	return vec2(
		randomIntInRange(min.x, max.x), 
		randomIntInRange(min.y, max.y)
		);
}

inline vec3 RandomGenerator::randomVector3InRange(vec3 min, vec3 max)
{
	return vec3(
		randomIntInRange(min.x, max.x), 
		randomIntInRange(min.y, max.y), 
		randomIntInRange(min.z, max.z)
		);
}

inline vec4 RandomGenerator::randomVector4InRange(vec4 min, vec4 max)
{
	return vec4(
		randomIntInRange(min.x, max.x), 
		randomIntInRange(min.y, max.y), 
		randomIntInRange(min.z, max.z), 
		randomIntInRange(min.a, max.a)
		);
}

inline RGB RandomGenerator::randomColor(){
	RGB colorGenerated = RGB(0,0,0);

	int Red   = randomIntInRange(0,255);
	int Green = randomIntInRange(0,255);
	int Blue  = randomIntInRange(0,255);

	colorGenerated = RGB(Red, Green, Blue);

	return colorGenerated;
}

inline RGB RandomGenerator::randomColorInRange(int minRed, int maxRed, int minGreen, int maxGreen, int minBlue, int maxBlue){
	RGB colorGenerated = RGB(0,0,0);

	int Red   = randomIntInRange(minRed,maxRed);
	int Green = randomIntInRange(minGreen,maxGreen);
	int Blue  = randomIntInRange(minBlue,maxBlue);

	colorGenerated = RGB(Red, Green, Blue);

	return colorGenerated;

}

RandomGenerator* RandomGenerator::getInstance()
{
	return &instance;
}
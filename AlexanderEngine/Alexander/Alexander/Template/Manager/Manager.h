#pragma once

#ifndef MANAGER_H
#define MANAGER_H

#include <map>

template <class T>
class Manager
{

	std::map<int, T> storage;

public:
	Manager();
	~Manager();


	void addIteam(T, int key);
	void removeIteam(T, int key);
};


#endif
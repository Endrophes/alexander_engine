#version 400

in vec3 theNormal;
in vec4 thePosition;
in vec2 vs_tex_UV;
in vec3 theTanget;
in vec3 theBiTanget;
in vec3 modelPosition;

out vec4 deColor;

uniform samplerCube textureSelection;
uniform sampler2D alphaSelection;
uniform sampler2D normalSelection;

uniform float materialSpec;
uniform float scale;
uniform float octiveLevel3;
uniform float threshHold;

uniform vec3 specularColor;
uniform vec3 lightPosition;
uniform vec3 ambientLight;
uniform vec3 colorObjects;
uniform vec3 colorLightBulb;
uniform vec3 cameraPosition;

uniform mat4 projectionMatrix;
uniform mat4 translation;
uniform mat4 rotation;
uniform mat4 view;

uniform bool showlight;

void main()
{

	vec4 alpha = texture(alphaSelection,vs_tex_UV);
	vec4 color = vec4(1,1,1,1);

	vec3 LightToPoint = (lightPosition) - vec3(thePosition);
	vec3 nomalizedLightToPoint = normalize(LightToPoint);
	float diffuseAngle = dot(nomalizedLightToPoint, normalize(theNormal));

	vec3 LightReflected = -reflect(nomalizedLightToPoint, normalize(theNormal));
	
	vec3 eyeVector = cameraPosition - vec3(thePosition);

	vec3 normalizedEyeVector = normalize(eyeVector);

	float angleSpec = clamp(dot(LightReflected, normalizedEyeVector), 0 , 1);
	float spec = pow(angleSpec, materialSpec);

	vec3 worldView = normalize(vec3(cameraPosition - vec3(thePosition)));
	vec3 reflectColor = refract(-worldView, normalize(theNormal), 0.66f);


	float target = 0.0;

	if(octiveLevel3 >= 1 && octiveLevel3 < 2)
	{
		target = (alpha.x);
	}
	else if(octiveLevel3 >= 2 && octiveLevel3 < 3)
	{
		target = (alpha.y);
	}
	else if(octiveLevel3 >= 3 && octiveLevel3 < 4)
	{
		target = (alpha.z);
	}
	else if(octiveLevel3 >= 4 && octiveLevel3 < 5)
	{
		target = (alpha.w);
	}

	if(target < (threshHold / 255))
	{
		color = texture(textureSelection, reflectColor);
	}
	else
	{
		color = texture(normalSelection,vs_tex_UV);
	}
	
	if(showlight)
	{
		deColor = color * vec4(((diffuseAngle * colorLightBulb) + ambientLight) * vec3(colorObjects) + (spec * specularColor), 1.0);
	}
	else
	{
		deColor = color;
	}
}

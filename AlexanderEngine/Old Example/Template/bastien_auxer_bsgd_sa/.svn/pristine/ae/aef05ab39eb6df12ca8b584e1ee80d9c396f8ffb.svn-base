#version 400
in vec3 theNormal;
in vec4 thePosition;
in vec4 theColor;
in vec2 vs_tex_UV;
in vec3 theTanget;
in vec3 theBiTanget;

out vec4 deColor;

uniform sampler2D tex;

uniform float materialSpec;
uniform float scale;

uniform vec3 specularColor;
uniform vec3 lightPosition;
uniform vec3 ambientLight;
uniform vec3 colorObjects;
uniform vec3 colorLightBulb;
uniform vec3 cameraPosition;

uniform mat4 modelToWorld;
uniform mat4 projectionMatrix;
uniform mat4 translation;
uniform mat4 rotation;
uniform mat4 view;

uniform bool showlight;

void main()
{
	vec3 texNormal = ((2.0 * vec3(texture(tex,vs_tex_UV))) - vec3(1.0,1.0,1.0));
	mat3 tangBitang;
	tangBitang[0] = theTanget;
	tangBitang[1] = theBiTanget;
	tangBitang[2] = theNormal;

	vec3 finalNormal = vec3(normalize(rotation * vec4(tangBitang * texNormal, 0.0)));


	vec3 LightToPoint = (lightPosition) - vec3(thePosition);
	vec3 nomalizedLightToPoint = normalize(LightToPoint);
	float diffuseAngle = clamp(dot(nomalizedLightToPoint, normalize(finalNormal)), 0.0, 1.0);

	vec3 LightReflected = -reflect(nomalizedLightToPoint, normalize(finalNormal));
	
	vec3 eyeVector = cameraPosition - vec3(thePosition);
	vec3 normalizedEyeVector = normalize(eyeVector);

	float angleSpec = clamp(dot(LightReflected, normalizedEyeVector), 0.0 , 1.0);
	float spec = pow(angleSpec, materialSpec);


	if(showlight)
	{
		deColor = vec4(1.0,1.0,1.0,1.0) * vec4(((diffuseAngle) + ambientLight) * normalize(vec3(colorObjects)) + (0.1 * specularColor), 1.0);
	}
	else
	{
		deColor = texture(tex,vs_tex_UV);
	}
}

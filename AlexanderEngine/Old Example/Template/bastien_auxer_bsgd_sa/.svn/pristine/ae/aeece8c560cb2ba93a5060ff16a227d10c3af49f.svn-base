#pragma once

#include <GL\glew.h>
#include "GLWindow.h"

#include <Qt\qdebug.h>
#include <gtx\transform.hpp>
#include <fstream>
#include <string>
#include <iostream>
#include <cstdlib>
#include <QtGui\QMouseEvent>
#include <QtGui\QKeyEvent>
#include <QtGui\QImage>
#include <QtGui\qimage.h>
#include <QtGui\QFileDialog>

#include <ShapeData.h>
#include <ShapeGenerator.h>
#include "Sliders\DebugSlider.h"
#include "Camera\Camera.h"
#include "Widget\meWidget.h"
#include <Debug Tools\DebugShapes\DebugShapes.h>

//#include "Debug Tools\DebugShapes\DebugShapes.h"

#ifdef PROFILING_ON
	#include "Debug Tools\Profiling\Profiler.h"
	#include "Debug Tools\Profiling\Profile.h"
#endif

#define ARRAY_SIZE(a) sizeof(a)/sizeof(*a)

using namespace std;
using Neumont::ShapeGenerator;
using Neumont::ShapeData;
using Neumont::Vertex;

int mouseX, mouseY, theHeight, theWidth;

DebugShapes debugShapes;

bool nodeplaced;
bool nodelinked;


void GLWindow::initializeGL()
{
	glewInit();
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glEnable(GL_BLEND);

	setMouseTracking(true);
	//change backGroundColor
	//glClearColor(0.0f,1.0f,0.0f,1.0f);

	glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
	int testWidth = width();
	int testHeight= height();

	renderer.setUpRender(testWidth, testHeight);
	libnoise = new LibNoiseManager(256,256);


	//Needed for set up
	//debugShapes.setupDebug(&renderer);
	//Place them anywhere
	//debugShapes.AddCube(vec3(0,0,0),vec4(0.0f,0.0f,1.0f,1.0f), vec3(0,0,0), 1000.0f, true);
	//debugShapes.AddSphere(vec3(5.0f,0.0f,0.0f),vec4(0.0f,1.0f,0.0f,1.0f), vec3(0.0f,0.0f,0.0f), 1000.0f, true);
	//debugShapes.AddCross(2.0f,vec3(0,0,2),1000.0f,true);
	//debugShapes.AddLine(5,vec3(1,1,1),vec3(1,1,4),vec4(0,1,0,1),1000.0f,true);
	//debugShapes.AddVector(5,vec3(1,1,1),vec3(2,2,4),vec4(0,1,0,1),1000.0f,true);

	//This is temp
	renderer.ambientLight = vec3(0.3f,0.3f,0.3f);
	renderer.colorObjects = vec3(0.3f,0.3f,0.3f);
	renderer.colorLightBulb = vec3(0.3f,0.3f,0.3f);
	renderer.specularColor = vec3(0.3f,0.3f,0.3f);
	renderer.specularMateral = 25;
	renderer.showlight = false;

	DisplayLight = false;
	showPlane = false;
	showCube = false;

	showOger = false;
	OgerNormal = false;
	OgerAmbient = false;
	OgerDiffuse = false;

	xRotation = 0.0f;
	yRotation = 0.0f;
	zRotation = 0.0f;

	sendDataToHardware();

}

void GLWindow::createShadders()
{
	/*
	normalShaders    = renderer.createShaderInfo("ShaderCode\\VertexShaderCode.glsl", "ShaderCode\\FragmentShaderCode.glsl");
	colorRemoveShaders = renderer.createShaderInfo("ShaderCode\\VertexShaderCode.glsl", "ShaderCode\\removeSpecalFragmentsSC.rfsc");
	*/
	PassThroughShaders = renderer.createShaderInfo("ShaderCode\\passThroughVertexShader.LOL", "ShaderCode\\passThroughFragmentShader.LOL");
	TransparentShaders = renderer.createShaderInfo("ShaderCode\\VertexShaderCode.glsl", "ShaderCode\\TransparentSpecalFragmentsSC.rfsc");
	WhiteNormalMap = renderer.createShaderInfo("ShaderCode\\VertexShaderCode.glsl", "ShaderCode\\WhiteNormalMap.fsc");
	JamieShader = renderer.createShaderInfo("ShaderCode\\jamieShape.vsc", "ShaderCode\\JamieShape.fsc");
	obj2shader = renderer.createShaderInfo("ShaderCode\\objConverted.vsc", "ShaderCode\\objConverted.fsc");
	nosieShader = renderer.createShaderInfo("ShaderCode\\VertexShaderCode.glsl", "ShaderCode\\noise.fsc");
}

void GLWindow::loadTextures()
{

	//Oger 8-9 ogre_normalmap
	renderer.addTexture("images\\Oger\\diffuse.png", "PNG");
	renderer.addTexture("images\\Oger\\ao_ears.png", "PNG");
	renderer.addTexture("images\\Oger\\ogre_normalmap.png", "PNG");
	renderer.addTexture("images\\MH.png", "PNG");
	renderer.addTexture("images\\grid.bmp");
	renderer.addTexture("images\\BlueWindowTransparent.png", "PNG");
	renderer.addTexture("images\\BlueWall.png", "PNG");
	renderer.addTexture("images\\DMText.png", "PNG");
	renderer.addTexture("images\\Base.png", "PNG");
	int test = renderer.addTexture("NormalMap\\Shapes.png", "PNG");
	GLuint testnum = libnoise->createNoiseMap(256,256,&renderer);

}

void GLWindow::createUniforms()
{
	/*SpecularMateral = renderer.uniformHandiler.addFloatUniform("materialSpec");
	alpha = renderer.uniformHandiler.addFloatUniform("alpha");*/

	//add bool* function
	Showlight = renderer.uniformHandiler.addIntUniform("showlight");
	OgerNormalcolor  = renderer.uniformHandiler.addIntUniform("showOgerNornmal");
	OgerAmbintlight  = renderer.uniformHandiler.addIntUniform("showOgerAmbint");
	OgerDiffusecolor = renderer.uniformHandiler.addIntUniform("showOgerDiffuse");
	
	AmbientLight = renderer.uniformHandiler.addVec3Uniform("ambientLight");
	ColorObjects = renderer.uniformHandiler.addVec3Uniform("colorObjects");
	ColorLightBulb = renderer.uniformHandiler.addVec3Uniform("colorLightBulb");
	SpecularColor = renderer.uniformHandiler.addVec3Uniform("specularColor");
	LightPosition = renderer.uniformHandiler.addVec3Uniform("lightPosition");

	ProjectionMatrix = renderer.uniformHandiler.addMat4Uniform("projectionMatrix");
	TransformMatix = renderer.uniformHandiler.addMat4Uniform("transfom");

	CameraPosition = renderer.uniformHandiler.addVec3Uniform("cameraPosition");
	CamraVeiw = renderer.uniformHandiler.addMat4Uniform("view");

	SpecularMateral = renderer.uniformHandiler.addFloatUniform("materialSpec");
	alpha = renderer.uniformHandiler.addFloatUniform("alpha");

	glm::vec3 white = glm::vec3(1.0f,1.0f,1.0f);
	*SpecularColor = white;
	*ColorObjects = white;
	*SpecularMateral = 0.5f;
	*alpha = 0.5f;
	
}

void GLWindow::loadShapes()
{
	plane = renderer.loadShape("bin\\Plane.bin");
	//renderer.addGeometryData(&plane);

	box = renderer.loadShape("bin\\Box.bin");
	//renderer.addGeometryData(&box);

	shpere = renderer.loadShape("bin\\shpere.bin");
	//renderer.addGeometryData(&shpere);

	doomsDay = renderer.loadShape("bin\\Doomsday.bin");
	//renderer.addGeometryData(&doomsDay);

	base = renderer.loadShape("bin\\Base.bin");
	//renderer.addGeometryData(&base);

	cubeGeo = renderer.loadShape("bin\\cube.bin");
	//renderer.addGeometryData(&cubeGeo);

	int numAtrib = 5;
	GLsizei strideB = (4 * sizeof(vec3)) + sizeof(vec2);
	Render::VertexAttrib newAtrib[] = 
	{
		//Position
		3,
		GL_FLOAT,
		GL_FALSE,
		strideB,
		0,

		//Normal
		3,
		GL_FLOAT,
		GL_FALSE,
		strideB,
		(sizeof(glm::vec3)),

		//UV
		2,
		GL_FLOAT,
		GL_FALSE,
		strideB,
		(2 * sizeof(glm::vec3)),

		//Tanget
		3,
		GL_FLOAT,
		GL_FALSE,
		strideB,
		(2 * sizeof(glm::vec3)) + sizeof(glm::vec2),

		//BiTanget
		3,
		GL_FLOAT,
		GL_FALSE,
		strideB,
		(3 * sizeof(glm::vec3)) + sizeof(glm::vec2)
	};
	testBase = renderer.loadShape("bin\\Ogre.ba", numAtrib, newAtrib);
}

void GLWindow::createRenderables()
{
	//
	/////DoomsDay
	//renderer.createRenderable(&plane,&TransparentShaders,glm::vec3(0.0f,1.0f,3.0f),3.0f,90.0f,0.0f,180.0f,GL_TRIANGLES,0,0,5);
	//renderer.createRenderable(&shpere,&TransparentShaders,glm::vec3(0.0f,1.0f,0.0f),1.5f,0.0f,0.0f,0.0f,GL_TRIANGLES,4,0.5f);
	//renderer.createRenderable(&doomsDay,&TransparentShaders,glm::vec3(0.0f,1.0f,0.0f),1.0f,0.0f,145.0f,20.0f,GL_TRIANGLES,6);
	//renderer.createRenderable(&base,&TransparentShaders,glm::vec3(0.0f,-1.0f,0.0f),2.0f,0.0f,-90.0f,0.0f,GL_TRIANGLES,7);

	flatNormPlane = renderer.createRenderable(&plane,&WhiteNormalMap,glm::vec3(0.0f,0.0f,0.0f),1.0f,90.0f,0.0f,0.0f,GL_TRIANGLES,0,0,9);
	cube = renderer.createRenderable(&cubeGeo,&PassThroughShaders,glm::vec3(0.0f,0.0f,3.0f),0.5f,0.0f,0.0f,0.0f,GL_TRIANGLES,0,0,0);
	whiteCube = renderer.createRenderable(&cubeGeo,&WhiteNormalMap,glm::vec3(0.0f,0.0f,0.0f),1.0f,0.0f,0.0f,0.0f,GL_TRIANGLES,0,0,9);
	Jaime = renderer.createRenderable(&jamieCude,&JamieShader,glm::vec3(0.0f,0.0f,0.0f),1.0f,0.0f, 0.0f, 0.0f,GL_TRIANGLES,0,0,9);

	baseRen = renderer.createRenderable(&testBase,&obj2shader,glm::vec3(0.0f,0.0f,0.0f),1.0f,0.0f, 0.0f, 0.0f,GL_TRIANGLES,2,1,0);

	//nosiePlane = renderer.createRenderable(&plane,&nosieShader,glm::vec3(0.0f,0.0f,0.0f),1.0f,90.0f,0.0f,0.0f,GL_TRIANGLES,0,0,10);
}



void GLWindow::sendDataToHardware(){
	cout<<"Sending data to hardware"<<endl;
	
	jamieCude = makeCube();

	createShadders();
	loadShapes();
	loadTextures();
	createUniforms();
	createRenderables();

	*AmbientLight = vec3(0.0f,0.0f,0.0f);
	*LightPosition = vec3(0.0f,0.0f,3.0f);
}

void GLWindow::paintGL()
{
	glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
	int testWidth = width();
	int testHeight= height();

	renderer.setWidthAndHeight(testWidth, testHeight);

	*ProjectionMatrix = glm::perspective(60.0f, ((float)testWidth)/testHeight, 0.1f, 200.0f);
	*CameraPosition = renderer.camera.getPosition();
	*CamraVeiw = renderer.camera.getWorldToViewMatrix();
	*Showlight = (int)DisplayLight;

	*OgerNormalcolor = OgerNormal;
	*OgerAmbintlight  = OgerAmbient;
	*OgerDiffusecolor = OgerDiffuse;

	debugShapes.updateDebug();

	renderer.drawRenderables();

	//renderer.drawRenderable(nosiePlane);
}

void GLWindow::mouseMoveEvent(QMouseEvent* e)
{
	mouseX = e->x();
	mouseY = e->y();
	theWidth = width();
	theHeight = height();

	if(GetAsyncKeyState(VK_RBUTTON))
	{
		renderer.camera.mouseUpdate(glm::vec2(mouseX, mouseY));
	}
}

void GLWindow::keyPressEvent(QKeyEvent*e)
{
	e;
}

void GLWindow::keyCheck()
{
	float moveLightBy = 0.5f;
	if(GetAsyncKeyState('G')){
		//cout << "LEFT LEFT!!!" << endl;
		LightPosition->x += moveLightBy;
	};
	if(GetAsyncKeyState('J')){
		//cout << "RIGHT RIGHT!!!" << endl;
		LightPosition->x -= moveLightBy;
	};
	
	if(GetAsyncKeyState('H')){
		//cout << "UP UP!!!" << endl;
		LightPosition->z -= moveLightBy;
	};
	if(GetAsyncKeyState('Y')){
		//cout << "DOWN DOWN!!!" << endl;
		LightPosition->z  += moveLightBy;
	};
	if(GetAsyncKeyState('I')){
		//cout << "UP UP!!!" << endl;
		LightPosition->y += moveLightBy;
	};
	if(GetAsyncKeyState('K')){
		//cout << "DOWN DOWN!!!" << endl;
		LightPosition->y  -= moveLightBy;
	};

	//if(GetAsyncKeyState('L'))
	//{
	//	//cout << "LET THERE BE LIGHT!!!" << endl;
	//	renderer.showlight = true;
	//}
	//if(GetAsyncKeyState('O'))
	//{
	//	//cout << "Black Hole" << endl;
	//	renderer.showlight = false;
	//}

	if(GetAsyncKeyState(VK_ESCAPE)){
		//cout << "DIE DIE!!!" << endl;
		glUseProgram(0);
		
		#ifdef PROFILING_ON
			profiler.shutDown();
		#endif

		exit(0);
	};

	//if(GetAsyncKeyState(VK_LCONTROL)){
	//	cout << "camera Position: " << renderer.cameraPosition.x  << " " << renderer.cameraPosition.y  << " " << renderer.cameraPosition.z << " " << endl;
	//	cout << "camera View Direction: " << renderer.camera.viewDirection.x  << "f, " << renderer.camera.viewDirection.y  << "f, " << renderer.camera.viewDirection.z << "f " << endl;
	//};

	if(GetAsyncKeyState('A')){
		//cout << "Right!!!" << endl;
		renderer.camera.moveRight();
	};
	if(GetAsyncKeyState('D')){
		//cout << "Left!!!" << endl;
		renderer.camera.moveLeft();
	};
	
	if(GetAsyncKeyState('W')){
		//cout << "Forward!!!" << endl;
		renderer.camera.moveForward();
	};
	if(GetAsyncKeyState('S')){
		//cout << "Back!!!" << endl;
		renderer.camera.moveBack();
	};
	if(GetAsyncKeyState('R')){
		//cout << "UP!!!" << endl;
		renderer.camera.moveUp();
	};
	if(GetAsyncKeyState('F')){
		//cout << "Down!!!" << endl;
		renderer.camera.moveDown();
	};

	
	flatNormPlane->visable = showPlane;
	whiteCube->visable = showCube;


	cube->Position = *LightPosition;
	cube->translationMatrix = glm::translate(*LightPosition);
	cube->modelToWorldMatrix = cube->translationMatrix * cube->rotationMatrix * cube->scaleMatrix;

}

void GLWindow::update()
{
	float moveLightBy = 0.2f;
	keyCheck();

	glm::mat4 newRotation;
	newRotation  = glm::rotate(xRotation, vec3(1.0,0,0));
	newRotation *= glm::rotate(yRotation, vec3(0,1.0,0));
	newRotation *= glm::rotate(zRotation, vec3(0,0,1.0));

	Jaime->rotationMatrix = newRotation;
	Jaime->visable = showCube;

	baseRen->visable = showOger;

	glm::mat4 newModelToWorldMatrix =  Jaime->translationMatrix * Jaime->rotationMatrix * Jaime->scaleMatrix;

	paintGL();
	swapBuffers();
}

GLWindow::~GLWindow()
{
	glUseProgram(0);
}

Render::GeomitryInfo GLWindow::copyToShapeData(Vertex Verts[], int vertArraySize, unsigned short Indices[], int indexArraySize)
{
	Vertex* vertData = new Vertex[vertArraySize];
	unsigned short* indexData = new unsigned short[indexArraySize];

	vertData = Verts;
	indexData = Indices;

	uint vertBufferSize = vertArraySize * sizeof(Vertex);
	uint indexBuffersize = indexArraySize * sizeof(unsigned short);

	int numOfAtributes = 6;

	GLsizei stride = ( 3 * sizeof(vec3)) + sizeof(vec2) + sizeof(vec4);
	GLsizei strideB = ( 4 * sizeof(vec3)) + sizeof(vec2) + sizeof(vec4);

	Render::VertexAttrib jaimeCudeAtrib[] = 
	{
		//Position
		3,
		GL_FLOAT,
		GL_FALSE,
		strideB,
		0,

		//Color
		4,
		GL_FLOAT,
		GL_FALSE,
		strideB,
		sizeof(glm::vec3),

		//Normal
		3,
		GL_FLOAT,
		GL_FALSE,
		strideB,
		(sizeof(glm::vec3)) + sizeof(glm::vec4),

		//UV
		2,
		GL_FLOAT,
		GL_FALSE,
		strideB,
		(2 * sizeof(glm::vec3)) + sizeof(glm::vec4),

		//Tanget
		3,
		GL_FLOAT,
		GL_FALSE,
		strideB,
		(2 * sizeof(glm::vec3)) + sizeof(glm::vec4) + sizeof(glm::vec2),

		//BiTanget
		3,
		GL_FLOAT,
		GL_FALSE,
		strideB,
		(3 * sizeof(glm::vec3)) + sizeof(glm::vec4) + sizeof(glm::vec2)
	};

	Render::GeomitryInfo debug = renderer.createGeomitry(
		numOfAtributes, jaimeCudeAtrib, vertBufferSize, indexBuffersize, indexArraySize, vertData, indexData
		);

	return debug;
}

Render::GeomitryInfo GLWindow::makeCube()
{
using glm::vec2;
using glm::vec3;
using glm::vec4;
	Vertex stackVerts[] = 
	{
		// Top
		vec3(-1.0f, +1.0f, +1.0f), // 0
		vec4(+1.0f, +0.0f, +0.0f, +1.0f), // Color
		vec3(+0.0f, +1.0f, +0.0f), // Normal
		vec2(+0.0f, +1.0f), // UV
		vec3(+1.0f, +0.0f, +0.0f), // Tanget
		vec3(+0.0f, +0.0f, +1.0f), // BiTanget

		vec3(+1.0f, +1.0f, +1.0f), // 1
		vec4(+0.0f, +1.0f, +0.0f, +1.0f), // Color
		vec3(+0.0f, +1.0f, +0.0f), // Normal
		vec2(+1.0f, +1.0f), // UV
		vec3(+1.0f, +0.0f, +0.0f), // Tanget
		vec3(+0.0f, +0.0f, +1.0f), // BiTanget

		vec3(+1.0f, +1.0f, -1.0f), // 2
		vec4(+0.0f, +0.0f, +1.0f, +1.0f), // Color
		vec3(+0.0f, +1.0f, +0.0f), // Normal
		vec2(+1.0f, +0.0f), // UV
		vec3(+1.0f, +0.0f, +0.0f), // Tanget
		vec3(+0.0f, +0.0f, +1.0f), // BiTanget

		vec3(-1.0f, +1.0f, -1.0f), // 3
		vec4(+1.0f, +1.0f, +1.0f, +1.0f), // Color
		vec3(+0.0f, +1.0f, +0.0f), // Normal
		vec2(+0.0f, +0.0f), // UV
		vec3(+1.0f, +0.0f, +0.0f), // Tanget
		vec3(+0.0f, +0.0f, +1.0f), // BiTanget

		// Back
		vec3(-1.0f, +1.0f, -1.0f), // 4
		vec4(+1.0f, +0.0f, +1.0f, +1.0f), // Color
		vec3(+0.0f, +0.0f, -1.0f), // Normal
		vec2(+0.0f, +1.0f), // UV
		vec3(+1.0f, +0.0f, +0.0f), // Tanget
		vec3(+0.0f, +1.0f, 0.0f), // BiTanget

		vec3(+1.0f, +1.0f, -1.0f), // 5
		vec4(+0.0f, +0.5f, +0.2f, +1.0f), // Color
		vec3(+0.0f, +0.0f, -1.0f), // Normal
		vec2(+1.0f, +1.0f), // UV
		vec3(+1.0f, +0.0f, +0.0f), // Tanget
		vec3(+0.0f, +1.0f, 0.0f), // BiTanget

		vec3(+1.0f, -1.0f, -1.0f), // 6
		vec4(+0.8f, +0.6f, +0.4f, +1.0f), // Color
		vec3(+0.0f, +0.0f, -1.0f), // Normal
		vec2(+1.0f, +0.0f), // UV
		vec3(+1.0f, +0.0f, +0.0f), // Tanget
		vec3(+0.0f, +1.0f, +0.0f), // BiTanget

		vec3(-1.0f, -1.0f, -1.0f), // 7
		vec4(+0.3f, +1.0f, +0.5f, +1.0f), // Color
		vec3(+0.0f, +0.0f, -1.0f), // Normal
		vec2(+0.0f, +0.0f), // UV
		vec3(+1.0f, +0.0f, +0.0f), // Tanget
		vec3(+0.0f, +1.0f, +0.0f), // BiTanget

		// Right
		vec3(+1.0f, +1.0f, -1.0f), // 8
		vec4(+0.2f, +0.5f, +0.2f, +1.0f), // Color
		vec3(+1.0f, +0.0f, +0.0f), // Normal
		vec2(+1.0f, +0.0f), // UV
		vec3(+0.0f, +0.0f, -1.0f), // Tanget
		vec3(+0.0f, -1.0f, +0.0f), // BiTanget

		vec3(+1.0f, +1.0f, +1.0f), // 9
		vec4(+0.9f, +0.3f, +0.7f, +1.0f), // Color
		vec3(+1.0f, +0.0f, -1.0f), // Normal
		vec2(+0.0f, +0.0f), // UV
		vec3(+0.0f, +0.0f, -1.0f), // Tanget
		vec3(+0.0f, -1.0f, +0.0f), // BiTanget

		vec3(+1.0f, -1.0f, +1.0f), // 10
		vec4(+0.3f, +0.7f, +0.5f, +1.0f), // Color
		vec3(+1.0f, +0.0f, +0.0f), // Normal
		vec2(+0.0f, +1.0f), // UV
		vec3(+0.0f, +0.0f, -1.0f), // Tanget
		vec3(+0.0f, -1.0f, +0.0f), // BiTanget

		vec3(+1.0f, -1.0f, -1.0f), // 11
		vec4(+0.5f, +0.7f, +0.5f, +1.0f), // Color
		vec3(+1.0f, +0.0f, +0.0f), // Normal
		vec2(+1.0f, +1.0f), // UV
		vec3(+0.0f, +0.0f, -1.0f), // Tanget
		vec3(+0.0f, -1.0f, +0.0f), // BiTanget

		// Left
		vec3(-1.0f, +1.0f, +1.0f), // 12
		vec4(+0.7f, +0.8f, +0.2f, +1.0f), // Color
		vec3(-1.0f, +0.0f, +0.0f), // Normal
		vec2(+1.0f, +0.0f), // UV
		vec3(+0.0f, +0.0f, +1.0f), // Tanget
		vec3(+0.0f, -1.0f, +0.0f), // BiTanget

		vec3(-1.0f, +1.0f, -1.0f), // 13
		vec4(+0.5f, +0.7f, +0.3f, +1.0f), // Color
		vec3(-1.0f, +0.0f, +0.0f), // Normal
		vec2(+0.0f, +0.0f), // UV
		vec3(+0.0f, +0.0f, +1.0f), // Tanget
		vec3(+0.0f, -1.0f, +0.0f), // BiTanget

		vec3(-1.0f, -1.0f, -1.0f), // 14
		vec4(+0.4f, +0.7f, +0.7f, +1.0f), // Color
		vec3(-1.0f, +0.0f, +0.0f), // Normal
		vec2(+0.0f, +1.0f), // UV
		vec3(+0.0f, +0.0f, +1.0f), // Tanget
		vec3(+0.0f, -1.0f, +0.0f), // BiTanget

		vec3(-1.0f, -1.0f, +1.0f), // 15
		vec4(+0.2f, +0.5f, +1.0f, +1.0f), // Color
		vec3(-1.0f, +0.0f, +0.0f), // Normal
		vec2(+1.0f, +1.0f), // UV
		vec3(+0.0f, +0.0f, +1.0f), // Tanget
		vec3(+0.0f, -1.0f, +0.0f), // BiTanget

		// Front
		vec3(+1.0f, +1.0f, +1.0f), // 16
		vec4(+0.6f, +1.0f, +0.7f, +1.0f), // Color
		vec3(+0.0f, +0.0f, +1.0f), // Normal
		vec2(+1.0f, +0.0f), // UV
		vec3(+1.0f, +0.0f, +0.0f), // Tanget
		vec3(+0.0f, -1.0f, +0.0f), // BiTanget

		vec3(-1.0f, +1.0f, +1.0f), // 17
		vec4(+0.6f, +0.4f, +0.8f, +1.0f), // Color
		vec3(+0.0f, +0.0f, +1.0f), // Normal
		vec2(+0.0f, +0.0f), // UV
		vec3(+1.0f, +0.0f, +0.0f), // Tanget
		vec3(+0.0f, -1.0f, +0.0f), // BiTanget

		vec3(-1.0f, -1.0f, +1.0f), // 18
		vec4(+0.2f, +0.8f, +0.7f, +1.0f), // Color
		vec3(+0.0f, +0.0f, +1.0f), // Normal
		vec2(+0.0f, +1.0f), // UV
		vec3(+1.0f, +0.0f, +0.0f), // Tanget
		vec3(+0.0f, -1.0f, +0.0f), // BiTanget

		vec3(+1.0f, -1.0f, +1.0f), // 19
		vec4(+0.2f, +0.7f, +1.0f, +1.0f), // Color
		vec3(+0.0f, +0.0f, +1.0f), // Normal
		vec2(+1.0f, +1.0f), // UV
		vec3(+1.0f, +0.0f, +0.0f), // Tanget
		vec3(+0.0f, -1.0f, +0.0f), // BiTanget

		// Bottom
		vec3(+1.0f, -1.0f, -1.0f), // 20
		vec4(+0.8f, +0.3f, +0.7f, +1.0f), // Color
		vec3(+0.0f, -1.0f, +0.0f), // Normal
		vec2(+1.0f, +1.0f), // UV
		vec3(+1.0f, +0.0f, +0.0f), // Tanget
		vec3(+0.0f, +0.0f, -1.0f), // BiTanget

		vec3(-1.0f, -1.0f, -1.0f), // 21
		vec4(+0.8f, +0.9f, +0.5f, +1.0f), // Color
		vec3(+0.0f, -1.0f, +0.0f), // Normal
		vec2(+0.0f, +1.0f), // UV
		vec3(+1.0f, +0.0f, +0.0f), // Tanget
		vec3(+0.0f, +0.0f, -1.0f), // BiTanget

		vec3(-1.0f, -1.0f, +1.0f), // 22
		vec4(+0.5f, +0.8f, +0.5f, +1.0f), // Color
		vec3(+0.0f, -1.0f, +0.0f), // Normal
		vec2(+0.0f, +0.0f), // UV
		vec3(+1.0f, +0.0f, +0.0f), // Tanget
		vec3(+0.0f, +0.0f, -1.0f), // BiTanget

		vec3(+1.0f, -1.0f, +1.0f), // 23
		vec4(+0.9f, +1.0f, +0.2f, +1.0f), // Color
		vec3(+0.0f, -1.0f, +0.0f), // Normal
		vec2(+1.0f, +0.0f), // UV
		vec3(+1.0f, +0.0f, +0.0f), // Tanget
		vec3(+0.0f, +0.0f, -1.0f), // BiTanget
	};
	//24

	unsigned short stackIndices[] = {
	0, 1, 2, 0, 2, 3, // Top
	4, 5, 6, 4, 6, 7, // Front
	8, 9, 10, 8, 10, 11, // Right 
	12, 13, 14, 12, 14, 15, // Left
	16, 17, 18, 16, 18, 19, // Back
	20, 22, 21, 20, 23, 22, // Bottom
	};
	//36

	int arraSizeVert = ARRAY_SIZE(stackVerts);
	int arraSizeIndex = ARRAY_SIZE(stackIndices);

	return GLWindow::copyToShapeData(stackVerts, arraSizeVert, stackIndices, arraSizeIndex);
}
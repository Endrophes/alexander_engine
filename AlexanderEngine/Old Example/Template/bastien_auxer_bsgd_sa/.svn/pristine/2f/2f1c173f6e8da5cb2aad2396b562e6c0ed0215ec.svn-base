#pragma once

#include <gl\glew.h>
#include <QtOpenGL\qglwidget>
#include <glm.hpp>
#include <gtx\transform.hpp>
#include <ShapeData.h>
#include <ShapeGenerator.h>

#include "..\Loader\FileLoader.h"
#include "..\UniformHandler\UniformHandler.h"
#include "..\Camera\Camera.h"
#include "..\ExportHeader.h"

#define NUMBER_OF_RENDERABLES 100
#define NUMBER_OF_TEXTURES 20
#define NUMBER_OF_GEOMITRYS 20
#define NUMBER_OF_SHADERS 20
#define NUMBER_OF_BUFFERINFOS 10
#define NUMBER_OF_UNIFORMS 20

using glm::vec2;
using glm::vec3;
using glm::vec4;
using glm::mat3;
using glm::mat4;


class ENGINE_SHARED Render
{
public:

	Render();
	~Render();

	enum ParameterType
	{
		// These values matter:
		PT_FLOAT = sizeof(float) * 1,
		PT_INT1  = sizeof(int)   * 1,
		PT_VEC2  = sizeof(float) * 2,
		PT_VEC3  = sizeof(float) * 3,
		PT_VEC4  = sizeof(float) * 4,
		PT_MAT3  = sizeof(float) * 9,
		PT_MAT4  = sizeof(float) * 16,
	};
	
	class shaderInfo
	{
	public:
		GLuint ProgramID;
	}shaderStorage[NUMBER_OF_SHADERS];

	class ENGINE_SHARED BufferInfo
	{
	public:
		//Max size 1 mb
		GLuint bufferID;
		GLuint remaingSpace;
		GLsizeiptr offSet;

		BufferInfo()
		{
			bufferID = 0;
			remaingSpace = 0;
			offSet = 0;
		}

	}BufferStorage[NUMBER_OF_BUFFERINFOS];

	class ENGINE_SHARED GeomitryInfo
	{
	public:
		GLuint VertexArrayObjectID;
		GLuint vertexDataByteOffSet;
		GLuint IndecesDataByteOffSet;
		GLuint bufferID;
		uint vertexBufferSize;
		uint indexBufferSize;
		uint numIndices;
		uint numVerts;
		void* vertsData;
		void* indicesData;
	}geomitryStorage[NUMBER_OF_GEOMITRYS];
	
	class ENGINE_SHARED Renderable
	{
	public:
		GeomitryInfo* gemomitry;
		shaderInfo* shadder;
		GLenum DrawMode;

		mat4 modelToWorldMatrix;
		mat4 scaleMatrix;

		mat4 rotationMatrix;
		mat4 translationMatrix;

		vec3 direction;
		vec3 Position;

		float scale;
		float alpha;
		int texture;
		int normalMap;
		int alphaMap;
		bool visable;

	}renderableStorage[NUMBER_OF_RENDERABLES];
	
	class ENGINE_SHARED VertexAttrib
	{
	public:
		GLint size;
		GLenum type;
		GLboolean normalize;
		GLsizei stride;
		uint offset;
	};

	class Texture
	{
	public:
		GLuint textureID;
	}textureStorage[NUMBER_OF_TEXTURES];

	class Vertex
	{
		glm::vec3 position;
		glm::vec3 normal;
		glm::vec2 uv;
	};

	class VertexTB
	{
		glm::vec3 position;
		glm::vec3 normal;
		glm::vec2 uv;
		glm::vec3 tanget;
		glm::vec3 bitanget;
	};

	class uniform
	{
	public:
		int location;
		ParameterType type;
		void* data;
	};

	UniformHandler uniformHandiler;

	FileLoader fileLoader;

	//Uniform pointers
	float* Scale;
	float* alpha;
	mat4*  TranslationMatrix;
	mat4*  RotationMatrix;
	mat4*  WorldTransform;
	mat4*  scaleMatrix;

	//Uniforms
	float specularMateral;
	float scale;

	bool  showlight;
		  
	vec3  ambientLight;
	vec3  colorObjects;
	vec3  colorLightBulb;
	vec3  specularColor;
	vec3  lightPosition;
	vec3  cameraPosition;
		  
	mat4  projectionMatrix;
	mat4  translationMatrix;
	mat4  rotationMatrix;
	mat4  transformMatix;
	mat4  camraVeiw;
	mat4  worldTransform;
	mat4  speculerMatrix;

	int translationLocation;
	int worldTransformationLocation;
	int scaleLocation;
	int rotationLocation;

	int* texture;
	int* normaltex;
	int* alphatex;

	shaderInfo normalShaders;    
	shaderInfo PassThroughShaders;
	uniform uniformStorage[NUMBER_OF_UNIFORMS];

	Camera camera;

	int currentTextureSlot;
	int currentGeomitrySlot;
	int currentShadderSlot;
	int currentRenderableSlot;
	int currentUniformSlot;
	int currentBufferStorageSlot;

	GLsizei width;
	GLsizei height;

	GeomitryInfo createGeomitry(
		uint vertexBufferSize,
		uint indexBufferSize,
		uint numIndices,
		void* vertsData,
		void* indicesData,
		bool withColor = false
	);

	GeomitryInfo createGeomitry(
		int numVertexAttrib, 
		VertexAttrib bufferData[],
		uint vertexBufferSize,
		uint indexBufferSize,
		uint numIndices,
		void* vertsData,
		void* indicesData
	);

	GeomitryInfo createGeomitry2(
		uint vertexBufferSize,
		uint indexBufferSize,
		uint numIndices,
		void* vertsData,
		void* indicesData,
		bool withColor = false,
		bool onlyTwoParts = false
	);

	GeomitryInfo loadShape(char* filePath);
	GeomitryInfo loadShape(char* filePath, int numVertexAttrib, Render::VertexAttrib bufferData[]);

	Renderable* createRenderable(GeomitryInfo* shape, shaderInfo* shaders, vec3 position, float size,
		float rotateX, float rotateY, float rotateZ, GLenum drawMode, int normalmMap = 0, int alphaMap = 0, int textOption = 2, float alpha = 1.0f);

	shaderInfo createShaderInfo(const char* vertexShadder, const char* fragmentShadder);

	GLuint compileShaders(const char* vertexShadder, const char* fragmentShadder);

	GLuint importImage(char* file, char* format = "bmp");

	GLuint addTexture(QImage imageFile);

	GLuint addTexture(GLvoid* textureBytes, uint width, uint height);

	GLuint addTexterInfoToBuffer(GLvoid* textureBytes, uint width, uint height);

	void addGeometryData(GeomitryInfo* shape, bool withColor = false, bool onlyTwoInfo = false);
	void addGeometryDataToBuffer(GeomitryInfo* shape, int numVertexAttrib, Render::VertexAttrib bufferData[]);

	void generateBuffer(BufferInfo* buffer);
	void generateBuffer(BufferInfo* buffer, GLsizeiptr size);

	void getUnifomLocations();
	void getUnifomLocations(GLuint programID);

	void addUnifrom(char* uniformName, GLuint programID , void* data, ParameterType valueType);
	void addUnifroms();
	
	void sendUniforms(Renderable* object);

	void addShapeDataWithColor2(BufferInfo* buffer, GeomitryInfo* shape);

	void addShapeDataWithColor(BufferInfo* buffer, GeomitryInfo* shape);
	void addShapeData(BufferInfo* buffer, GeomitryInfo* shape);
	void addShapeData(BufferInfo* buffer, GeomitryInfo* shape, int numVertexAttrib, VertexAttrib bufferData[]);
	void addSubdata(BufferInfo* buffer, GeomitryInfo* shape);


	int addTexture(char* file, char* format = "bmp");
	void setActiveTexture(int texture = 0);

	void update();

	void drawRenderable(Renderable* object);
	void drawRenderable2(Renderable* object);

	void drawGeomotry(GeomitryInfo* shape, shaderInfo* shaders, vec3 position, vec4 color, float size,
				 float rotateX, float rotateY, float rotateZ, int textOption = 0, bool solidShape = false);

	void drawRenderables();

	void setUpRender(GLsizei TheWidth, GLsizei TheHeight);

	void setWidthAndHeight(GLsizei TheWidth, GLsizei TheHeight);

	void reset();

	
};
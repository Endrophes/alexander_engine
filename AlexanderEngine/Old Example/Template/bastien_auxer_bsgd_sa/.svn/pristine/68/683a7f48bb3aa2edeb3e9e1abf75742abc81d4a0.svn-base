#version 400

in vec3 theNormal;
in vec4 thePosition;
in vec2 vs_tex_UV;
in vec3 testout;
out vec4 deColor;

uniform sampler2D tex;

uniform float materialSpec;
uniform float scale;

uniform vec3 specularColor;
uniform vec3 lightPosition;
uniform vec3 ambientLight;
uniform vec3 colorObjects;
uniform vec3 colorLightBulb;
uniform vec3 cameraPosition;

uniform mat4 projectionMatrix;
uniform mat4 translation;
uniform mat4 rotation;
uniform mat4 view;

uniform bool showlight;

void main()
{
	vec3 LightToPoint = (lightPosition) - vec3(thePosition);
	vec3 nomalizedLightToPoint = normalize(LightToPoint);
	float diffuseAngle = dot(nomalizedLightToPoint, normalize(theNormal));

	vec3 LightReflected = -reflect(nomalizedLightToPoint, normalize(theNormal));
	
	vec3 eyeVector = cameraPosition - vec3(thePosition);
	vec3 normalizedEyeVector = normalize(eyeVector);

	float angleSpec = clamp(dot(LightReflected, normalizedEyeVector), 0 , 1);
	float spec = pow(angleSpec, materialSpec);

	if(showlight)
	{
		deColor = texture(tex,vs_tex_UV) * vec4(((diffuseAngle * colorLightBulb) + ambientLight) * vec3(colorObjects) + (spec * specularColor), 1.0);
	}
	else
	{
		deColor = texture(tex,vs_tex_UV);
	}
}

#ifndef CONVERTER_SYSTEM
#define CONVERTER_SYSTEM

#include <vector>
#include <string>
#include <Utility\FileGrabber.h>

namespace Converter
{
	class ConverterSystem
	{

		
		std::string objConverterLocation;
		
		std::string inputDir;
		std::string outDir;

		std::string capString(std::string input);

		void copyFileOver(std::string file, std::string exitPath);

	private:
		//helpers
		std::string getFileExtention(std::string file);
		std::string getFileName(std::string file);
		std::string getfileLocation(std::string file);
		std::string getDriveLeter(std::string dir);
		std::string getSubDir(std::string dir);
		void writeToFile();

		void runSystemCommand(std::string command);

		//Main functions
		//void convertimages(std::string subDir);
		//void convertObjFiles(std::string subDir);
		//void convertMp3();

	public:
		//Main functions
		FileGrabber graber;

		void convertimages(std::string subDir);
		void convertObjFiles(std::string subDir);
		void convertAudioFiles(std::string subDir);
		void convertShaderFiles(std::string subDir);
		void makeDir(std::string dir);
		std::string getMacro(std::string macroName);

		void setRootInputDir(std::string inputRoot);
		void setRootOutputDir(std::string outputRoot);

		ConverterSystem();
		~ConverterSystem();

	};
}

#endif
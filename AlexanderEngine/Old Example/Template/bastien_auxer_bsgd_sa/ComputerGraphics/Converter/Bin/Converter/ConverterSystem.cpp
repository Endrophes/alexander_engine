#include <QtOpenGL\qglwidget>
#include <ConverterSystem.h>
#include <locale>         // std::locale, std::toupper
#include <direct.h>
#include <fstream>
#include <cstring>
#include <ios>
#include <assert.h>

#define uint unsigned int 

namespace Converter
{

	ConverterSystem::ConverterSystem()
	{
		objConverterLocation = "";
	}

	ConverterSystem::~ConverterSystem()
	{

	}

	void ConverterSystem::setRootInputDir(std::string inputRoot)
	{
		inputDir = inputRoot;
	}

	void ConverterSystem::setRootOutputDir(std::string outputRoot)
	{
		outDir = outputRoot;
	}

	std::string ConverterSystem::getFileExtention(std::string file)
	{
		std::string exstention = file.substr(file.find_last_of(".") + 1);

		return exstention;
	}

	std::string ConverterSystem::getFileName(std::string file)
	{
		std::string filename = file.substr(0,file.find_last_of("."));

		return filename;
	}

	void ConverterSystem::convertimages(std::string subDir)
	{
		std::vector<std::string> files;
		std::string folder = inputDir;
		folder.append(subDir);
		graber.getdir(folder,files);
		uint length = files.size();

		//std::vector<QImage> qImageFiles;

		for(uint step = 0; step < length; step++)
		{
			QImage image;
			
			std::string currentFile = files[step];

			std::string currentFileLocation = folder;
			currentFileLocation.append("\\");
			currentFileLocation.append(currentFile);

			std::locale loc;
			const char* file = currentFileLocation.c_str();
			const char* type = getFileExtention(currentFile).c_str(); //may have to make string uppercase

			if(!image.load(file, type))
			{
				std::cout<< "Image failed to load: "<< file << std::endl;
			}
			else
			{
				image = QGLWidget::convertToGLFormat(image);
				//qImageFiles.push_back(image);

				//Out
				std::string output = outDir;
				output.append("\\images\\");
				output.append(getFileName(currentFile) + ".QImageBin");
				std::ofstream file(output, std::ios::binary);
				//file.write((const char*)(image.constBits()), sizeof QImage);
				file.close();
			}
		}

	}

	void ConverterSystem::convertObjFiles(std::string subDir)
	{
		std::vector<std::string> files;

		std::string folder = inputDir;
		folder.append(subDir);

		graber.getdir(folder,files);
		uint length = files.size();

		for(uint step = 0; step < length; step++)
		{

			std::string currentFile = files[step];

			std::string targetObj = folder + currentFile;
			std::string fileName = getFileName(currentFile);
			std::string output = folder + fileName + ".ba";

			std::string command("ObjConverter.exe");
			command += " \"" + targetObj + "\" " + output;
			std::string finalCommand = command;
			int result = system(finalCommand.c_str());
			assert(result == 0);
		}

	}

}
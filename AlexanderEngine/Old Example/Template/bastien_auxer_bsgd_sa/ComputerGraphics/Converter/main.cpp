#include <ConverterSystem.h>
#include <ostream>


int main(int argc, char* argv[])
{
	Converter::ConverterSystem converter;
	Converter::FileGrabber graber;
	
	//setup

	std::string masterDIR = graber.getEnviromentVarible("OriginalAssets");
	std::string outputDIR = graber.getEnviromentVarible("GameOut");
	
	outputDIR += "Assets\\";

	bool noNeed = converter.graber.checkIfPathExsits(outputDIR);
	if(!noNeed)
	{
		converter.makeDir(outputDIR);
	}

	converter.setRootInputDir(masterDIR);
	converter.setRootOutputDir(outputDIR);

	std::cout << "Converting Images" << std::endl;
	converter.convertimages("\\images\\");
	
	std::cout << "Converting Music" << std::endl;
	converter.convertAudioFiles("\\music\\");

	std::cout << "Converting ShaderCode" << std::endl;
	converter.convertShaderFiles("\\ShaderCode\\");

	std::cout << "Converting Obj's" << std::endl;
	converter.convertObjFiles("\\Obj\\");
	///Convert all the files

}
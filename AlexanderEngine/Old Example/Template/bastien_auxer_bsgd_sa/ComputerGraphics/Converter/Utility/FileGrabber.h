#pragma once

#include <string>
#include <iostream>
#include <vector>
#include <windows.h>
#include <stdio.h>

namespace Converter
{

	class FileGrabber
	{

		bool checkSYSTEMTIME(SYSTEMTIME first, SYSTEMTIME sec);

	public:

		int getdir(std::string dir, std::vector<std::string> &files);
		bool checkIfFileExists(std::string file);
		bool checkIfUpdateNeeded(std::string currentFile, std::string outputFile);
		bool checkIfPathExsits(std::string dir);

		std::string getEnviromentVarible(std::string varible);

		FILETIME LastWritten(std::string filePath, bool& validFile);
		bool OutOfDate(std::string sourceFilePath, std::string generatedFilePath);

	};

}
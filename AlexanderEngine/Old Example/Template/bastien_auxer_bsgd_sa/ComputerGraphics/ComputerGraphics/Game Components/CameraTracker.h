
#pragma once
#ifndef ENGINE_CAMERA_COMPONENT_H
#define ENGINE_CAMERA_COMPONENT_H

#include <Component\Component.h>
#include <glm.hpp>

namespace Entites
{
	class Entity;

	class CameraTracker : public Component
	{
		Entity* targetToTrack;
		glm::vec3 UP;
		glm::vec3 positionFromTarget;

		float MOVEMENT_SPEED;

	public:

		glm::vec3 position;
		glm::vec3 viewDirection;

		glm::mat4 getWorldToViewMatrix() const;
		bool initialize(Entity* newTarget, glm::vec3 cameraLocationFromTarget, glm::vec3 viewDirection);
		void update();

		glm::vec3 getViewDirection();
		void setViewDirection(glm::vec3 newViewDirection);

		glm::vec3 getPosition();
		void setPosition(glm::vec3 newPosition);

		CameraTracker();
		~CameraTracker(void);
	};

}
#endif


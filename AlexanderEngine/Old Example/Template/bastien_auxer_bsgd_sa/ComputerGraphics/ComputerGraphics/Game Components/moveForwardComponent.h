#pragma once

#ifndef ENGINE_MOVEFORWARD_COMPONENT_H
#define ENGINE_MOVEFORWARD_COMPONENT_H

#include <Component\Component.h>

namespace Entites
{

	class MoveForward : public Component
	{
		float moveSpeed;
	public:
		bool initialize(float speed);
		void update();
	};

}

#endif
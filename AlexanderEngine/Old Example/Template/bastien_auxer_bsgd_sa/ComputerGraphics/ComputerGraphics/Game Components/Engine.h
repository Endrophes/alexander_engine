#pragma once

#ifndef ENGINE_ENGINE_COMPONENT_H
#define ENGINE_ENGINE_COMPONENT_H

#include <Component\Component.h>

namespace Entites
{


	class Engine : public Component
	{
	public:
		Engine(void);
		~Engine(void);

		void update();
	};

}

#endif
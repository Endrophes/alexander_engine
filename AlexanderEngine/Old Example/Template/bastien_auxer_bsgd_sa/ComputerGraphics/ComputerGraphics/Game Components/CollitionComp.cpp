#include <Enity\Entity.h>
#include <glm.hpp>

#include <Game Components\CollitionComp.h>

namespace Entites
{

	bool CollitionComp::step(float target)
	{
		bool result = true;

		targetDistance = target;

		return result;
	}

	bool CollitionComp::checkIfHit(Entity target)
	{
		glm::vec3 targetPosition = target.position;
		glm::vec3 currentPosition = getParent()->position;

		glm::vec3 diffrence = (targetPosition - currentPosition);
		float length = 3;

		bool result = false;

		if(length <= targetDistance)
		{
			result = true;
		}

		return result;
	}

}
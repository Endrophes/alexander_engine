#include "Turbo.h"
#include <Enity\Entity.h>
#include <Input\KeyInput.h>
#include <Game Components\menuChoice.h>

namespace Entites
{

	Turbo::Turbo(void)
	{
		baseSpeed = 3.0f;
		bootSpeed = 10.0f;
	}


	Turbo::~Turbo(void)
	{

	}

	void Turbo::setUp(float baseSpeed, float bootSpeed)
	{
		this->baseSpeed = baseSpeed;
		this->bootSpeed = bootSpeed;
	}

	void Turbo::update()
	{
		float newVelocity = baseSpeed;

		if(input.activeActionThisFrame(Input::Shift))
		{
			newVelocity = bootSpeed;
		}

		getParent()->velocity = newVelocity;
	}

}
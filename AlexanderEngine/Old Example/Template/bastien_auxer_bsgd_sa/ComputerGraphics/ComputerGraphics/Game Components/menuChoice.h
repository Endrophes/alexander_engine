#ifndef MY_MENU_CHOICE_H
#define MY_MENU_CHOICE_H

namespace Input
{
	const int basNum = 1;

	enum MenuChoice
	{
		Up          =  (basNum << 0),
		Down        =  (basNum << 1),
		Left        =  (basNum << 2),
		Right       =  (basNum << 3),
		Space		=  (basNum << 4),
		Enter		=  (basNum << 5),
		Shift       =  (basNum << 6),
		W           =  (basNum << 7),
		A           =  (basNum << 8),
		S           =  (basNum << 9),
		D           =  (basNum << 10),
		Q           =  (basNum << 11),
		E           =  (basNum << 12),
		MAX         =  (basNum << 13),

	};
}

#endif
#include "CameraTracker.h"
#include <Enity\Entity.h> 
#include <Components\RendererComponent.h>

namespace Entites
{

	CameraTracker::CameraTracker()
	{

	}

	CameraTracker::~CameraTracker(void)
	{

	}

	bool CameraTracker::initialize(Entity* newTarget, glm::vec3 cameraLocationFromTarget, glm::vec3 viewDirection)
	{
		targetToTrack = newTarget;
		positionFromTarget = cameraLocationFromTarget;
		this->viewDirection = viewDirection;

		return true;
	}

	void CameraTracker::update()
	{
		RendereComponent* toargetObj = targetToTrack->getComponent<RendereComponent>();
		glm::vec3 CurrentPosition = toargetObj->getParent()->position;
		glm::vec3 cameraNewPosition = CurrentPosition + positionFromTarget;

		getParent()->position = cameraNewPosition;
	}

}
#include <Enity\Entity.h>
#include <glm.hpp>
#include <Input\KeyInput.h>
#include <Game Components\menuChoice.h>
#include <Game Components\moveForwardComponent.h>

#ifdef _DEBUG
	#include <Debug Tools\Memmory Maniger\DebugMemory.h>
#endif


namespace Entites
{
	bool MoveForward::initialize(float speed)
	{
		bool result = true; 

		this->moveSpeed = speed;

		return result;
	}

	void MoveForward::update()
	{
		glm::vec3 currentPosition = getParent()->position;

		if(input.activeActionThisFrame(Input::MenuChoice::Shift))
		{
			currentPosition.z += (moveSpeed * 2);
		}
		else
		{
			currentPosition.z += (moveSpeed);
		}

		//new int;
		getParent()->position = currentPosition;
	}
}
#include <Game Components\randomPositonComp.h>
#include <Random Generator\RandomGenerator.h>

//randomPositonComp::

namespace Entites
{

	bool randomPositonComp::initialize(glm::vec3 min, glm::vec3 max)
	{
		bool results = true;

		this->min = min;
		this->max = max;

		return results;
	}

	glm::vec3 randomPositonComp::getNewPosition()
	{

		float x = Random->randomFloatInRange(min.x, max.x);
		float y = Random->randomFloatInRange(min.y, max.y);
		float z = Random->randomFloatInRange(min.z, max.z);

		return glm::vec3( x, y, z);

	}

	void randomPositonComp::update()
	{

	}

}
#include <Game Components\RestToRandomPositionComp.h>
#include <Enity\Entity.h>
#include <Random Generator\RandomGenerator.h>


#ifdef _DEBUG
	#include <Debug Tools\Memmory Maniger\DebugMemory.h>
#endif

//RestToRandomPositionComp::

namespace Entites
{

	bool RestToRandomPositionComp::initialize(glm::vec3 min, glm::vec3 max, float targetPoint, int axis)
	{
		bool results = true;

		axisOption = axis;
		this->targetPoint = targetPoint;
		this->min = min;
		this->max = max;

		return results;
	}

	glm::vec3 RestToRandomPositionComp::getNewPosition()
	{
		float x = Random->randomFloatInRange(min.x, max.x);
		float y = Random->randomFloatInRange(min.y, max.y);
		float z = Random->randomFloatInRange(min.z, max.z);

		return glm::vec3(x, y, z);
	}

	void RestToRandomPositionComp::update()
	{
		glm::vec3 currentPosition = getParent()->position;

		bool changePosition = false;

		if(axisOption == axis::xAxis)
		{
			if(currentPosition.x >= targetPoint)
			{
				changePosition = true;
			}
		}
		else if(axisOption == axis::yAxis)
		{
			if(currentPosition.y >= targetPoint)
			{
				changePosition = true;
			}
		}
		else if(axisOption == axis::zAxis)
		{
			if(currentPosition.z >= targetPoint)
			{
				changePosition = true;
			}
		}

		if(changePosition)
		{
			currentPosition = this->getNewPosition();
			getParent()->position = currentPosition;
		}

	}

}
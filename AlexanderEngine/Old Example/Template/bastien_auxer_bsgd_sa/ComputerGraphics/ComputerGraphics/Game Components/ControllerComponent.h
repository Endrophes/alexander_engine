#pragma once
#ifndef ENGINE_CONTROLLER_COMPONENT_H
#define ENGINE_CONTROLLER_COMPONENT_H

#include <Component\Component.h>

namespace Entites
{

	class PhysicsComponent;
	class ControllerComponent : public Component
	{
		PhysicsComponent* physics;
		float moveBy;
		float rotateBy;
	public:
		bool initialize();
		void update();
		void setup(float moveBy = 0.2f, float rotateBy = 0.2f);
	};
}
#endif
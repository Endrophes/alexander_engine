#pragma once
#ifndef ENGINE_RANDOMPOSITON_COMPONENT_H
#define ENGINE_RANDOMPOSITON_COMPONENT_H

#include <Component\Component.h>
#include <glm.hpp>

namespace Entites
{
	class randomPositonComp : public Component
	{
		glm::vec3 min;
		glm::vec3 max;



	public:
		bool initialize(glm::vec3 min, glm::vec3 max);
		glm::vec3 getNewPosition();
		void update();
	};
}

#endif
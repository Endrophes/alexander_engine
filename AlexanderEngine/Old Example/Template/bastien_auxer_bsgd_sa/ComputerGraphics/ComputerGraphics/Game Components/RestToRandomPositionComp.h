#pragma once

#include <Component\Component.h>
#include <glm.hpp>

namespace Entites
{

	class RestToRandomPositionComp : public Component
	{
		glm::vec3 min;
		glm::vec3 max;

		float targetPoint;

		int axisOption;

		enum axis
		{
			xAxis = 1,
			yAxis = 2,
			zAxis = 3,
		};



	public:
		bool RestToRandomPositionComp::initialize(glm::vec3 min, glm::vec3 max, float targetPoint, int axis);
		glm::vec3 getNewPosition();
		void update();
	};

}


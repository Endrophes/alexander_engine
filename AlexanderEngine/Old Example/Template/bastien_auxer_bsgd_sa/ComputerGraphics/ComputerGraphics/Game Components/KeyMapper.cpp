#include <Windows.h>
#include <Game Components\KeyMapper.h>
#include <Game Components\menuChoice.h>
#include <assert.h>

#ifdef _DEBUG
	#include <Debug Tools\Memmory Maniger\DebugMemory.h>
#endif


namespace Input
{
	/*
	int KeyMapper::getActionFor(int key) const
	{
		int a = -1;

		switch(key)
		{
			case Accelerate  : a = VK_UP;
			case VK_LEFT  : a = RotateLeft;
			case VK_RIGHT : a = RotateRight;
			case Up          : a = VK_UP;
			case VK_DOWN		 : a = Down;
			case VK_LEFT		 : a = Left;
			case VK_RIGHT		 : a = Right;
		}

		bool worked = a != -1;
		assert(worked);
		return a;
	}
	//*/

	int KeyMapper::getKeyFor(int action) const
	{
		int k = -1;

		switch(action)
		{
			case Up    : k = VK_UP     ; break;
			case Down  : k = VK_DOWN   ; break;
			case Left  : k = VK_LEFT   ; break;
			case Right : k = VK_RIGHT  ; break;
			case Space : k = VK_SPACE  ; break;
			case Enter : k = VK_RETURN ; break;
			case Shift : k = VK_SHIFT  ; break;
			case W : k = VK_F1  ; break;
			case A : k = VK_F2  ; break;
			case S : k = VK_F3  ; break;
			case D : k = VK_F4  ; break;
			case Q : k = VK_F5  ; break;
			case E : k = VK_F6  ; break;
		}

		bool goodToGo = (k != -1);
		assert(goodToGo);

		return k;
	}

	KeyMapper::KeyMapper()
	{

	}
}
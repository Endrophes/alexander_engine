#include "Thruster.h"
#include <Input\KeyInput.h>
#include <Game Components\menuChoice.h>

namespace Entites
{

	Thruster::Thruster(void)
	{
		lerpAmount = 0.1f;
		stepAmount  = 0.1f;
	}


	Thruster::~Thruster(void)
	{

	}

	glm::vec3 Thruster::lerp(glm::vec3 A, glm::vec3 B, float amount)
	{
		return ((A + (B - A)) / amount);
	}

	void Thruster::lerpUp()
	{
		if(lerpAmount < 0.9f)
		{
			lerpAmount += stepAmount;
		}
	}

	void Thruster::lerpDown()
	{
		if(lerpAmount > 0.5f)
		{
			lerpAmount -= stepAmount;
		}
	}

	void Thruster::setup(float stepAmount, glm::vec3 minScale, glm::vec3 maxScale, Render::Renderable* renderable)
	{
		this->stepAmount = stepAmount;
		this->renderable = renderable;
		this->minScale = minScale;
		this->maxScale = maxScale;
	}

	void Thruster::update()
	{
		if(input.activeActionThisFrame(Input::MenuChoice::Shift))
		{
			lerpDown();
		}
		else
		{
			lerpUp();
		}

		scale = lerp(maxScale, minScale, lerpAmount);
		renderable->scale = scale;
		renderable->scaleMatrix = glm::scale(scale);
		renderable->Position = this->position;
		renderable->translationMatrix = glm::translate(this->position);
	}

}
#include <Input\KeyInput.h>
#include <Enity\Entity.h>
#include <cmath>
#include <glm.hpp>
#include <Game Components\ControllerComponent.h>
#include <Game Components\menuChoice.h>
#include <Components\PhysicsComponent.h>

#ifdef _DEBUG
	#include <Debug Tools\Memmory Maniger\DebugMemory.h>
#endif


namespace Entites
{

	bool ControllerComponent::initialize()
	{
		bool allGood = true;

		physics = getParent()->getComponent<PhysicsComponent>();
		if(physics == NULL)
		{
			allGood = false;
		}

		moveBy = 0.2f;
		rotateBy = 0.2f;
		
		return allGood;
	}

	void ControllerComponent::setup(float moveBy, float rotateBy)
	{
		this->moveBy = moveBy;
		this->rotateBy = rotateBy;
	}

	void ControllerComponent::update()
	{
		//const float ANGULAR_VELOCITY = 0.1f;

		float baseX = 4.5f;
		float baseY = 2.4f;

		float maxX =  baseX;
		float minX = -baseX;
		
		float maxY =  baseY;
		float minY = -baseY;

		//if(input.activeActionThisFrame(Input::RotateLeft))
		//{
		//	//physics->angularVelocity = ANGULAR_VELOCITY;
		//}
		//if(input.activeActionThisFrame(Input::RotateRight))
		//{
		//	//physics->angularVelocity = -ANGULAR_VELOCITY;
		//}
		//if(input.activeActionThisFrame(Input::Accelerate))
		//{
		//	//getParent()->orientationX;
			//getParent()->orientationY;
			//getParent()->orientationZ;
			//float pOrient = getParent()-> orientation;
			//getParent()->getComponent<PhysicsComponent>()->acceleration = 
			//	glm::vec3(-sin(pOrient), cos(pOrient));
			//physics->acceleration.x = -sin(pOrient);
			//physics->acceleration.y = cos(pOrient);
		//}

		glm::vec3 currentPosition = getParent()->position;
		//glm::vec3 currentRotation = getParent()->rotation;
		glm::vec3 currentForward = getParent()->forwardVector;

		if(input.activeActionThisFrame(Input::Up))
		{
			if(currentPosition.y < maxY)
			{
				currentPosition.y += moveBy;
				//currentRotation.x += rotateBy;
			}
		}
		if(input.activeActionThisFrame(Input::Down))
		{
			if(currentPosition.y > minY)
			{
				currentPosition.y -= moveBy;
				//currentRotation.x -= rotateBy;
			}
		}
		if(input.activeActionThisFrame(Input::Right))
		{
			if(currentPosition.x < maxX)
			{
				currentPosition.x += moveBy;
				//currentRotation.y += rotateBy;
			}
		}
		if(input.activeActionThisFrame(Input::Left))
		{
			if(currentPosition.x > minX)
			{
				currentPosition.x -= moveBy;
				//currentRotation.y -= rotateBy;
			}
		}

		getParent()->position = currentPosition;
		//getParent()->rotation = currentRotation;
	}


}
#ifndef ENGINE_MYKEYMAPPER_H
#define ENGINE_MYKEYMAPPER_H

#include <Input\IKeyMapper.h>

#pragma once

namespace Input
{
	class KeyMapper : public IKeyMapper
	{
	public:
		//int getActionFor(int key) const;
		int getKeyFor(int action) const;
		KeyMapper();
	};
}
#endif
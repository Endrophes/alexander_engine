#pragma once
#ifndef ENGINE_THRUSTER_H
#define ENGINE_THRUSTER_H

#include <vector>
#include <glm.hpp>
#include <Enity\Entity.h>
#include <Render\Render.h>


namespace Entites
{

	class Thruster : public Entity
	{
	public:

		glm::vec3 minScale;
		glm::vec3 maxScale;
		glm::vec3 scale;

		float lerpAmount;
		float stepAmount;

		Render::Renderable* renderable;

		Thruster(void);
		~Thruster(void);

		glm::vec3 lerp(glm::vec3 A, glm::vec3 B, float amount);

		void lerpUp();

		void lerpDown();

		void setup(float stepAmount, glm::vec3 minScale, glm::vec3 maxScale, Render::Renderable* renderable);

		void update();

	};

}
#endif
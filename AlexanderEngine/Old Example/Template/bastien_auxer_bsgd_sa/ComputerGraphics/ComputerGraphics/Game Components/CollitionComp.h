#pragma once
#ifndef ENGINE_COLLITION_COMPONENT_H
#define ENGINE_COLLITION_COMPONENT_H

#include <Component\Component.h>

namespace Entites
{

	class Entity;

	class CollitionComp : public Component
	{
		float targetDistance;
	public:
		bool step(float target);
		bool checkIfHit(Entity target);
	};

}
#endif
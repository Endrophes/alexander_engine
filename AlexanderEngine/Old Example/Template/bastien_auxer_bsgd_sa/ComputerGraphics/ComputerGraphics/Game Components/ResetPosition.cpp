#include <Game Components\ResetPosition.h>
#include <Enity\Entity.h>
#include <Random Generator\RandomGenerator.h>

#ifdef _DEBUG
	#include <Debug Tools\Memmory Maniger\DebugMemory.h>
#endif

namespace Entites
{
	bool ResetPosition::initialize(glm::vec3 restTo, float targetPoint, int axis)
	{
		this->restTo = restTo;

		this->targetPoint = targetPoint;
		this->axisOption = axis;

		return true;
	}
	
	void ResetPosition::update()
	{
		glm::vec3 currentPosition = getParent()->position;

		bool changePosition = false;

		if(axisOption == axis::xAxis)
		{
			if(currentPosition.x <= targetPoint)
			{
				changePosition = true;
			}
		}
		else if(axisOption == axis::yAxis)
		{
			if(currentPosition.y <= targetPoint)
			{
				changePosition = true;
			}
		}
		else if(axisOption == axis::zAxis)
		{
			if(currentPosition.z <= targetPoint)
			{
				changePosition = true;
			}
		}

		if(changePosition)
		{
			getParent()->position = restTo;
		}
	}

}
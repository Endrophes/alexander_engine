#pragma once
#ifndef ENGINE_TURBO_COMPONENT_H
#define ENGINE_TURBO_COMPONENT_H

#include <Component\Component.h>

namespace Entites
{

	class Turbo : public Component
	{

		float baseSpeed;
		float bootSpeed;

	public:
		Turbo(void);
		~Turbo(void);

		void setUp(float baseSpeed = 3.0f, float bootSpeed = 10.0f);
		void update();

	};


}
#endif
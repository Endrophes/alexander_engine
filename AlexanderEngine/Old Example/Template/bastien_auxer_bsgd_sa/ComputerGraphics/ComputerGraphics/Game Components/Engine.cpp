#include "Engine.h"
#include <Enity\Entity.h>
#include <glm.hpp>
namespace Entites
{

	Engine::Engine(void)
	{
	}


	Engine::~Engine(void)
	{
	}


	void Engine::update()
	{
		/* //Debug
		float Vel = getParent()->velocity;
		glm::vec3 FV = getParent()->forwardVector;
		glm::vec3 newposition = (FV * Vel);
		getParent()->position += newposition;
		//*/

		getParent()->position += getParent()->velocity * getParent()->forwardVector;
	}

}
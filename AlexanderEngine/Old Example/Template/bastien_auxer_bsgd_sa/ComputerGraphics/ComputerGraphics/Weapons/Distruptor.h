#pragma once

#include <Component\Component.h>
#include <glm.hpp>
#include <vector>

namespace Entites
{

	class Distruptor : public Component
	{
		int ammoCount;
		//std::vector<Component*> components;

	public:
		bool initialize(int numShots);
		void fire();
		void update();
	};

}


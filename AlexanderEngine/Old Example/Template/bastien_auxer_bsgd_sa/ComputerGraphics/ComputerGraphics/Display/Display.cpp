#pragma once

#include <Display\Display.h>
#include <Node\NodeHandler.h>

#ifdef _DEBUG
	#include <Debug Tools\Memmory Maniger\DebugMemory.h>
#endif

//#define RC_Char(x) reinterpret_cast<char*>(x)

using glm::vec3;

//Render::shaderInfo normalShaders;
Render::GeomitryInfo level;

Display::Display()
{
	exampleSliderTarget = 0.0f;
	
	connect(&timer,SIGNAL(timeout()), this, SLOT(myUpdate()));
	timer.start(0);

	QVBoxLayout* mainLayout = new QVBoxLayout();
	QWidget::setLayout(mainLayout);

	//place debug window here
	#ifdef DEBUG_VALUES
		shapeAdded = false;
		hidden = false;
		buttonPressed = false;
		debugMenu = new DebugMenu();
		mainLayout->addWidget(debugMenu);
		debugMenu->setLayout(debugMenu->debugLayout);
		debugMenu->setHidden(true);
		debugMenu->setFixedHeight(200);
	#endif

	worldView.setMinimumHeight(900);
	mainLayout->addWidget(&worldView);
	debugMenu->displayValues();

	faileSaveLocation = "";
}

void Display::setupDebugMenu()
{
	//float min = 0.0f;
	//float max = 0.0f;

	//This run's before the renderables are created
	//To add shapes you need to go down
	//int pnm = debugMenu->addTab("Plane Normal Map");
	//debugMenu->addFlaotSlider("x",worldView.cube->Position.x,0,5,pnm);
	//debugMenu->addFlaotSlider("y",worldView.cube->Position.x,0,5,pnm);
	//debugMenu->addFlaotSlider("z",worldView.cube->Position.x,0,5,pnm);
	//debugMenu->addCheckBox("Show Light", worldView.renderer.showlight,pnm);
	//debugMenu->addFlaotSlider("Color Of light Bulb Green", worldView.renderer.colorLightBulb.y, 0.0f, 1.0f);
	//debugMenu->addFlaotSlider("Color Of light Bulb Blue", worldView.renderer.colorLightBulb.z, 0.0f, 1.0f);
	//debugMenu->addVec3("Camera Position", worldView.renderer.cameraPosition);
	//debugMenu->addBool("Show Light", worldView.renderer.showlight);

	///To add shapes you need to go down
	/*
	int lc = debugMenu->addTab("Light Controls");
	min = -100.0f;
	max = 100.0f;
	debugMenu->addFlaotSlider("x",worldView.LightPosition->x,min,max,lc, 0.0f, 2000.0f);
	debugMenu->addFlaotSlider("y",worldView.LightPosition->y,min,max,lc, 1.2f, 2000.0f);
	debugMenu->addFlaotSlider("z",worldView.LightPosition->z,min,max,lc, 3.0f, 2000.0f);
	debugMenu->addCheckBox("Show Light", worldView.DisplayLight,lc);
	/*/
	
	debugMenu->displayValues();
}

void Display::myUpdate()
{
	#ifdef DEBUG_VALUES
	if(GetAsyncKeyState(VK_OEM_3) && !buttonPressed)
	{
		buttonPressed = true;

		if(hidden)
		{
			hidden = false;
		}
		else
		{
			hidden = true;
		}

		if(!shapeAdded)
		{
			//setupDebugMenu();
			shapeAdded = true;
		}

		debugMenu->setVisible(hidden);
	}
	else if(!GetAsyncKeyState(VK_OEM_3))
	{
		buttonPressed = false;
	}
	
	/*if(!(debugMenu->isHidden()))
	{*/
		//debugMenu->update();
	//}
	#endif

	#if PROFILING_ON
	{
		profiler.initialize("profiles.csv");
		PROFILE("Update total time");
		profiler.newFrame();
	}
	#endif
	worldView.update();

}

void Display::resetSystem()
{
	/*worldView.nodeHandler.resetNodes();*/
}

Display::~Display()
{	
	#ifdef DEBUGMENU_ON
		delete debugMenu;
	#endif
}
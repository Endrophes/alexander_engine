#pragma once
#ifndef DISPLAY
#define DISPLAY

#include <GL\glew.h>
#include <glm.hpp>
#include <Qt\qdebug.h>
#include <QtGui\QHBoxLayout>
#include <QtGui\QVBoxLayout>
#include <QtGui\QFileDialog>
#include <QtGui\QMenu>
#include <QtGui\QMenuBar>
#include <QtGui\QMainWindow>
#include <QtGui\qwidget.h>
#include <QtGui\qwidget>
#include <QtGui\QPushButton>
#include <QtGui\QCheckBox>
#include <GL\glew.h>
#include <glm.hpp>
#include <QtGui\qlabel>
#include <Qt\qtimer.h>
#include <fstream>
#include <Windows.h>

#include <QtGui\QTabBar>
#include <QtGui\QTabWidget>

#include <Sliders\DebugSlider.h>
#include <GLWindow.h>

#ifdef DEBUGMENU_ON
	#include <Debug Tools\DebugMenu\DebugMenu.h>
#endif

class Display : public QWidget
{
	Q_OBJECT;
	QTimer timer;

	std::string faileSaveLocation;

	#ifdef DEBUGMENU_ON
		bool hidden;
		bool shapeAdded;
		bool buttonPressed;
		DebugMenu* debugMenu;
	#endif

	float exampleSliderTarget;
	int numberOfQlabels;
	int numberOfSliders;
	int numberOfCheckBoxes;

	void resetSystem();

	void saveOutFile();

	void setupDebugMenu();

public:
	Display();
	~Display();
	GLWindow worldView;

private slots:
	void myUpdate();
};

#endif

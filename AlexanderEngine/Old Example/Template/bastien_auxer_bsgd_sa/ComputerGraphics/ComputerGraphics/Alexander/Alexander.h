#pragma once

#ifndef ALEXANDER
#define ALEXANDER

#include <Core.h>
#include <glm.hpp>
#include <Particle\Particle.h>

using Core::RGB;

class Alexander
{
public:

	static Particle sign[];

	bool signName;

	int dealy;
	int defultDealy;
	int defultNTL;
	int numberToLight;
	int numParticels;
	float scale;

	glm::vec3 offSet;

	Alexander(void);
	~Alexander(void);

	void transformationCalculation();
	void setParticules(float addX = 0.00f, float addY = 0.00f);
	void drawName(Core::Graphics& graphics);
	void updateName();

	void shutdown();
};

#endif
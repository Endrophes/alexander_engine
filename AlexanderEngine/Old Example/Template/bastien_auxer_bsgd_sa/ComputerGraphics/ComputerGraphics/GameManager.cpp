#include <GameManager.h>
#include <Game Components\menuChoice.h>
#include <Game Components\RestToRandomPositionComp.h>
#include <Input\KeyInput.h>
#include <Audio\Audio.h>

#ifdef _DEBUG
	#include <Debug Tools\Memmory Maniger\DebugMemory.h>
#endif

namespace Game
{

	GameManager::GameManager(void)
	{
		audio.addAudioFile("BG", "Assets\\music\\BattlestarGalactica.mp3", true);
		audio.addAudioFile("VE", "Assets\\music\\viper_group.mp3", true);
		audio.addAudioFile("VG", "Assets\\music\\vipergun.mp3", false);
		audio.addAudioFile("RE", "Assets\\music\\CylonRaiderEngine.mp3", false);
		audio.addAudioFile("EX", "Assets\\music\\Explosion.mp3", false);

		audio.playFile("BG", true);
		//audio.playFile("VE", true);

		decrament = 0.1f;
		maxDelay = 3.0f;
		fireDelay = 0.0f;
	}

	GameManager::~GameManager(void)
	{

	}

	void GameManager::inisialize()
	{
		//Put forLoops here for setup?
		closest = FLT_MAX;
		targetDistance = 0.4f;
		gameState = gameStates::Splash;
	}

	void GameManager::setState(int newState)
	{
		gameState = newState;
	}


	void GameManager::update()
	{
		/*
		if(gameState == gameStates::Splash)
		{
			//draw splash
		}
		if(gameState == gameStates::Game)
		{
			//Update Objects
			gameUpdate();
		}
		//*/

		//Temp till splash
		gameUpdate();
	}

	void GameManager::keyCheck()
	{
		input.update();

		bool fire = input.activeActionThisFrame(Input::MenuChoice::Space);

		if(fire)
		{
			if(fireDelay <= 0.0f)
			{
				fireDelay = maxDelay;
				//cout << "Fire!" << endl;
				uint size = torpedos.size();
				for(uint step = 1; step < size; step++) 
				{
					audio.playFile("VG", false);
					Entites::torpedo* current = torpedos.at(step);

					if(!current->fired)
					{
						current->position = hero.position;
						current->fired = true;
						break;
					}

				}
			}
			else
			{
				fireDelay -= decrament;
			}
		}

		bool moreLife = input.activeActionThisFrame(Input::MenuChoice::Enter);
		if(moreLife)
		{
			hero.setLife(3);
			hero.revive();
		}

	}

	void GameManager::gameUpdate()
	{
		keyCheck();
		hero.update();

		//cout << "Size: " << enemyShips.size() << endl;
		for(uint step = 0; step < (enemyShips.size()); step++) {
		
			Entites::Ship* currentEnemey = enemyShips[step];
			currentEnemey->update();
			if(currentEnemey->position.z >= 4.0f)
			{
				currentEnemey->hit = false;
			}
			//cout << "Step: " << step << endl;
		}

		for(uint step = 0; step < (HealthBar.size()); step++) {
		
			Entites::Ship* currentEnt = HealthBar[step];
			currentEnt->update();
		}

		uint size = torpedos.size();
		for(uint step = 1; step < size; step++) 
		{
			Entites::torpedo* current = torpedos.at(step);

			if(current->fired)
			{
				current->update();
				if(current->position.z <= -20.0f)
				{
					current->fired = false;
				}
			}
		}


		//for(uint step = 0; step < (particleSystems.size()); step++)
		//{
		//	particleSystems[step]->update();
		//}


		for(uint step = 0; step < (Thrusters.size()); step++)
		{
			Thrusters[step]->update();
		}

		checkForCollition();

	}

	float GameManager::getLengthOfVector(glm::vec3 vec)
	{
		vec.x = (vec.x * vec.x);
		vec.y = (vec.y * vec.y);
		vec.z = (vec.z * vec.z);

		float added = vec.x + vec.y + vec.z;

		float result = sqrt(added);

		//float dotedVec = glm::dot(vec,vec);

		return result;
	}

	bool GameManager::checkDistance(glm::vec3 locationOne, glm::vec3 locationTwo, float targetDistance)
	{
		glm::vec3 diffrence = locationOne - locationTwo;
		float length = getLengthOfVector(diffrence);
	
		bool result = false;

		if(length <= targetDistance)
		{
			result = true;
		}

		//if(closest > length)
		//{
		//	closest = length;
		//}

		//cout << "check results: " << result << endl;

		return result;
	}

	void GameManager::checkForCollition()
	{
			//hero

		glm::vec3 heroPoition = hero.position;
	
		float size = enemyShips.size();
		for(uint step = 1; step < size; step++) {

			bool hit = false;

			glm::vec3 enemyPosition = enemyShips[step]->position;
			hit = checkDistance(enemyPosition, heroPoition, targetDistance);

			if(hit && !enemyShips[step]->hit)
			{
				hero.takeHit(1);
				enemyShips[step]->hit = true;
				enemyShips[step]->takeHit(9000);

			}
			else
			{
				float torpedoBayCapacity = torpedos.size();
				for(uint step = 1; step < torpedoBayCapacity; step++) 
				{
					Entites::torpedo* current = torpedos.at(step);
					if(checkDistance(enemyPosition, current->position, targetDistance))
					{
						hit = true;
						break;
					}
				}
			}

			if(hit)
			{
				//cout << "HIT!" << endl;
				enemyShips[step]->hit = true;
				enemyShips[step]->takeHit(9000);
				//heroShipRender->renderable->visable = false;
			}

		}
	}

}
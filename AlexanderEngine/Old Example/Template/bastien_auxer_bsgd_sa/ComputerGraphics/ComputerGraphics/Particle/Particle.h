#pragma once

#ifndef PARTICLE
#define PARTICLE

#include <glm.hpp>
#include <Core.h>

using Core::RGB;

class Particle
{

public:
	bool reversed;
	bool alive;
	int type;
	double lifetime;

	RGB color;

	glm::vec3 position;
	glm::vec3 velocity;
	glm::vec3 origen;

	Particle(int kind = 1,glm::vec3 start = glm::vec3(0.0f,0.0f,0.0f), glm::vec3 speed = glm::vec3(10.0f,0.0f,0.0f),
		RGB startColor = (10,10,10), double lifeSpan = 200);

	~Particle(void);


	void particleUpdate(float dt);
	void setColor(int red, int green, int blue);
	void Particle::setPosition(float x, float y, float z);

};

#endif

#include <Particle\Particle.h>
#include <Random Generator\RandomGenerator.h>

Particle::Particle(int kind,glm::vec3 start, glm::vec3 speed,
	RGB startColor, double lifeSpan){
	position = start;
	origen = start;
	velocity = speed;
	color = startColor;
	lifetime = lifeSpan;
	//lifetime = 20;
	type = kind;
	reversed = false;
	alive = false;
}

Particle::~Particle(void)
{

}

void Particle::particleUpdate(float dt){
	lifetime -= 1;
	position = position + (dt * velocity); 
}

void Particle::setColor(int red, int green, int blue){
	color = RGB(red, green, blue);
}

void Particle::setPosition(float x, float y, float z){
	position = glm::vec3(x, y, z);
}

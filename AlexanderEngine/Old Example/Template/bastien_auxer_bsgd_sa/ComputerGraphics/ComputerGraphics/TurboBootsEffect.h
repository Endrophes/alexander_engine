#pragma once

#include <gl\glew.h>
#include <QtOpenGL\qglwidget>
#include <glm.hpp>
#include <gtx\transform.hpp>

namespace Effects
{

	class TurboBootsEffect
	{

	static float g_vertex_buffer_data[];
	unsigned int billboard_vertex_buffer;
	unsigned int particles_position_buffer;
	unsigned int particles_color_buffer;

	int g_particule_position_size_data;
	int g_particule_color_data;

	int MaxParticles;

	public:
		TurboBootsEffect(void);
		~TurboBootsEffect(void);

		void setup();
		void update();

		void draw();

	};

}
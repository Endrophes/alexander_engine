#pragma once

#ifndef NODEHADLER
#define NODEHADLER


#define NODE_STORAGE_SIZE 100
#define LINKED_NODE_STORAGE_SIZE 50
#define ARROW_STORAGE_SIZE 50

#include <Render\Render.h>
#include <Camera\Camera.h>
#include <vector>

using glm::vec3;
using glm::mat4;

class NodeHandler
{
public:
	class gameNode;

	//in game info
	class gameConnection
	{
	public:
		float cost;
		gameNode* target;
	};

	class gameNode
	{
	public:
		vec3 position;
		int numberofConnections;
		gameConnection* connections;
	};

	class arrow
	{
	public:
		float cost;

		mat4 arrowTranslationMatrix;
		mat4 arrowTransformationMatrix;
		mat4 arrowScaleMatrix;
		mat4 arrowRoationMatrix;
	   float arrowScale;

	    mat4 coneTranslationMatrix;
		mat4 coneTransformationMatrix;
		mat4 coneScaleMatrix;
		mat4 coneRoationMatrix;
	   float coneScale;

	   int arrowNumber;
	   int pointingToNode;

	   bool used;

		arrow()
		{
			arrowTranslationMatrix = mat4();
			arrowTransformationMatrix = mat4();
			arrowScaleMatrix = mat4();
			arrowRoationMatrix = mat4();
			arrowScale = 0.0f;
			
			coneTranslationMatrix = mat4();
			coneTransformationMatrix = mat4();
			coneScaleMatrix = mat4();
			coneRoationMatrix = mat4();
			coneScale = 0.0f;

			pointingToNode = 0;
			arrowNumber = 0;
			used = false;
		}
	};

	class Node
	{
	public:
		vec3 position;
		bool MasterNode;
		bool seconedNode;

		bool used;

		int currentArrowLink;
		int currentLinkedNode;
		int numberOfNode;

		std::vector<arrow*> connections;


		int linkendNodeStorage[LINKED_NODE_STORAGE_SIZE];
		int arrowStroage[LINKED_NODE_STORAGE_SIZE];

		Node()
		{
			used = false;
			currentArrowLink = 0;
			currentLinkedNode = 0;
			MasterNode = false;
			seconedNode = false;
			position = vec3();
		}
	};


	Node* currentMasterNode;

	Render* worldCreater;
	Camera currentCam;
	Render::GeomitryInfo shpere;
	Render::shaderInfo nodeShader;
	Render::Renderable* nodeRenderable;

	Render::GeomitryInfo cylenderGI;
	Render::GeomitryInfo coneGI;
	Render::Renderable* coneRenderable;
	Render::Renderable* arrowRenderable;


	mat4* projectionMatrix;
	mat4* cameraViewMatrix;

	vec3 Threshold;

	float shpereSize;

public:

	int maxnumberOfNodes;

	int currentArrow;
	int currentNode;

	int* mainNode;
	int* seconedNode;

	arrow arrowStorage[ARROW_STORAGE_SIZE];
	Node nodeStorage[NODE_STORAGE_SIZE];

	mat4 CreateBasisFromOneVector(vec3* basis);
	//Node* toNode, float length
	void addRay(Node* toNode, Node* fromNode, float length);
	void addRay(Node* toNode, float length);

	bool masterNodeSelected;

	vec3 rayToworldCalculator(int mouseXposition, int mouseYposition, int width, int height);

	void addNode(vec3 position);

	void addNode(vec3 position, Node* TrackNode);

	void addNode(int mouseXposition, int mouseYposition, int width, int height);

	void deleteNode();

	void selectNode(int mouseXposition, int mouseYposition, int width, int height);

	void resetNodes();

	void drawNodes();
	
	void massReset();

	NodeHandler();
	NodeHandler(mat4* projection, mat4* cameraview, Render* currentRenderer, Camera setCam);
	~NodeHandler();
};

#endif
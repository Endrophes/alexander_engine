#include "NodeHandler.h"
#include <QtGui\QMouseEvent>
#include <QtGui\QKeyEvent>

#ifdef _DEBUG
	#include <Debug Tools\Memmory Maniger\DebugMemory.h>
#endif


vec3 NodeHandler::rayToworldCalculator(int mouseXposition, int mouseYposition, int width, int height)
{
	float x = (2.0f * mouseXposition) / width - 1.0f;
	float y = 1.0f - (2.0f * mouseYposition) / height;
	float z = -1.0f;

	vec3 ray_nds = vec3 (x, y, z);
	vec4 ray_clip = vec4 (ray_nds.x, ray_nds.y, -1.0, 1.0);
	vec4 ray_eye = glm::inverse(*projectionMatrix) * ray_clip;
	ray_eye = vec4 (ray_eye.x, ray_eye.y, -1.0, 0.0);
	vec3 ray_wor = vec3(glm::inverse(*cameraViewMatrix) * ray_eye);
	vec3 ray_wor_Norm = glm::normalize(ray_wor);

	bool intersects = false;

	float d;

	float denominator = glm::dot(vec3(0,1,0), ray_wor_Norm);
		
	vec3 cameraCurrentPosition = currentCam.getPosition();

	vec3 p0l0 = vec3(0,0,0) - cameraCurrentPosition;
	d = glm::dot(p0l0, vec3(0,1,0)) / denominator; 
	intersects = (d <= 0);
		
	vec3 ray_wor_Norm_Scaled = ray_wor_Norm * d;
	vec3 position;
	position = (cameraCurrentPosition + (ray_wor_Norm_Scaled));

	return position;
}

void NodeHandler::addNode(int mouseXposition, int mouseYposition, int width, int height)
{
	if(currentNode < maxnumberOfNodes)
	{
		vec3 NodePositon = rayToworldCalculator(mouseXposition, mouseYposition, width, height);

		bool foundUnUsedNode = false;
		for(int findUnUsedNode = 0; findUnUsedNode < NODE_STORAGE_SIZE && !foundUnUsedNode; findUnUsedNode++)
		{
			if(!nodeStorage[findUnUsedNode].used)
			{
				Node* workingNode = &nodeStorage[currentNode];
				workingNode->numberOfNode = currentNode++;
				workingNode->position = NodePositon;
				workingNode->used = true;
				foundUnUsedNode = true;
			}
		}
	}
}

void NodeHandler::addNode(vec3 position)
{
	if(currentNode < maxnumberOfNodes)
	{
		bool foundUnUsedNode = false;
		for(int findUnUsedNode = 0; findUnUsedNode < NODE_STORAGE_SIZE && !foundUnUsedNode; findUnUsedNode++)
		{
			if(!nodeStorage[findUnUsedNode].used)
			{
				Node* workingNode = &nodeStorage[currentNode];
				workingNode->numberOfNode = currentNode++;
				workingNode->position = position;
				workingNode->used = true;
				foundUnUsedNode = true;
			}
		}
	}
}

void NodeHandler::addNode(vec3 position, Node* TrackNode)
{
	if(currentNode < maxnumberOfNodes)
	{
		bool foundUnUsedNode = false;
		for(int findUnUsedNode = 0; findUnUsedNode < NODE_STORAGE_SIZE && !foundUnUsedNode; findUnUsedNode++)
		{
			if(!nodeStorage[findUnUsedNode].used)
			{
				//TrackNode = &nodeStorage[currentNode];
				TrackNode->numberOfNode = currentNode++;
				TrackNode->position = position;
				TrackNode->used = true;
				foundUnUsedNode = true;
			}
		}
	}
}

void NodeHandler::deleteNode()
{
	//off the MasterNode
	if(masterNodeSelected)
	{
		//delete arrows pointing to masternode
		for(int alterthisNodesArrows = 0; alterthisNodesArrows < currentNode; alterthisNodesArrows++)
		{
			Node* workingNode = &nodeStorage[alterthisNodesArrows];
			for(int alterThisArrow = 0; alterThisArrow < workingNode->currentArrowLink; alterThisArrow++)
			{
				if(arrowStorage[workingNode->arrowStroage[alterThisArrow]].pointingToNode == currentMasterNode->numberOfNode)
				{
					workingNode->arrowStroage[alterThisArrow] = workingNode->arrowStroage[workingNode->currentArrowLink - 1];
					workingNode->currentArrowLink--;
				}
			}
		}

		//remove arrows from masternode
		for(int alterThisArrow = 0; alterThisArrow < currentMasterNode->currentArrowLink; alterThisArrow++)
		{
			arrowStorage[currentMasterNode->arrowStroage[alterThisArrow]] = arrowStorage[currentArrow - 1];
			currentArrow--;
		}

		//set all other nodes to defalt
		for(int alterThisNode = 0; alterThisNode < currentMasterNode->currentLinkedNode; alterThisNode++)
		{
			nodeStorage[currentMasterNode->linkendNodeStorage[alterThisNode]].seconedNode = false;
		}
		
		//remove master node
		*currentMasterNode = nodeStorage[currentNode-1];
		currentNode--;
		masterNodeSelected = false;
	}
}

void NodeHandler::selectNode(int mouseXposition, int mouseYposition, int width, int height)
{
	
	float x = (2.0f * mouseXposition) / width - 1.0f;
	float y = 1.0f - (2.0f * mouseYposition) / height;
	float z = -1.0f;

	vec3 ray_nds = vec3 (x, y, z);
	vec4 ray_clip = vec4 (ray_nds.x, ray_nds.y, -1.0, 1.0);
	vec4 ray_eye = glm::inverse(*projectionMatrix) * ray_clip;
	ray_eye = vec4 (ray_eye.x, ray_eye.y, -1.0, 0.0);
	vec3 ray_wor = vec3(glm::inverse(*cameraViewMatrix) * ray_eye);

	//parts
	vec3 ray_wor_Norm = glm::normalize(ray_wor);
	vec3 cameraCurrentPosition = currentCam.getPosition();	
	//float lastDistance = 0.0f;
	float b;
	float c;
	Node* closestNode = NULL;
	bool aNodeWasFound = false;
	bool tempDChanged = false;

	float tempD = 0.0f;

	 for(int drawNode = 0; drawNode < currentNode; drawNode++)
	{
		vec3 camraMinNodePositions = cameraCurrentPosition - nodeStorage[drawNode].position;
		b = glm::dot(ray_wor_Norm, camraMinNodePositions);
		c = glm::dot(camraMinNodePositions, camraMinNodePositions) - shpereSize;
		
		float check = (b*b) - c;

		if(check > 0)
		{
			//Hit
			aNodeWasFound = true;
			float negative = (-b) - sqrt(check);
			float positive = (-b) + sqrt(check);

			if(negative < positive && !(negative < 0))
			{
				if(!tempDChanged)
				{
					tempDChanged = true;
					tempD = negative;
					closestNode = &nodeStorage[drawNode];
				}
				else if(negative <= tempD)
				{
					tempD = negative;
					closestNode = &nodeStorage[drawNode];
				}
			}
			else if(!(positive < 0))
			{
				if(!tempDChanged)
				{
					tempDChanged = true;
					tempD = positive;
					closestNode = &nodeStorage[drawNode];
				}
				else if(positive <= tempD)
				{
					tempD = positive;
					closestNode = &nodeStorage[drawNode];
				}

			}
		}
	}

	if(aNodeWasFound && !masterNodeSelected)
	{
		currentMasterNode = closestNode;
		currentMasterNode->MasterNode = true;
		masterNodeSelected = true;
		if(currentMasterNode->currentLinkedNode != 0)
		{
			 for(int likedNode = 0; likedNode < currentMasterNode->currentLinkedNode; likedNode++)
	    	 {
				 nodeStorage[currentMasterNode->linkendNodeStorage[likedNode]].seconedNode = true;
			 }
		}
	}
	else if(aNodeWasFound && masterNodeSelected)
	{
		if(closestNode->MasterNode)
		{
			resetNodes();
		}
		else if(!closestNode->seconedNode)
		{
			closestNode->seconedNode = true;
			currentMasterNode->linkendNodeStorage[currentMasterNode->currentLinkedNode++]=closestNode->numberOfNode;
			float length = 1.0f;
			addRay(closestNode, length);
		}
		else
		{
			closestNode->seconedNode = false;
			bool foundArrow = false;
			for(int alterThisArrow = 0; alterThisArrow < currentMasterNode->currentArrowLink && !foundArrow; alterThisArrow++)
			{
				if(arrowStorage[currentMasterNode->arrowStroage[alterThisArrow]].pointingToNode == closestNode->numberOfNode)
				{
					arrowStorage[currentMasterNode->arrowStroage[alterThisArrow]] = arrowStorage[currentArrow - 1];
					currentArrow--;

					currentMasterNode->arrowStroage[alterThisArrow] = currentMasterNode->arrowStroage[currentMasterNode->currentArrowLink- 1];
					currentMasterNode->currentArrowLink--;

					currentMasterNode->currentLinkedNode--;

					foundArrow = true;
				}
			}
		}
	}
	else
	{
		resetNodes();
	}

}

void NodeHandler::resetNodes()
{
	masterNodeSelected = false;

	for(int drawNode = 0; drawNode < currentNode; drawNode++)
	{
		nodeStorage[drawNode].MasterNode  = false;
		nodeStorage[drawNode].seconedNode = false;
	}
}

void NodeHandler::drawNodes()
{
	for(int drawNode = 0; drawNode < currentNode; drawNode++)
	{
		*mainNode = nodeStorage[drawNode].MasterNode;
		*seconedNode = nodeStorage[drawNode].seconedNode;
		nodeRenderable->translationMatrix = glm::translate(nodeStorage[drawNode].position);
		worldCreater->drawRenderable(nodeRenderable);
	}

	*mainNode = false;
	*seconedNode = false;

	if(masterNodeSelected && currentMasterNode->currentArrowLink != 0)
	{
		for(int drawArrow = 0; drawArrow < currentMasterNode->currentArrowLink; drawArrow++)
		{
			coneRenderable->scaleMatrix = arrowStorage[currentMasterNode->arrowStroage[drawArrow]].coneScaleMatrix;
			coneRenderable->rotationMatrix = arrowStorage[currentMasterNode->arrowStroage[drawArrow]].coneRoationMatrix;
			coneRenderable->translationMatrix = arrowStorage[currentMasterNode->arrowStroage[drawArrow]].coneTranslationMatrix;
			worldCreater->drawRenderable(coneRenderable);

			arrowRenderable->scaleMatrix = arrowStorage[currentMasterNode->arrowStroage[drawArrow]].arrowScaleMatrix;
			arrowRenderable->rotationMatrix = arrowStorage[currentMasterNode->arrowStroage[drawArrow]].arrowRoationMatrix;
			arrowRenderable->translationMatrix = arrowStorage[currentMasterNode->arrowStroage[drawArrow]].arrowTranslationMatrix;
			worldCreater->drawRenderable(arrowRenderable);
		}
	}

}

mat4 NodeHandler::CreateBasisFromOneVector(vec3* basis)
{
	vec3 z = glm::normalize(glm::cross(*basis,vec3(1,0,0)));
	vec3 y = glm::normalize(glm::cross(*basis,z));

	mat4 result(vec4(*basis,0), vec4(y,0),vec4(z,0), vec4(0,0,0,1));
	return result;
}

void NodeHandler::addRay(Node* toNode, float length)
{
	addRay(toNode, currentMasterNode, length);
}

void NodeHandler::addRay(Node* toNode, Node* fromNode, float length)
{
	vec3 to = toNode->position;
	vec3 from = fromNode->position;

	vec3 theLine = to - from;
	vec3 xBasis = glm::normalize(theLine);
	mat4 theRotation(CreateBasisFromOneVector(&xBasis));

	const float cylinderWeightLoss = 0.02f;
	glm::vec3 backOff = (length / 2) * xBasis;
	const float CONE_SCALE = 0.3f;
	glm::vec3 coneHeight = xBasis * (0.5f * 1);

	arrow temp;

	temp.arrowTranslationMatrix = glm::translate(from + backOff);
	temp.arrowScaleMatrix = 
		glm::scale(glm::length(theLine) - length - glm::length(coneHeight), 
		cylinderWeightLoss, cylinderWeightLoss);
	temp.arrowRoationMatrix = theRotation;

	temp.coneTranslationMatrix = glm::translate(to - backOff - coneHeight);
	temp.coneRoationMatrix = theRotation;
	temp.coneScale = CONE_SCALE;
	temp.coneScaleMatrix = glm::scale(CONE_SCALE, CONE_SCALE, CONE_SCALE);

	temp.arrowNumber = currentArrow;
	temp.used = true;
	temp.pointingToNode = toNode->numberOfNode;

	temp.cost = length;

	arrowStorage[currentArrow++] = temp;
	fromNode->arrowStroage[fromNode->currentArrowLink++] = temp.arrowNumber;
}

void NodeHandler::massReset()
{
	resetNodes();

	for(int targetNode = 0; targetNode < currentNode; targetNode++)
	{
		nodeStorage[targetNode].currentLinkedNode = 0;
		nodeStorage[targetNode].currentArrowLink = 0;
		nodeStorage[targetNode].used = false;

	/*	for(int currentNodeLink = 0; currentNodeLink < LINKED_NODE_STORAGE_SIZE; currentNodeLink++)
		{
			nodeStorage[targetNode].linkendNodeStorage[currentNodeLink] = 0;
			nodeStorage[targetNode].arrowStroage[currentNodeLink] = 0;
		}*/
	}

	for(int tragetArrow = 0; tragetArrow < currentArrow; tragetArrow++)
	{
		arrowStorage[tragetArrow].used = false;
	}

	currentNode = 0;
	currentArrow = 0;
}

NodeHandler::NodeHandler()
{
	
}

NodeHandler::NodeHandler(mat4* projection, mat4* cameraview, Render* currentRenderer, Camera setCam)
{
	projectionMatrix = projection;
	cameraViewMatrix = cameraview;
	worldCreater = currentRenderer;
	currentCam = setCam;
	Threshold = vec3(0,0,0);

	masterNodeSelected = false;

	currentNode = 0;
	currentArrow = 0;

	mainNode = worldCreater->uniformHandiler.addIntUniform("mainNode");
	seconedNode = worldCreater->uniformHandiler.addIntUniform("seconedNode");

	nodeShader = worldCreater->createShaderInfo("ShaderCode\\NodeVertexShader.lol", "ShaderCode\\NodeFragmentShader.lol");

	shpereSize = 0.3f;
	shpere = worldCreater->loadShape("bin\\shpere.bin");
	worldCreater->addGeometryData(&shpere);
	nodeRenderable = worldCreater->createRenderable(&shpere,&nodeShader, vec3(0,0,0),shpereSize, 0.0f,0.0f,0.0f, GL_TRIANGLES, 1);
	
	cylenderGI = worldCreater->loadShape("bin\\cylender.bin");
	worldCreater->addGeometryData(&cylenderGI);
	arrowRenderable = worldCreater->createRenderable(&cylenderGI,&nodeShader, vec3(0,0,0),1.0f, 0.0f,0.0f,0.0f, GL_TRIANGLES, 1);

	coneGI = worldCreater->loadShape("bin\\cone.bin");
	worldCreater->addGeometryData(&coneGI);
	coneRenderable = worldCreater->createRenderable(&coneGI,&nodeShader, vec3(0,0,0),1.0f, 0.0f,0.0f,0.0f, GL_TRIANGLES, 1);

	maxnumberOfNodes = 20;
}

NodeHandler::~NodeHandler()
{
	//delete nodeStorage;
}
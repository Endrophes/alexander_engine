#pragma once

#include <GL\glew.h>
#include <GLWindow.h>

#include <Qt\qdebug.h>
#include <gtx\transform.hpp>
#include <fstream>
#include <string>
#include <iostream>
#include <cstdlib>
#include <QtGui\QMouseEvent>
#include <QtGui\QKeyEvent>
#include <QtGui\QImage>
#include <QtGui\qimage.h>
#include <QtGui\QFileDialog>
#include <Random Generator\RandomGenerator.h>

#include <Particles\Particle\Particle.h>
#include <Particles\ParticleSystem\ParticleSystem.h>

//#include <ShapeData.h>
//#include <ShapeGenerator.h>

////New for Components
#include <Game Components\menuChoice.h>
#include <Game Components\RestToRandomPositionComp.h>
#include <Game Components\Thruster.h>
///

#include <Sliders\DebugSlider.h>

//#include <Debug Tools\DebugShapes\DebugShapes.h>

#ifdef PROFILING_ON
	#include <Debug Tools\Profiling\Profiler.h>
	#include <Debug Tools\Profiling\Profile.h>
#endif

#define ARRAY_SIZE(a) sizeof(a)/sizeof(*a)



#ifdef _DEBUG
	#include <Debug Tools\Memmory Maniger\DebugMemory.h>
#endif

using namespace std;
//using Neumont::ShapeGenerator;
//using Neumont::ShapeData;
//using Neumont::Vertex;
//DebugShapes debugShapes;

int mouseX, mouseY, theHeight, theWidth;

bool nodeplaced;
bool nodelinked;

void GLWindow::println(std::string output)
{
	cout << output << endl;
}

void GLWindow::print(std::string output)
{
	cout << output;
}

GLWindow::~GLWindow()
{
	//All the delete
	//delete currentCam;
	//delete Showlight;
	//delete SpecularMateral;
	//delete Scale;
	//delete alpha;
	//delete AmbientLight;
	//delete ColorObjects;
	//delete ColorLightBulb;
	//delete SpecularColor;
	//delete LightPosition;
	//delete CameraPosition;
	//delete TransformMatix;
	//delete CamraView;
	//delete SpeculerMatrix;
	//delete depthBias;
	//delete TranslationMatrix;
	//delete RotationMatrix;
	//delete WorldTransform;
	//delete ProjectionMatrix;
	//
	//delete cubeR;
	//cube.shutdown();
	//glUseProgram(0);

	cout<< "The closest was: " << closest << endl;
}

void GLWindow::initializeGL()
{
	glewInit();
	glEnable(GL_DEPTH_TEST);
	//glEnable(GL_CULL_FACE);
	//glCullFace(GL_BACK);
	glEnable(GL_BLEND);
	//glEnable(GL_POLYGON_SMOOTH);

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	setMouseTracking(true);
	//change backGroundColor
	int testWidth = width();
	int testHeight= height();

	renderer.setUpRender(testWidth, testHeight);
	renderer.specularMateral = 25;

	if(!input.initialize(&keyMapper, Input::MenuChoice::MAX))
	{
		//return;
	}
	currentCam = &panCamOne;


	closest = FLT_MAX;
	targetDistance = 0.4f;

	sendDataToHardware();
	setupEntites();

	gameSystem.inisialize();
}

void GLWindow::createShadders()
{
	/*////Example////
	
	//normalShaders    = renderer.createShaderInfo("ShaderCode\\VertexShaderCode.glsl", "ShaderCode\\FragmentShaderCode.glsl");
	//colorRemoveShaders = renderer.createShaderInfo("ShaderCode\\VertexShaderCode.glsl", "ShaderCode\\removeSpecalFragmentsSC.rfsc");

	TransparentShaders = renderer.createShaderInfo("ShaderCode\\VertexShaderCode.glsl", "ShaderCode\\TransparentSpecalFragmentsSC.rfsc");
	WhiteNormalMap = renderer.createShaderInfo("ShaderCode\\VertexShaderCode.glsl", "ShaderCode\\WhiteNormalMap.fsc");
	JamieShader = renderer.createShaderInfo("ShaderCode\\jamieShape.vsc", "ShaderCode\\JamieShape.fsc");
	obj2shader = renderer.createShaderInfo("ShaderCode\\objConverted.vsc", "ShaderCode\\objConverted.fsc");
	nosieShader = renderer.createShaderInfo("ShaderCode\\VertexShaderCode.glsl", "ShaderCode\\noise.fsc");
	objshader2 = renderer.createShaderInfo("ShaderCode\\objConverted2.vsc", "ShaderCode\\objConverted2.fsc");
	noiseHeightShader = renderer.createShaderInfo("ShaderCode\\noiseHeight.vsc", "ShaderCode\\noiseHeight.fsc");
	moniterColor = renderer.createShaderInfo("ShaderCode\\passTex.vsc", "ShaderCode\\passtex.fsc");
	moniterDepth = renderer.createShaderInfo("ShaderCode\\passTexDepth.vsc", "ShaderCode\\passtexDepth.fsc");
	moniterDepth2 = renderer.createShaderInfo("ShaderCode\\passTexDepth2.vsc", "ShaderCode\\passtexDepth2.fsc");
	nonalphaNormal = renderer.createShaderInfo("ShaderCode\\objConverted.vsc", "ShaderCode\\objNonNormal.fsc");
	shadowCaster = renderer.createShaderInfo("ShaderCode\\shadowCast.vsc", "ShaderCode\\shadowCast.fsc");
	enviroMap = renderer.createShaderInfo("ShaderCode\\VertexShaderCode.glsl", "ShaderCode\\InvertNormal.glsl");
	mirror = renderer.createShaderInfo("ShaderCode\\mirror.vsc", "ShaderCode\\mirror.fsc");
	TBpass = renderer.createShaderInfo("ShaderCode\\TBpass.vsc", "ShaderCode\\TBpass.fsc");

	/*/

	PassThroughShaders = renderer.createShaderInfo("Assets\\ShaderCode\\passThroughVertexShader.LOL", "Assets\\ShaderCode\\passThroughFragmentShader.LOL");
	nonalphaNormal = renderer.createShaderInfo("Assets\\ShaderCode\\objConverted.vsc", "Assets\\ShaderCode\\objNonNormal.fsc");
	ThrusterShader = renderer.createShaderInfo("Assets\\ShaderCode\\ThrusterShader.vsc", "Assets\\ShaderCode\\ThrusterShader.fsc");

}

void GLWindow::loadTextures()
{
	//debuging
	//int textSlotNum = -1;
	/*////Example////
	//
	//Oger 0-2 ogre_normalmap
	textSlotNum = renderer.addTexture("images\\Oger\\diffuse.png", "PNG");
	textSlotNum = renderer.addTexture("images\\Oger\\ao_ears.png", "PNG");
	textSlotNum = renderer.addTexture("images\\Oger\\ogre_normalmap.png", "PNG");

	textSlotNum = renderer.addTexture("images\\NormalMap\\Shapes.png", "PNG");
	textSlotNum = libnoise->createNoiseMap(256,256,&renderer);
	textSlotNum = renderer.addTexture("images\\CamaroText.png", "PNG");
	textSlotNum = renderer.addTexture("images\\gray.bmp");

	int H = height();
	int W = width();
	textSlotNum = renderer.addMoniterBuffers(256, 256);
	renderer.addDepthBuffer(&shadowFrameID, &shadowTexID, 9, 2048, 2048);
	textSlotNum = renderer.addCubeMap("images\\testMirror\\", "PNG", "posX.png", "negX.png", "posY.png", "negY.png", "posZ.png", "negZ.png");
	textSlotNum = renderer.addCubeMap("images\\Stairs\\png\\", "PNG", "posx.png", "negx.png", "posy.png", "negy.png", "posz.png", "negz.png");
	renderer.addTexture("images\\DMText.png", "PNG");
	renderer.addTexture("images\\EnterpriseTexture.png", "PNG");

	//renderer.addTexture("images\\MH.png", "PNG");
	//renderer.addTexture("images\\grid.bmp");
	//renderer.addTexture("images\\BlueWindowTransparent.png", "PNG");
	//renderer.addTexture("images\\BlueWall.png", "PNG");
	//renderer.addTexture("images\\DMText.png", "PNG");
	//renderer.addTexture("images\\Base.png", "PNG");	
	//*/

	int num = 0;
	num = renderer.addTexture("Assets\\images\\ViperTextuer.QImageBin");
	num = renderer.addTexture("Assets\\images\\RaderTexture.QImageBin");
	num = renderer.addTexture("Assets\\images\\HealthTexture.QImageBin");
	num = renderer.addTexture("Assets\\images\\EngineThrust.QImageBin");
	
}

void GLWindow::createUniforms()
{
	alpha = renderer.uniformHandiler.addFloatUniform("alpha");

	//////Needed
	AmbientLight     = renderer.uniformHandiler.addVec3Uniform("ambientLight");
	ColorObjects     = renderer.uniformHandiler.addVec3Uniform("colorObjects");
	ColorLightBulb   = renderer.uniformHandiler.addVec3Uniform("colorLightBulb");
	SpecularColor    = renderer.uniformHandiler.addVec3Uniform("specularColor");
	LightPosition    = renderer.uniformHandiler.addVec3Uniform("lightPosition");

	ProjectionMatrix = renderer.uniformHandiler.addMat4Uniform("projectionMatrix");
	TransformMatix   = renderer.uniformHandiler.addMat4Uniform("transfom");
					 
	SpecularMateral  = renderer.uniformHandiler.addFloatUniform("materialSpec");
					 
	CameraPosition   = renderer.uniformHandiler.addVec3Uniform("cameraPosition");
	CamraView        = renderer.uniformHandiler.addMat4Uniform("view");
	//////

	glm::vec3 white = glm::vec3(1.0f,1.0f,1.0f);

	////Defualts
	*AmbientLight = vec3(0.0f,0.0f,0.0f);
	*LightPosition = vec3(0.0f,1.2f,3.0f);
	*SpecularColor = white;
	*ColorObjects = white;
	*SpecularMateral = 0.5f;
	*alpha = 0.5f;
	*ProjectionMatrix = glm::perspective(60.0f, ((float)width())/height(), 0.1f, 200.0f);
	////
	
}

void GLWindow::loadShapes()
{
	////Example////
	//plane = renderer.loadShape("bin\\Plane.bin");
	/* ////Example////
	int numAtrib = 5;
	GLsizei strideB = (4 * sizeof(vec3)) + sizeof(vec2);
	Render::VertexAttrib newAtrib[] = 
	{
		//Position
		3,
		GL_FLOAT,
		GL_FALSE,
		strideB,
		0,

		//Normal
		3,
		GL_FLOAT,
		GL_FALSE,
		strideB,
		(sizeof(glm::vec3)),

		//UV
		2,
		GL_FLOAT,
		GL_FALSE,
		strideB,
		(2 * sizeof(glm::vec3)),

		//Tanget
		3,
		GL_FLOAT,
		GL_FALSE,
		strideB,
		(2 * sizeof(glm::vec3)) + sizeof(glm::vec2),

		//BiTanget
		3,
		GL_FLOAT,
		GL_FALSE,
		strideB,
		(3 * sizeof(glm::vec3)) + sizeof(glm::vec2)
	};
	testBase = renderer.loadShape("bin\\Ogre.ba", numAtrib, newAtrib);
	//*/
	
	#pragma region GeoPramiters
	int numAtrib = 5;
	GLsizei strideB = (4 * sizeof(vec3)) + sizeof(vec2);
	Render::VertexAttrib newAtrib[] = 
	{
		//Position
		3,
		GL_FLOAT,
		GL_FALSE,
		strideB,
		0,

		//Normal
		3,
		GL_FLOAT,
		GL_FALSE,
		strideB,
		(sizeof(glm::vec3)),

		//UV
		2,
		GL_FLOAT,
		GL_FALSE,
		strideB,
		(2 * sizeof(glm::vec3)),

		//Tanget
		3,
		GL_FLOAT,
		GL_FALSE,
		strideB,
		(2 * sizeof(glm::vec3)) + sizeof(glm::vec2),

		//BiTanget
		3,
		GL_FLOAT,
		GL_FALSE,
		strideB,
		(3 * sizeof(glm::vec3)) + sizeof(glm::vec2)
	};
	#pragma endregion GeoPramiters

	cubeGeo     = renderer.loadShape("Assets\\BAFiles\\Cube.ba", numAtrib, newAtrib);
	viperGeo    = renderer.loadShape("Assets\\BAFiles\\Viper.ba", numAtrib, newAtrib);
	raderGeo    = renderer.loadShape("Assets\\BAFiles\\CylonRader.ba", numAtrib, newAtrib);
	HealthPoint = renderer.loadShape("Assets\\BAFiles\\HealthPoint.ba", numAtrib, newAtrib);
	sign        = renderer.loadShape("Assets\\BAFiles\\ConeTest.ba", numAtrib, newAtrib);
	bullet      = renderer.loadShape("Assets\\BAFiles\\VipperBullet.ba", numAtrib, newAtrib);
	//renderer. makeCube();
}

void GLWindow::setupEntites()
{

	glm::vec3 position = glm::vec3();
	glm::vec3 rotation = glm::vec3();

	////**Component**////

	//new
	Entites::RendereComponent* heropRender = new Entites::RendereComponent();
	Entites::ControllerComponent* shipController = new Entites::ControllerComponent();
	Entites::BoomArm* arm = new Entites::BoomArm();
	//Entites::Engine* engine = new Entites::Engine();
	Entites::Turbo* turboBoot = new Entites::Turbo();
	Entites::CameraComponent* playerCam = new Entites::CameraComponent();
	//Entites::CameraEnity* playerCam = new Entites::CameraEnity();
	
	currentCam = playerCam;
	gameSystem.hero.forwardVector = glm::vec3(0,0,-1);

	turboBoot->setUp(0.3f, 3.0f);

	//add
	gameSystem.hero.addComponent(playerCam);
	gameSystem.hero.addComponent(shipController);
	gameSystem.hero.addComponent(heropRender);
	gameSystem.hero.addComponent(arm);
	//gameSystem.hero.addComponent(turboBoot);
	//gameSystem.hero.addComponent(engine);

	//setup
	position = glm::vec3(0.0f,0.0f,-10.0f);
	rotation = glm::vec3(0.0f, 180.0f, 0.0f);

	Render::Renderable* heroShip = renderer.createRenderable(&viperGeo, &nonalphaNormal, position, rotation, glm::vec3(0.2f, 0.2f, 0.2f), GL_TRIANGLES, 0, 0, 1);
	gameSystem.hero.position = position;
	gameSystem.hero.rotation = rotation;
	playerCam->setPositionOffSet(glm::vec3(0.0f, 0.0f, 10.0f));
	heropRender->setData(heroShip);

	arm->setup(&gameSystem.hero);
	shipController->initialize();

	//arm->addEnitieToArm(playerCam, glm::vec3(0.0f,0.0f,10.0f));


	//int   numParts = 2;
	//float maxLife = 10.0f;
	//float minLife = 5.0f;
	//float base = 1.0f;
	//float lifeTime = 10.0f;
	//float speed = 0.3f;
	//float maxSpeed = 0.3f;
	//float minSpeed = 0.1f;
	//float scale = 0.1f;
	//
	//position = glm::vec3(0.0, 0.0f, 0.0f);
	//rotation = glm::vec3(0.0f, 0.0f, 0.0f);
	//
	////make systems
	//for(uint step = 0; step < 3; step++)
	//{
	//	Entites::ParticleSystem* thruster = new Entites::ParticleSystem();
	//
	//	for(int num = 0; num < numParts; num++)
	//	{
	//		Entites::Particle particle;
	//		Render::Renderable* newSign = renderer.createRenderable(&sign, &nonalphaNormal, position, rotation, scale, GL_TRIANGLES, 0, 0, 4);
	//		particle.setData(newSign, glm::vec3(), thruster->position, lifeTime, speed);
	//
	//		thruster->particles.push_back(particle);
	//	}
	//
	//	thruster->setup(numParts, maxSpeed, minSpeed, glm::vec3(base, base, base), glm::vec3(-base, -base, -base), minLife, maxLife);
	//
	//	gameSystem.particleSystems.push_back(thruster);
	//}
	//
	//arm->addEnitieToArm(gameSystem.particleSystems[0], glm::vec3(0.0f, 1.0f, 1.0f));
	//arm->addEnitieToArm(gameSystem.particleSystems[1], glm::vec3(-1.0f, -1.0f, 1.0f));
	//arm->addEnitieToArm(gameSystem.particleSystems[2], glm::vec3(1.0f, -1.0f, 1.0f));

	position = glm::vec3(0.0f, 0.0f, -5.0f);
	rotation = glm::vec3(0.0f, 0.0f, 0.0f);

	float scaleMin = 0.1f;
	float scaleMax = 1.0f;

	//Thruster
	for(uint step = 0; step < 3; step++)
	{
		Entites::Thruster* engine = new Entites::Thruster();
		Render::Renderable* newThrust = renderer.createRenderable(&sign, &ThrusterShader, position, rotation, glm::vec3(scaleMin, scaleMin, scaleMin), GL_TRIANGLES, 0, 0, 4);
		engine->setup(0.1f,glm::vec3(scaleMin, scaleMin, scaleMin), glm::vec3(scaleMin, scaleMin, scaleMax), newThrust);
		gameSystem.Thrusters.push_back(engine);
	}
	
	float spaceX = 0.3f;
	float spaceY = 0.1f;
	float spaceYB = 0.2f;
	float spaceZ = 1.4f;

	arm->addEnitieToArm(gameSystem.Thrusters[0], glm::vec3(0.0f, spaceY, spaceZ));
	arm->addEnitieToArm(gameSystem.Thrusters[1], glm::vec3(-spaceX, -spaceYB, spaceZ));
	arm->addEnitieToArm(gameSystem.Thrusters[2], glm::vec3(spaceX, -spaceYB, spaceZ));


	//Health
	float startXPosition = -5.0f;
	float incrament = 1.0f;

	position = glm::vec3(startXPosition, 2.4f, -5.0f);
	rotation = glm::vec3(90.0f, 0.0f, -25.0f);

	for(uint step = 0; step < 4; step++)
	{
		position = glm::vec3(startXPosition, 2.4f, -5.0f);

		Entites::Ship* healthTick = new Entites::Ship();
		Entites::RendereComponent* healthRenderer = new Entites::RendereComponent();

		Render::Renderable* healthRenderable = renderer.createRenderable(&HealthPoint, &nonalphaNormal, position, rotation, glm::vec3(0.5f, 0.5f, 0.5f), GL_TRIANGLES, 3, 3, 3);
		healthRenderer->setData(healthRenderable);

		healthTick->rotation = rotation;
		healthTick->addComponent(healthRenderer);

		gameSystem.HealthBar.push_back(healthTick);

		position = glm::vec3(startXPosition, 2.4f, 5.0f);
		arm->addEnitieToArm(healthTick, position);

		startXPosition += incrament;
	}


	float baseX = 4.5f;
	float baseY = 2.4f;

	float maxX =  baseX;
	float minX = -baseX;
		
	float maxY =  baseY;
	float minY = -baseY;

	float minZ = -20.0f;
	float maxZ = -50.0f;

	glm::vec3 minSpace = glm::vec3(minX, minY, minZ);
	glm::vec3 maxSpace = glm::vec3(maxX, maxY, maxZ);

	position = glm::vec3(0.0f, 0.0f, 20.0f);
	rotation = glm::vec3(0.0f, 0.0f, 90.0f);

	for(uint step = 0; step < 20; step++)
	{
		//new
		Entites::torpedo* torpedo = new Entites::torpedo();
		Entites::RendereComponent* torpedoRender = new Entites::RendereComponent();
		Entites::MoveForward* newEngine = new Entites::MoveForward();
		Entites::ResetPosition* resetPost = new Entites::ResetPosition();

		//set
		Render::Renderable* torpedoRenderable = renderer.createRenderable(&bullet, &nonalphaNormal, position, rotation, glm::vec3(0.1f , 0.1f, 0.1f), GL_TRIANGLES, 3, 3, 3);
		torpedoRender->setData(torpedoRenderable);
		resetPost->initialize(position, -20.0f, 3);
		newEngine->initialize(-0.5f);

		//add
		//torpedo->addComponent();
		torpedo->addComponent(torpedoRender);
		torpedo->addComponent(resetPost);
		torpedo->addComponent(newEngine);

		//torpedos.push_back(torpedo);
		gameSystem.torpedos.push_back(torpedo);
	}


	rotation = glm::vec3(0.0f, 0.0f, 0.0f);

	for(uint step = 0; step < 20; step++)
	{
		//new
		Entites::Ship* enemyShip = new Entites::Ship();
		Entites::RendereComponent* shipRender = new Entites::RendereComponent();
		Entites::MoveForward* newEngine = new Entites::MoveForward();
		Entites::RestToRandomPositionComp* resetPostRand = new Entites::RestToRandomPositionComp();
		//Entites::ResetPosition* reseter = new Entites::ResetPosition();

		position = glm::vec3(Random->randomFloatInRange(minX, maxX), Random->randomFloatInRange(minY, maxY), Random->randomFloatInRange(minZ, maxZ));

		Render::Renderable* enemyRenderable = renderer.createRenderable(&raderGeo, &nonalphaNormal, position, rotation, glm::vec3(0.2f, 0.2f, 0.2f), GL_TRIANGLES, 1, 0, 2);
		enemyRenderable->visable = true;

		//setup
		shipRender->setData(enemyRenderable);
		enemyShip->position = enemyRenderable->Position;
		newEngine->initialize(0.2f);
		resetPostRand->initialize(minSpace, maxSpace, 5.0f, 3);
		//reseter->initialize(glm::vec3(0,0,0), start);
		
		//add
		enemyShip->addComponent(newEngine);
		enemyShip->addComponent(shipRender);
		//enemyShip->addComponent(reseter);
		enemyShip->addComponent(resetPostRand);

		//enemyShips.push_back(enemyShip);
		gameSystem.enemyShips.push_back(enemyShip);
	}




	//cube.addComponent(&shipController);
	//cube.addComponent(&cubeRender);
	//cube.position = cubeR->Position;

	////**Component**////
}


void GLWindow::sendDataToHardware(){
	cout<<"Sending data to hardware"<<endl;
	
	//jamieCude = makeCube();
	//planeV = makePlane();

	createShadders();
	loadShapes();
	loadTextures();
	createUniforms();
}

void GLWindow::paintGL()
{
	int _width  = width()  ;
	int _height = height() ;

	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
	glViewport(0,0,_width,_height);
	glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

	if(gameSystem.hero.isAlive())
	{
		Entites::RendereComponent* tempA = gameSystem.hero.getComponent<Entites::RendereComponent>();
		renderer.drawRenderable(tempA->renderable);
	}

	for(int step = 0; step < gameSystem.hero.getLife(); step++)
	{
		Entites::Ship* current = gameSystem.HealthBar.at(step);
		Entites::RendereComponent* tempA = current->getComponent<Entites::RendereComponent>();
		renderer.drawRenderable(tempA->renderable);
	}

	uint size = 0;

	size = (gameSystem.enemyShips.size());
	for(uint step = 1; step < size; step++) {

		Entites::Ship* current = gameSystem.enemyShips.at(step);
		Entites::RendereComponent* tempB = current->getComponent<Entites::RendereComponent>();
		bool drawIt = current->hit;


		if(!drawIt)
		{
			renderer.drawRenderable(tempB->renderable);
		}
	}

	size = gameSystem.torpedos.size();
	for(uint step = 1; step < size; step++) 
	{
		Entites::torpedo* current = gameSystem.torpedos.at(step);

		if(current->fired)
		{
			//cout<< "Draw torpedo" << endl;
			//current->update();
			Entites::RendereComponent* tempB = current->getComponent<Entites::RendereComponent>();
			renderer.drawRenderable(tempB->renderable);
		}
	}

	//for(uint step = 0; step < (gameSystem.particleSystems.size()); step++)
	//{
	//	Entites::ParticleSystem* current = gameSystem.particleSystems[step];
	//	for(uint stepb = 0; stepb < (current->particles.size()); stepb++)
	//	{
	//		Entites::Particle* currentParticle = &current->particles[stepb];
	//		if(currentParticle->isAlive())
	//		{
	//			renderer.drawRenderable(currentParticle->getRenderable());
	//		}
	//	}
	//	
	//}

	for(uint step = 0; step < (gameSystem.Thrusters.size()); step++)
	{
		Entites::Thruster* current = gameSystem.Thrusters[step];
		renderer.drawRenderable(current->renderable);
	}

	//renderer.drawAllRenderables();
}

void GLWindow::mouseMoveEvent(QMouseEvent* e)
{
	mouseX = e->x();
	mouseY = e->y();
	theWidth = width();
	theHeight = height();

	if(GetAsyncKeyState(VK_LBUTTON))
	{
		panCamOne.mouseUpdate(glm::vec2(mouseX, mouseY));
	}
}

void GLWindow::keyPressEvent(QKeyEvent*e)
{
	e;
}

void GLWindow::moveDebugCam()
{
	if(GetAsyncKeyState(VK_LBUTTON))
	{
		if(input.activeActionThisFrame(Input::MenuChoice::W))
		{
			panCamOne.moveForward();
		}
		if(input.activeActionThisFrame(Input::MenuChoice::S))
		{
			panCamOne.moveBack();
		}
		if(input.activeActionThisFrame(Input::MenuChoice::A))
		{
			panCamOne.moveLeft();
		}
		if(input.activeActionThisFrame(Input::MenuChoice::D))
		{
			panCamOne.moveRight();
		}
		if(input.activeActionThisFrame(Input::MenuChoice::Q))
		{
			panCamOne.moveUp();
		}
		if(input.activeActionThisFrame(Input::MenuChoice::E))
		{
			panCamOne.moveDown();
		}
	}
}

void GLWindow::update()
{
	moveDebugCam();

	int _width  = width()  ;
	int _height = height() ;
	
	renderer.setWidthAndHeight(_width, _height);
	
	///NEED///
	*CamraView = currentCam->getWorldToViewMatrix();
	*CameraPosition = currentCam->getPosition();
	*ProjectionMatrix = glm::perspective(60.0f, ((float)_width)/_height, 0.1f, 200.0f);
	//////////
	
	//debugShapes.updateDebug();

	//Only need
	gameSystem.update();

	paintGL();
	swapBuffers();
}




/*
void GLWindow::createRenderables()
{
	///Example//// OLD
	/////DoomsDay
	//renderer.createRenderable(&plane,&TransparentShaders,glm::vec3(0.0f,1.0f,3.0f),3.0f,90.0f,0.0f,180.0f,GL_TRIANGLES,0,0,5);
	//renderer.createRenderable(&shpere,&TransparentShaders,glm::vec3(0.0f,1.0f,0.0f),1.5f,0.0f,0.0f,0.0f,GL_TRIANGLES,4,0.5f);
	//renderer.createRenderable(&doomsDay,&TransparentShaders,glm::vec3(0.0f,1.0f,0.0f),1.0f,0.0f,145.0f,20.0f,GL_TRIANGLES,6);
	//renderer.createRenderable(&base,&TransparentShaders,glm::vec3(0.0f,-1.0f,0.0f),2.0f,0.0f,-90.0f,0.0f,GL_TRIANGLES,7);
	//Moniters
	//colorMoniter = renderer.createRenderable(&planeV,&moniterColor,glm::vec3(0.0f,0.0f,0.0f),0.2f,0.0f,0.0f,0.0f,GL_TRIANGLES,5,5,8,1,1);
	//depthMoniter = renderer.createRenderable(&planeV,&moniterDepth,glm::vec3(0.0f,0.0f,0.0f),0.2f,0.0f,0.0f,0.0f,GL_TRIANGLES,5,5,9,1,1);
	//depthMoniterCamtwo = renderer.createRenderable(&planeV,&moniterDepth2,glm::vec3(0.0f,0.0f,0.0f),0.2f,0.0f,0.0f,0.0f,GL_TRIANGLES,5,5,10,1,1);
	

	//NEW
	//cubeR = renderer.createRenderable(&cubeGeo, &PassThroughShaders, glm::vec3(0.0f,0.0f,-5.0f), 0.2f, 0.0f, 0.0f, 0.0f, GL_TRIANGLES, 0, 0, 0);

	//Test
	//renderer.createRenderable(&cubeGeo, &PassThroughShaders, glm::vec3(0.0f,2.0f,-50.0f), 0.2f, 0.0f, 0.0f, 0.0f, GL_TRIANGLES, 0, 0, 0);

}
//*/

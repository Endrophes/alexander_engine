#include <Qt\qapplication.h>
#include <GL\glew.h>

#include <GLWindow.h>
#include <Display\Display.h>

#ifdef _DEBUG
	#include <Debug Tools\Memmory Maniger\DebugMemory.h>
#endif


Display* mainDisplay;

int main(int argc, char* argv[])
{
	int code = 0;
	#ifdef _DEBUG
		// faster
		//_CrtSetDbgFlag( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
		//slower
		_CrtSetDbgFlag( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF | _CRTDBG_CHECK_ALWAYS_DF );
	#endif

	QApplication app(argc, argv);
	
	mainDisplay = new Display();
	mainDisplay->showMaximized();

	code = app.exec();
	
	delete mainDisplay;

	return code;
}
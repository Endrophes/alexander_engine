#include <ships\ship.h>
#include <Audio\Audio.h>

namespace Entites
{
	void Ship::finalSetup()
	{
		//this->position = render.renderable->Position;
		//this->addComponent(&render);
	}

	void Ship::setUp(int amountOfLife)
	{
		life = amountOfLife;
	}

	void Ship::takeHit(int damage)
	{
		life -= damage;
		if(life <= 0 )
		{
			dead = true;
			audio.playFile("EX", false);
		}
	}

	Ship::Ship()
	{
		life = 3;
		dead = false;
	}

	Ship::~Ship()
	{

	}

	int Ship::getLife()
	{
		return life;
	}


	bool Ship::isAlive()
	{
		bool result = true;

		if(dead)
		{
			result = false;
		}
		
		return result;
	}


	void Ship::setLife(int amount)
	{
		life = amount;
	}

	void Ship::revive()
	{
		dead = false;
	}

}
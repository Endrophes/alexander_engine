#pragma once

#include <Enity\Entity.h>
//#include <Components\RendererComponent.h>

namespace Entites
{

	class Ship : public Entity
	{
		int life;
		bool dead;
	public:
		//RendereComponent render;
		Ship();
		~Ship();
		void setUp(int amountOfLife);
		void takeHit(int damage);
		void finalSetup();
		void setLife(int amount);
		int getLife();
		bool isAlive();
		void revive();

	};

}
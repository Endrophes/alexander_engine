#pragma once

#ifndef GAMEMANAGER
#define GAMEMANAGER

#include <Enity\Entity.h>
#include <Component\Component.h>

//Components
#include <Components\PhysicsComponent.h>
#include <Components\RendererComponent.h>

#include <Input\KeyInput.h>
#include <Game Components\KeyMapper.h>
#include <Game Components\ControllerComponent.h>
#include <Game Components\moveForwardComponent.h>
#include <Game Components\ResetPosition.h>
#include <Game Components\Thruster.h>
//

//
#include <Particles\Particle\Particle.h>
#include <Particles\ParticleSystem\ParticleSystem.h>
//

//Enities
#include <ships\ship.h>
#include <torpedos\torpedo.h>
///

namespace Game
{

	class GameManager
	{
		static const int base = 1;

		int gameState;
		float targetDistance;

		//debugging
		float closest;

		float decrament;
		float maxDelay;
		float fireDelay;

		void gameUpdate();

		float getLengthOfVector(glm::vec3 vec);
		bool checkDistance(glm::vec3 locationOne, glm::vec3 locationTwo, float targetDistance);
		void checkForCollition();


	public:

		GameManager(void);
		~GameManager(void);

		enum gameStates
		{
			Splash  =  (base << 0),
			Game    =  (base << 1),
			End     =  (base << 2),
			MAX     =  (base << 3),
		};

		void setState(int newState);

		void keyCheck();

		void update();

		void inisialize();

		/// add Objcts here

		Entites::Ship hero;

		std::vector<Entites::Ship*> HealthBar;
		std::vector<Entites::Ship*> enemyShips;
		std::vector<Entites::torpedo*> torpedos;
		std::vector<Entites::ParticleSystem*> particleSystems;
		std::vector<Entites::Thruster*> Thrusters;
		//////////////////////

	};

}
#endif
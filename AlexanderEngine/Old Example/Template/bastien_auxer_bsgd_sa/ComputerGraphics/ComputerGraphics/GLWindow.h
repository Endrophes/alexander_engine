#pragma once

#include <glm.hpp>
#include <gl\glew.h>
#include <QtOpenGL\qglwidget>
#include <Libnoise\Libnoise.h>
#include <Camera\Camera.h>
#include <Camera\MoveCam\MoveCam.h>
#include <Render\Render.h>

#include <vector>

////New for Components
#include <Enity\Entity.h>
#include <Component\Component.h>
#include <Components\PhysicsComponent.h>
#include <Components\RendererComponent.h>
#include <Components\CameraComponent.h>
#include <Components\BoomArm.h>

#include <Entites\CameraEnity.h>

#include <Input\KeyInput.h>
#include <Game Components\KeyMapper.h>
#include <Game Components\ControllerComponent.h>
#include <Game Components\moveForwardComponent.h>
#include <Game Components\ResetPosition.h>
#include <Game Components\Turbo.h>
#include <Game Components\Engine.h>

#include <ships\ship.h>
#include <torpedos\torpedo.h>
///

/// New Game Manager
#include <GameManager.h>
///

using glm::vec2;
using glm::vec3;
using glm::vec4;
using glm::mat3;
using glm::mat4;

class GLWindow : public  QGLWidget
{

public:

	Game::GameManager gameSystem;

	float targetDistance;

	struct Vertex
	{
		glm::vec3 position;
		glm::vec4 color;
		glm::vec3 normal;
		glm::vec2 uv;
		glm::vec3 tanget;
		glm::vec3 BiTanget;
		static uint POSITION_OFFSET;
		static uint COLOR_OFFSET;
		static uint NORMAL_OFFSET;
		static uint UV_OFFSET;
		static uint STRIDE;
	};

	struct Vertex2D
	{
		glm::vec2 position;
		glm::vec2 uv;
		static uint POSITION_OFFSET;
		static uint UV_OFFSET;
		static uint STRIDE;
	};

	Render renderer;
	Camera* currentCam;
	Camera cameraOne;
	MoveCam panCamOne;
	//Camera cameraTwo;
	//Camera cameraThree;
	//Camera cameraFour;

	Input::KeyMapper keyMapper;

	glm::mat4 depthMVP;
	glm::mat4 biasMatrix;

	Render::GeomitryInfo cubeGeo;
	Render::GeomitryInfo viperGeo;
	Render::GeomitryInfo raderGeo;
	Render::GeomitryInfo HealthPoint;
	Render::GeomitryInfo sign;
	Render::GeomitryInfo bullet;

	Render::shaderInfo PassThroughShaders;
	Render::shaderInfo nonalphaNormal;
	Render::shaderInfo ThrusterShader;

	/* //Example//
	Render::Renderable* level;
	Render::Renderable* whiteCube;
	Render::Renderable* flatNormPlane;
	Render::Renderable* Jaime;
	Render::Renderable* oger;

	Render::Renderable* Camaro;

	Render::Renderable* nosiePlane;
	Render::Renderable* noiseHeightPlane;

	Render::Renderable* colorMoniter;
	Render::Renderable* depthMoniter;
	Render::Renderable* depthMoniterCamtwo;
	Render::Renderable* tea;
	Render::Renderable* planeShadow;
	Render::Renderable* world;
	Render::Renderable* ball;
	Render::Renderable* DoomsDay;
	Render::Renderable* Enterprise;
	Render::Renderable* Mario;

	Render::shaderInfo normalShaders;
	Render::shaderInfo PassThroughShaders;
	Render::shaderInfo colorRemoveShaders;
	Render::shaderInfo TransparentShaders;
	Render::shaderInfo WhiteNormalMap;
	Render::shaderInfo JamieShader;
	Render::shaderInfo obj2shader;
	Render::shaderInfo nosieShader;
	Render::shaderInfo noiseHeightShader;
	Render::shaderInfo objshader2;
	Render::shaderInfo moniterColor;
	Render::shaderInfo moniterDepth;
	Render::shaderInfo nonalphaNormal;
	Render::shaderInfo moniterDepth2;
	Render::shaderInfo shadowCaster;
	Render::shaderInfo enviroMap;
	Render::shaderInfo mirror;
	Render::shaderInfo TBpass;

	Render::GeomitryInfo plane;
	Render::GeomitryInfo planeMV;
	Render::GeomitryInfo box;
	Render::GeomitryInfo shpere;
	Render::GeomitryInfo base;
	Render::GeomitryInfo jamieCude;
	Render::GeomitryInfo testBase;
	Render::GeomitryInfo car;
	Render::GeomitryInfo planeV;
	Render::GeomitryInfo teapot;
	Render::GeomitryInfo inverseCube;
	Render::GeomitryInfo newCube;
	Render::GeomitryInfo newSphere;
	Render::GeomitryInfo doomsDay;
	Render::GeomitryInfo enterprise;
	Render::GeomitryInfo mario;
	*/
	/* //Example//
	LibNoiseManager* libnoise;

	bool DefaultSetting;
	bool showPlane;
	bool showPlaneNoise;
	bool showPlaneNoiseHeight;
	bool showCube;
	bool showOger;
	bool showCar;
	bool OgerNormal;
	bool OgerAmbient;
	bool OgerDiffuse;
	bool ShowShadow;

	bool useCamTwo;
	bool DisplayLight;
	bool objectDiform;

	bool showDepth;
	bool showColor;

	bool showCool;

	bool showEnviMap;
	*/
	/* //Example//
	float xRotation;
	float yRotation;
	float zRotation;
	float octive1;
	float octive2;
	float octive3;
	float threshHoldSlider;
	float texterSelect;

	unsigned int shadowFrameID;
	unsigned int shadowTexID;

	//Uniform Value Pointers
	float* SpecularMateral;

	float* octiveLevel;
	float* octiveLevel3;
	float* octiveLevelHeight;

	float* threshHold;
	float* scaleDeform;
	float deformScale;

	int* OgerNormalcolor;
	int* OgerAmbintlight;
	int* OgerDiffusecolor;
	int* diformObject;
		  
	vec3*  AmbientLight;
	vec3*  ColorObjects;
	vec3*  ColorLightBulb;
	vec3*  SpecularColor;
	vec3*  LightPosition;
	vec3*  CameraPosition;
	
	vec2* translate;
	vec2* translate2;
	vec2* translate3;
	
	*/

	int*   Showlight;
	float* SpecularMateral;
	float* Scale;
	float* alpha;

	vec3*  AmbientLight;
	vec3*  ColorObjects;
	vec3*  ColorLightBulb;
	vec3*  SpecularColor;
	vec3*  LightPosition;
	vec3*  CameraPosition;

	mat4*  TransformMatix;
	mat4*  CamraView;
	mat4*  SpeculerMatrix;
	mat4*  depthBias;
	mat4*  TranslationMatrix;
	mat4*  RotationMatrix;
	mat4*  WorldTransform;
	mat4*  ProjectionMatrix;

	void sendDataToHardware();
	void keyCheck();
	void update();
	~GLWindow();

	//debugging
	float closest;

	void println(std::string output);
	void print(std::string output);

	//
	//mat4*  TranslationMatrix;
	//mat4*  RotationMatrix;
	//mat4*  WorldTransform;
	//Scale = renderer.uniformHandiler.addFloatUniform("scale");
	//TranslationMatrix = renderer.uniformHandiler.addMat4Uniform("translation");
	//RotationMatrix = renderer.uniformHandiler.addMat4Uniform("rotation");
	//WorldTransform = renderer.uniformHandiler.addMat4Uniform("modelToWorld");

protected:

	void moveDebugCam();

	float getLengthOfVector(glm::vec3 vec);
	bool checkDistance(glm::vec3 locationOne, glm::vec3 locationTwo, float targetDistance);
	void checkForCollition();


	void initializeGL();
	void setupEntites();
	void paintGL();
	void mouseMoveEvent(QMouseEvent*);
	void keyPressEvent(QKeyEvent*);

	void createShadders();
	void loadShapes();
	//void createRenderables();
	void loadTextures();
	void createUniforms();

	void calculateGeoTagets(Vertex* data);

	vec3 tangetCalculator();
	
};


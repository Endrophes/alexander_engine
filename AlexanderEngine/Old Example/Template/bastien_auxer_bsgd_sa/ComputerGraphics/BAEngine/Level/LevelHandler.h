#pragma once

#ifndef LEVELHADLER
#define LEVELHADLER

#include "..\GLWindow.h"
#include "..\Render\Render.h"

class LevelHandler
{
	Render* worldCreater;
	Render::GeomitryInfo Level;
	Render::shaderInfo LevelShader;
	Render::Renderable* LevelRenderable;
	
	void createLevel();

	void drawLevel();

	LevelHandler();
};



#endif
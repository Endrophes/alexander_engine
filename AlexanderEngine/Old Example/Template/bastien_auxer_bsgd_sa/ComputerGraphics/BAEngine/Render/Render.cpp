#include <Render\Render.h>
#include <string>
#include <Qt\qdebug.h>
#include <fstream>
#include <iostream>
#include <cstdlib>
#include <exception>

#ifdef _DEBUG
	#include <Debug Tools\Memmory Maniger\DebugMemory.h>
#endif

#ifdef PROFILING_ON
	//#include <Debug Tools\Profiling\Profiler.h>
	//#include <Debug Tools\Profiling\Profile.h>
#endif

QImage Render::importImage(char* file, char* format)
{
	QImage image;

	//std::cout<<"Importing Image: " << file <<std::endl;
	if(!image.load(file, format))
	{
		std::cout<<"Image failed to load: "<< file <<std::endl;
	}
	else
	{
		image = QGLWidget::convertToGLFormat(image);
	}
	
	return image;
}

GLuint Render::addTexterInfoToBuffer(GLvoid* textureBytes, uint width, uint height)
{
	GLuint textureID;

	glActiveTexture(GL_TEXTURE0 + currentID);

	glGenTextures(1, &textureID);
	glBindTexture(GL_TEXTURE_2D, textureID);
	glTexImage2D(GL_TEXTURE_2D, 0,GL_RGBA,width,height, 0, GL_RGBA,GL_UNSIGNED_BYTE, textureBytes);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	return currentID++;
}

GLuint Render::addTexture(QImage imageFile)
{
	Texture temp;
	imageFile = QGLWidget::convertToGLFormat(imageFile);
	temp.textureID = addTexterInfoToBuffer((GLvoid*)imageFile.bits(), (uint)imageFile.width(), (uint)imageFile.height());
	textureStorage[currentTextureSlot++] = temp;
	return temp.textureID;
}

GLuint Render::addCubeMap(std::string folderLocation, std::string fileType, std::string posX, std::string negX, std::string posY, std::string negY, std::string posZ, std::string negZ)
{
	GLuint textureID;

	glActiveTexture(GL_TEXTURE0 + currentID);

	glGenTextures(1, &textureID);
	glBindTexture(GL_TEXTURE_CUBE_MAP, textureID);

	QImage positveX  = importImage((char*)(folderLocation + posX).c_str(), (char*)fileType.c_str()).mirrored(false,true);
	QImage negativeX = importImage((char*)(folderLocation + negX).c_str(), (char*)fileType.c_str()).mirrored(false,true);
	QImage positveY  = importImage((char*)(folderLocation + posY).c_str(), (char*)fileType.c_str()).mirrored(false,true);
	QImage negativeY = importImage((char*)(folderLocation + negY).c_str(), (char*)fileType.c_str()).mirrored(false,true);
	QImage positveZ  = importImage((char*)(folderLocation + posZ).c_str(), (char*)fileType.c_str()).mirrored(false,true);
	QImage negativeZ = importImage((char*)(folderLocation + negZ).c_str(), (char*)fileType.c_str()).mirrored(false,true);

	//positveX.mirrored(false,true);
	//negativeX.invertPixels(QImage::InvertMode::InvertRgba);
	//positveY.invertPixels(QImage::InvertMode::InvertRgba);
	//negativeY.invertPixels(QImage::InvertMode::InvertRgba);
	//positveZ.invertPixels(QImage::InvertMode::InvertRgba);
	//negativeZ.invertPixels(QImage::InvertMode::InvertRgba);

	int width = positveX.width();
	int height = positveX.height();

	glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, positveX.bits());
	glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, negativeX.bits());
	glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, positveY.bits());
	glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, negativeY.bits());
	glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, positveZ.bits());
	glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, negativeZ.bits());

	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

	return currentID++;
}

GLuint Render::addTexture(GLvoid* textureBytes, uint width, uint height)
{
	Texture temp;
	temp.textureID = addTexterInfoToBuffer(textureBytes, width, height);
	textureStorage[currentTextureSlot++] = temp;
	return temp.textureID ;
}

int Render::addTexture(char* filepath)
{
	std::ifstream file;
	file.open(filepath, std::ofstream::binary | std::ios::in);
	
	file.seekg(0, std::ios::end);
	int size = (int)file.tellg();
	file.seekg(0, std::ios::beg);

	char* bites = new char[size];
	file.read(bites, size);
	file.close();

	int offset = 0;
	int* height = reinterpret_cast<int*>(bites + offset);
	offset += sizeof(int);
	int* width = reinterpret_cast<int*>(bites + offset);
	offset += sizeof(int);
	char* bits =  (bites + offset);

	Texture temp;
	temp.textureID = addTexterInfoToBuffer((GLvoid*)bits, (uint)*width, (uint)*height);
	textureStorage[currentTextureSlot++] = temp;

	delete[] bites;
	return temp.textureID;
}

GLuint Render::addMoniterBuffers(int height, int width)
{
	currentID = moniter.createFrameBuffers(currentID, height, width);
	currentID += 2;
	return currentID;
}

void Render::addDepthBuffer(unsigned int* bufferID, unsigned int* textureID, int textureSlot, int height, int width)
{
	moniter.createFrameBufferWithTexture(bufferID, textureID, textureSlot, GL_DEPTH_ATTACHMENT, GL_DEPTH_COMPONENT32, GL_DEPTH_COMPONENT, GL_FLOAT, height, width);
	currentID++;
}

void Render::getUnifomLocations(GLuint programID)
{
	translationLocation = glGetUniformLocation(programID, "translation");
	rotationLocation = glGetUniformLocation(programID, "rotation");
	scaleLocation = glGetUniformLocation(programID, "scale");
	worldTransformationLocation = glGetUniformLocation(programID, "modelToWorld");
}

void Render::addShapeDataWithColor2(BufferInfo* buffer, GeomitryInfo* shape)
{
	/*buffer;
	shape;*/
	glBindBuffer(GL_ARRAY_BUFFER, buffer->bufferID);

	buffer->offSet = shape->vertexDataByteOffSet;

	glGenVertexArrays(1, &shape->VertexArrayObjectID);
	glBindVertexArray(shape->VertexArrayObjectID);
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, buffer->bufferID);
	uint positionOffset = 0;
	uint colorOffset    = sizeof(glm::vec3);
	GLsizei stride = sizeof(glm::vec3) * 2;
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, stride, (void*)(buffer->offSet + positionOffset));
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, stride, (void*)(buffer->offSet + colorOffset));
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer->bufferID);
}

void Render::addShapeDataWithColor(BufferInfo* buffer, GeomitryInfo* shape)
{
	/*buffer;
	shape;*/
	glBindBuffer(GL_ARRAY_BUFFER, buffer->bufferID);

	buffer->offSet = shape->vertexDataByteOffSet;

	glGenVertexArrays(1, &shape->VertexArrayObjectID);
	glBindVertexArray(shape->VertexArrayObjectID);
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
	glEnableVertexAttribArray(3);
	glBindBuffer(GL_ARRAY_BUFFER, buffer->bufferID);
	uint positionOffset = 0;
	uint colorOffset    = 12;
	uint normalOffset   = 28;
	uint uvOffset       = 40;
	GLsizei stride = sizeof(glm::vec3) * 2 + sizeof(glm::vec2) + sizeof(glm::vec4);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, stride, (void*)(buffer->offSet + positionOffset));
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, stride, (void*)(buffer->offSet + colorOffset));
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, stride, (void*)(buffer->offSet + normalOffset));
	glVertexAttribPointer(3, 2, GL_FLOAT, GL_FALSE, stride, (void*)(buffer->offSet + uvOffset));
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer->bufferID);
}

void Render::addShapeData(BufferInfo* buffer, GeomitryInfo* shape)
{
	glBindBuffer(GL_ARRAY_BUFFER, buffer->bufferID);

	buffer->offSet = shape->vertexDataByteOffSet;

	glGenVertexArrays(1, &shape->VertexArrayObjectID);
	glBindVertexArray(shape->VertexArrayObjectID);
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
	glBindBuffer(GL_ARRAY_BUFFER, buffer->bufferID);
	uint positionOffset = 0;
	uint normalOffset   = sizeof(glm::vec3);
	uint uvOffset       = sizeof(glm::vec3) * 2;
	GLsizei stride = sizeof(glm::vec3) * 2 + sizeof(glm::vec2);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, stride, (void*)(buffer->offSet + positionOffset));
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, stride, (void*)(buffer->offSet + normalOffset));
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, stride, (void*)(buffer->offSet + uvOffset));
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer->bufferID);
}

void Render::addShapeData(BufferInfo* buffer, GeomitryInfo* shape, int numVertexAttrib, Render::VertexAttrib bufferData[])
{
	glBindBuffer(GL_ARRAY_BUFFER, buffer->bufferID);

	buffer->offSet = shape->vertexDataByteOffSet;

	glGenVertexArrays(1, &shape->VertexArrayObjectID);
	glBindVertexArray(shape->VertexArrayObjectID);

	for(int i = 0; i < numVertexAttrib; i++)
	{
		glEnableVertexAttribArray(i);
	}

	glBindBuffer(GL_ARRAY_BUFFER, buffer->bufferID);

	for(int i = 0; i < numVertexAttrib; i++)
	{
		glVertexAttribPointer(i, bufferData[i].size, bufferData[i].type, bufferData[i].normalize, bufferData[i].stride, (void*)(buffer->offSet + bufferData[i].offset));
	}

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer->bufferID);
}

void Render::addSubdata(BufferInfo* buffer, GeomitryInfo* shape)
{
	glBindBuffer(GL_ARRAY_BUFFER, buffer->bufferID);

	glBufferSubData(GL_ARRAY_BUFFER, buffer->offSet, shape->vertexBufferSize, shape->vertsData);

	shape->vertexDataByteOffSet = buffer->offSet;
	buffer->offSet += shape->vertexBufferSize;

	glBufferSubData(GL_ARRAY_BUFFER, buffer->offSet, shape->indexBufferSize, shape->indicesData);

	shape->IndecesDataByteOffSet = buffer->offSet;
	buffer->offSet += shape->indexBufferSize;
}

void Render::generateBuffer(BufferInfo* buffer)
{
	GLuint bufferID;
	glGenBuffers(1, &bufferID);
	glBindBuffer(GL_ARRAY_BUFFER, bufferID);
	glBufferData(GL_ARRAY_BUFFER, 0x100000, 0, GL_STATIC_DRAW);
	buffer->bufferID = bufferID;
}

void Render::generateBuffer(BufferInfo* buffer, GLsizeiptr size)
{
	GLuint bufferID;
	glGenBuffers(1, &bufferID);
	glBindBuffer(GL_ARRAY_BUFFER, bufferID);
	glBufferData(GL_ARRAY_BUFFER, size, 0, GL_STATIC_DRAW);
	buffer->bufferID = bufferID;
}

void Render::addGeometryData(GeomitryInfo* shape, bool withColor , bool onlyTwoInfo)
{
	bool dataIsNotPlaced = true;
	BufferInfo& activeBuffer = BufferStorage[currentBufferStorageSlot];

	while(dataIsNotPlaced)
	{
		activeBuffer = BufferStorage[currentBufferStorageSlot];

		GLsizeiptr size = (shape->vertexBufferSize + shape->indexBufferSize);

		if((shape->vertexBufferSize + shape->indexBufferSize) >= 0x100000)
		{	
			currentBufferStorageSlot++;
			activeBuffer = BufferStorage[currentBufferStorageSlot];

			generateBuffer(&activeBuffer, size);
			
			shape->bufferID = activeBuffer.bufferID;
			addSubdata(&activeBuffer, shape);

			activeBuffer.remaingSpace = 0;

			currentBufferStorageSlot--;
			dataIsNotPlaced = false;
		}

		else if(activeBuffer.offSet == 0)
		{
			generateBuffer(&activeBuffer);
			shape->bufferID = activeBuffer.bufferID;

			addSubdata(&activeBuffer, shape);

			activeBuffer.remaingSpace = 0x100000 - (shape->vertexBufferSize + shape->indexBufferSize);
			
			dataIsNotPlaced = false;
		}

		else if (activeBuffer.remaingSpace >= (shape->vertexBufferSize + shape->indexBufferSize))
		{
			shape->bufferID = activeBuffer.bufferID;

			addSubdata(&activeBuffer, shape);

			activeBuffer.remaingSpace -= (shape->vertexBufferSize + shape->indexBufferSize);

			dataIsNotPlaced = false;
		}

		else
		{
			currentBufferStorageSlot++;
		}

		if(withColor)
		{
			addShapeDataWithColor(&activeBuffer, shape);
		}
		else if(onlyTwoInfo)
		{
			addShapeDataWithColor2(&activeBuffer, shape);
		}
		else
		{
			addShapeData(&activeBuffer, shape);
		}

	}
}
void Render::addGeometryDataToBuffer(GeomitryInfo* shape, int numVertexAttrib, Render::VertexAttrib bufferData[])
{
	bool dataIsNotPlaced = true;
	BufferInfo& activeBuffer = BufferStorage[currentBufferStorageSlot];

	while(dataIsNotPlaced)
	{
		activeBuffer = BufferStorage[currentBufferStorageSlot];

		GLsizeiptr size = (shape->vertexBufferSize + shape->indexBufferSize);

		if((shape->vertexBufferSize + shape->indexBufferSize) >= 0x100000)
		{	
			currentBufferStorageSlot++;
			activeBuffer = BufferStorage[currentBufferStorageSlot];

			generateBuffer(&activeBuffer, size);
			
			shape->bufferID = activeBuffer.bufferID;
			addSubdata(&activeBuffer, shape);

			activeBuffer.remaingSpace = 0;

			currentBufferStorageSlot--;
			dataIsNotPlaced = false;
		}

		else if(activeBuffer.offSet == 0)
		{
			generateBuffer(&activeBuffer);
			shape->bufferID = activeBuffer.bufferID;

			addSubdata(&activeBuffer, shape);

			activeBuffer.remaingSpace = 0x100000 - (shape->vertexBufferSize + shape->indexBufferSize);
			
			dataIsNotPlaced = false;
		}

		else if (activeBuffer.remaingSpace >= (shape->vertexBufferSize + shape->indexBufferSize))
		{
			shape->bufferID = activeBuffer.bufferID;

			addSubdata(&activeBuffer, shape);

			activeBuffer.remaingSpace -= (shape->vertexBufferSize + shape->indexBufferSize);

			dataIsNotPlaced = false;
		}
		else
		{
			currentBufferStorageSlot++;
		}
	}

	addShapeData(&activeBuffer, shape, numVertexAttrib, bufferData);

}

Render::GeomitryInfo  Render::createGeomitry(
	int numVertexAttrib, 
	Render::VertexAttrib bufferData[],
	uint vertexBufferSize,
	uint indexBufferSize,
	uint numIndices,
	void* vertsData,
	void* indicesData
)
{
	GeomitryInfo temp;
	temp.vertexBufferSize = vertexBufferSize;
	temp.indexBufferSize = indexBufferSize;
	temp.numIndices = numIndices;
	temp.vertsData = vertsData;
	temp.indicesData = indicesData;

	addGeometryDataToBuffer(&temp, numVertexAttrib, bufferData);

	return temp;
}

Render::GeomitryInfo Render::createGeomitry(
	uint vertexBufferSize, uint indexBufferSize, uint numIndices,
	void* vertsData, void* indicesData, bool withColor
	)
{
	GeomitryInfo temp;
	temp.vertexBufferSize = vertexBufferSize;
	temp.indexBufferSize = indexBufferSize;
	temp.numIndices = numIndices;
	temp.vertsData = vertsData;
	temp.indicesData = indicesData;

	addGeometryData(&temp, withColor);

	return temp;
}

Render::GeomitryInfo Render::createGeomitry2
	(
	uint vertexBufferSize, uint indexBufferSize, uint numIndices,
	void* vertsData, void* indicesData, bool withColor, bool onlyTwoParts)
{
	GeomitryInfo temp;
	temp.vertexBufferSize = vertexBufferSize;
	temp.indexBufferSize = indexBufferSize;
	temp.numIndices = numIndices;
	temp.vertsData = vertsData;
	temp.indicesData = indicesData;

	addGeometryData(&temp, withColor, onlyTwoParts);
	return temp;
}

Render::Renderable* Render::createRenderable(GeomitryInfo* shape, shaderInfo* shaders, vec3 position, glm::vec3 size,
											 float rotateX, float rotateY, float rotateZ, GLenum drawMode, int normalmMap, int alphaMap, int textOption, float alpha, int layer)
{
	Renderable temp;
	temp.gemomitry = shape;
	temp.shadder = shaders;

	temp.scaleMatrix= glm::scale(size);
	temp.translationMatrix = glm::translate(position);
	temp.rotationMatrix  = glm::rotate(rotateX, vec3(1.0,0,0));
	temp.rotationMatrix *= glm::rotate(rotateY, vec3(0,1.0,0));
	temp.rotationMatrix *= glm::rotate(rotateZ, vec3(0,0,1.0));
	temp.modelToWorldMatrix = temp.translationMatrix * temp.rotationMatrix * temp.scaleMatrix;
	
	temp.Position = position;

	temp.texture = textOption;
	temp.alphaMap = alphaMap;
	temp.normalMap = normalmMap;

	temp.scale = size;
	temp.visable = true;
	temp.DrawMode = drawMode;
	temp.alpha = alpha;

	temp.layer = layer;

	renderableStorage[currentRenderableSlot] = temp;
	return &renderableStorage[currentRenderableSlot++];
}

Render::Renderable* Render::createRenderable(GeomitryInfo* shape, shaderInfo* shaders, vec3 position, vec3 rotation, vec3 size,
		GLenum drawMode, int normalmMap, int alphaMap, int textOption, float alpha, int layer)
{
	Renderable temp;
	temp.gemomitry = shape;
	temp.shadder = shaders;

	temp.scaleMatrix= glm::scale(size);
	temp.translationMatrix = glm::translate(position);
	temp.rotationMatrix  = glm::rotate(rotation.x, vec3(1.0,0,0));
	temp.rotationMatrix *= glm::rotate(rotation.y, vec3(0,1.0,0));
	temp.rotationMatrix *= glm::rotate(rotation.z, vec3(0,0,1.0));
	temp.modelToWorldMatrix = temp.translationMatrix * temp.rotationMatrix * temp.scaleMatrix;
	
	temp.Position = position;

	temp.texture = textOption;
	temp.alphaMap = alphaMap;
	temp.normalMap = normalmMap;

	temp.scale = size;
	temp.visable = true;
	temp.DrawMode = drawMode;
	temp.alpha = alpha;

	temp.layer = layer;

	renderableStorage[currentRenderableSlot] = temp;
	return &renderableStorage[currentRenderableSlot++];
}

void Render::setActiveTexture(int texture)
{
	glBindTexture(GL_TEXTURE_2D, textureStorage[texture].textureID);
}

void Render::sendUniforms(Renderable* object)
{
	
	*Scale = object->scale;
	*TranslationMatrix = object->translationMatrix;
	*RotationMatrix = object->rotationMatrix;
	*WorldTransform = object->modelToWorldMatrix;


	uniformHandiler.sendUniforms(object->shadder->ProgramID);

}

void Render::drawRenderable(Renderable* object)
{
	glUseProgram(object->shadder->ProgramID);
	glBindBuffer(GL_ARRAY_BUFFER, object->gemomitry->bufferID);
	glBindVertexArray(object->gemomitry->VertexArrayObjectID);

	if(object->alpha == 1.0f)
	{
		glDisable(GL_BLEND);
		glDepthMask(GL_TRUE);
	}
	else
	{
		glEnable(GL_BLEND);
		glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glDepthMask(GL_FALSE);
	}

	*Scale = object->scale;
	*alpha = object->alpha;
	*TranslationMatrix = object->translationMatrix;
	*RotationMatrix = object->rotationMatrix;
	*WorldTransform = object->modelToWorldMatrix;
	*scaleMatrix = object->scaleMatrix;

	//if(object->texture > 3)
	//{
	//	*texture = object->texture;
	//}
	//else
	//{
	//	*texture = object->texture - 1;
	//}
	//if(object->alphaMap > 3)
	//{
	//	*normaltex = object->alphaMap;
	//}
	//else
	//{
	//	*normaltex = object->alphaMap - 1;
	//}
	//if(object->normalMap > 3)
	//{
	//	*alphatex = object->normalMap;
	//}
	//else
	//{
	//	*alphatex = object->normalMap - 1;
	//}

	int mins = 1;
	uniformHandiler.sendIntUniform(object->shadder->ProgramID, object->texture - mins, "textureSelection");
	uniformHandiler.sendIntUniform(object->shadder->ProgramID, object->normalMap - mins, "normalSelection");
	uniformHandiler.sendIntUniform(object->shadder->ProgramID, object->alphaMap - mins, "alphaSelection");

	uniformHandiler.sendUniforms(object->shadder->ProgramID);

	glDrawElements(object->DrawMode, object->gemomitry->numIndices, GL_UNSIGNED_SHORT, (void*)object->gemomitry->IndecesDataByteOffSet);
}

void Render::postProcessEffects()
{

}

void Render::drawRenderableTransform(Renderable* object, mat4 transform)
{

}

void Render::drawTobuffer(int bufferID, int viewportHeight, int viewportWidth)
{
	//Draw renderbales
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, bufferID);
	glViewport(0,0,viewportWidth,viewportHeight);
	glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

	for(int currentRenderable = 0; currentRenderable < currentRenderableSlot; currentRenderable++)
	{
		if(renderableStorage[currentRenderable].visable)

		{
			drawRenderable(&renderableStorage[currentRenderable]);
		}
	}
}

void Render::drawTobuffer(int bufferID, int viewportHeight, int viewportWidth, int layer)
{
	//Draw renderbales
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, bufferID);
	glViewport(0,0,viewportWidth,viewportHeight);
	glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

	for(int currentRenderable = 0; currentRenderable < currentRenderableSlot; currentRenderable++)
	{
		if(renderableStorage[currentRenderable].visable && renderableStorage[currentRenderable].layer == layer)

		{
			drawRenderable(&renderableStorage[currentRenderable]);
		}
	}
}

void Render::drawAllRenderables()
{
	//Draw renderbales
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
	glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
	glClearColor(0.0f,0.0f,0.0f,1.0f);

	for(int currentRenderable = 0; currentRenderable < currentRenderableSlot; currentRenderable++)
	{
		//if(renderableStorage[currentRenderable].visable && renderableStorage[currentRenderable].layer == layer)
		//{
			drawRenderable(&renderableStorage[currentRenderable]);
		//}
	}

}

void Render::drawRenderablesOnLayer(int layer)
{
	//Draw renderbales
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
	glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
	glClearColor(0.0f,0.0f,0.0f,1.0f);

	for(int currentRenderable = 0; currentRenderable < currentRenderableSlot; currentRenderable++)
	{
		if(renderableStorage[currentRenderable].visable && renderableStorage[currentRenderable].layer == layer)
		{
			drawRenderable(&renderableStorage[currentRenderable]);
		}
	}
}

void Render::drawRenderable2(Renderable* object)
{
	glUseProgram(object->shadder->ProgramID); 
	glBindBuffer(GL_ARRAY_BUFFER, object->gemomitry->bufferID);
	glBindVertexArray(object->gemomitry->VertexArrayObjectID);

	//getLocations
	getUnifomLocations(object->shadder->ProgramID);

	//setTexture
	setActiveTexture(object->texture);

	sendUniforms(object);

	glDrawElements(GL_LINES, object->gemomitry->numIndices, GL_UNSIGNED_SHORT, (void*)object->gemomitry->IndecesDataByteOffSet);
}

void Render::drawGeomotry(GeomitryInfo* shape, shaderInfo* shaders, vec3 position, vec4 color, float size,
				 float rotateX, float rotateY, float rotateZ, int textOption, bool solidShape)
{
	Renderable temp;
	temp.gemomitry = shape;
	temp.shadder = shaders;

	glm::mat4 scaleMatrix = glm::scale(vec3(size,size,size));
	glm::mat4 translationMatrix = glm::translate(position);
	glm::mat4 rotationMatrix = glm::rotate(rotateX, vec3(1.0,0,0));
	rotationMatrix *= glm::rotate(rotateY, vec3(0,1.0,0));
	rotationMatrix *= glm::rotate(rotateZ, vec3(0,0,1.0));
	glm::mat4 worldTransform = translationMatrix * rotationMatrix * scaleMatrix;

	glUseProgram(shaders->ProgramID);
	glBindBuffer(GL_ARRAY_BUFFER, shape->bufferID);
	glBindVertexArray(shape->VertexArrayObjectID);

	int colorLocation = glGetUniformLocation(shaders->ProgramID, "color");
	glUniform4fv(colorLocation, 1, &color[0]);

	int selectorLocation = glGetUniformLocation(shaders->ProgramID, "isSolid");
	glUniform1i(selectorLocation, solidShape);

	temp.modelToWorldMatrix = worldTransform;
	temp.translationMatrix = translationMatrix;
	temp.rotationMatrix = rotationMatrix;
	temp.texture = textOption;
	temp.scale = glm::vec3(size, size, size);

	if(solidShape)
	{
		temp.DrawMode = GL_TRIANGLES;
	}
	else
	{
		temp.DrawMode = GL_LINES;
	}

	drawRenderable(&temp);

}

void Render::setUpRender(GLsizei TheWidth, GLsizei TheHeight)
{
	currentTextureSlot = 0;
	currentUniformSlot = 0;
	currentRenderableSlot = 0;
	currentBufferStorageSlot = 0;

	width = TheWidth;
	height = TheHeight;

}

void Render::setWidthAndHeight(GLsizei TheWidth, GLsizei TheHeight)
{
	width = TheWidth;
	height = TheHeight;
}

std::string readShaderCode(const char* fileName){
	std::ifstream meInput(fileName);
	if(!meInput.good())
	{
		std::cout << "Unable to find file: " << fileName <<std::endl;
		exit(1);
	}

	return std::string(
		std::istreambuf_iterator<char>(meInput),
		std::istreambuf_iterator<char>());

}

GLuint Render::compileShaders(const char* vertexShadder, const char* fragmentShadder)
{
	GLuint VetexShaderID = glCreateShader(GL_VERTEX_SHADER);
	GLuint fragShaderID = glCreateShader(GL_FRAGMENT_SHADER);

	const char* adaptor[1];
	std::string ShaderCodeFile = readShaderCode(vertexShadder);
	std::string FragmentCodeFile = readShaderCode(fragmentShadder);

	adaptor[0] = ShaderCodeFile.c_str();
	glShaderSource(VetexShaderID, 1, adaptor, 0);
	adaptor[0] = FragmentCodeFile.c_str();
	glShaderSource(fragShaderID, 1, adaptor, 0);

	glCompileShader(VetexShaderID);
	glCompileShader(fragShaderID);

	GLint compileStatus;
	glGetShaderiv(VetexShaderID, GL_COMPILE_STATUS, &compileStatus);
	if(compileStatus != GL_TRUE)
	{
		GLint logLength;
		glGetShaderiv(VetexShaderID, GL_INFO_LOG_LENGTH, &logLength);
		char* buffer = new char[logLength];
		GLsizei bitBucket;
		glGetShaderInfoLog(VetexShaderID, logLength, &bitBucket, buffer);
		qDebug() << buffer;
		delete [] buffer;
	}

	glGetShaderiv(fragShaderID, GL_COMPILE_STATUS, &compileStatus);
	if(compileStatus != GL_TRUE)
	{
		GLint logLength;
		glGetShaderiv(fragShaderID, GL_INFO_LOG_LENGTH, &logLength);
		char* buffer = new char[logLength];
		GLsizei bitBucket;
		glGetShaderInfoLog(fragShaderID, logLength, &bitBucket, buffer);
		qDebug() << buffer;
		delete [] buffer;
	}


	GLuint program = glCreateProgram();
	glAttachShader(program, VetexShaderID);
	glAttachShader(program, fragShaderID);

	glLinkProgram(program);

	glGetProgramiv(program, GL_LINK_STATUS, &compileStatus);
	if(compileStatus != GL_TRUE)
	{
		GLint logLength;
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &logLength);
		char* buffer = new char[logLength];
		GLsizei bitBucket;
		glGetProgramInfoLog(program, logLength, &bitBucket, buffer);
		qDebug() << buffer;
		delete [] buffer;
	}

	glUseProgram(program);


	return program;
}

Render::shaderInfo Render::createShaderInfo(const char* vertexShadder, const char* fragmentShadder)
{
	shaderInfo temp;
	temp.ProgramID = compileShaders(vertexShadder, fragmentShadder);
	return temp;
}

Render::GeomitryInfo Render::loadShape(char* filePath)
{
	char* pointer = fileLoader.loadBinaryIntoBuffer(filePath);
	 
	int* numberOFverts = reinterpret_cast<int*>(pointer);
	pointer = pointer + 4;
	int* numberOfIndecs = reinterpret_cast<int*>(pointer);
	pointer = pointer + 4;

	int numVerts = *numberOFverts;
	int numIndeces = *numberOfIndecs;

	Vertex* vetexData = reinterpret_cast<Vertex*>(pointer);
	pointer = pointer + (numVerts * sizeof(Vertex));

	ushort* indexData = reinterpret_cast<ushort*>(pointer);
	pointer = pointer + (numIndeces * sizeof(ushort));

	///GLWindow window;
	GeomitryInfo temp;
	temp.numIndices = (uint)numIndeces;
	temp.numVerts = (uint)numVerts;
	temp.vertexBufferSize = sizeof(Vertex) * numVerts;
	temp.indexBufferSize = sizeof(ushort) * temp.numIndices;
	temp.vertsData = vetexData;
	temp.indicesData = indexData;

	addGeometryData(&temp);

	return temp;
}

Render::GeomitryInfo Render::loadShape(char* filePath, int numVertexAttrib, Render::VertexAttrib bufferData[])
{
	char* buffer;

	FILE * file;
	unsigned long size;
	
	#pragma warning( disable: 4996)

	char full[_MAX_PATH];
	_fullpath(full, filePath, _MAX_PATH);

	file = fopen(full, "rb");
	if(file == NULL)
	{
		//fputs("could not load Mesh file: " + *filePath, stderr);

		std::cout << "could not load Mesh file: " << filePath << std::endl;
		std::cout<<"Press [Enter] to continue . . .";
		std::cin.get();

		exit(1);
	}

	//get file size
	fseek(file, 0, SEEK_END);
	size = ftell (file);
	rewind(file);

	//Allocate memory
	buffer = new char[sizeof(char)*size];
	//buffer = (char*)malloc(sizeof(char)*size);
	if (buffer == NULL) 
	{
		fputs ("Memory error ",stderr); 
		exit(2);
	}

	//copyinfo
	size_t result = fread(buffer,1, size, file);
	if (result != size) 
	{
		//DARN
		fputs ("Reading error ",stderr); 
		exit(3);
	}

	//close file
	fclose (file);


	char* pointer = buffer;

	int* numberOFverts = reinterpret_cast<int*>(pointer);
	pointer = pointer + 4;
	int* numberOfIndecs = reinterpret_cast<int*>(pointer);
	pointer = pointer + 4;
	
	int numVerts = *numberOFverts;
	int numIndeces = *numberOfIndecs;
	
	VertexTB* vetexData = reinterpret_cast<VertexTB*>(pointer);
	pointer = pointer + (numVerts * sizeof(VertexTB));
	
	ushort* indexData = reinterpret_cast<ushort*>(pointer);
	pointer = pointer + (numIndeces * sizeof(ushort));
	
	///GLWindow window;
	GeomitryInfo temp;
	temp.numIndices = (uint)numIndeces;
	temp.numVerts = (uint)numVerts;
	temp.vertexBufferSize = sizeof(VertexTB) * numVerts;
	temp.indexBufferSize = sizeof(ushort) * temp.numIndices;
	temp.vertsData = vetexData;
	temp.indicesData = indexData;
	
	addGeometryDataToBuffer(&temp, numVertexAttrib, bufferData);

	delete[] buffer;

	return temp;
}

void Render::reset()
{
	//resetBuffers
	BufferInfo& activeBuffer = BufferStorage[currentBufferStorageSlot];

	for(int selectedBuffer = 0; selectedBuffer < currentBufferStorageSlot; selectedBuffer++)
	{
		activeBuffer = BufferStorage[selectedBuffer];
		activeBuffer.offSet = 0;
		activeBuffer.remaingSpace = 0x100000; //1 MB
	}

	currentBufferStorageSlot = 0;
	currentGeomitrySlot = 0;
	currentRenderableSlot = 0;
	currentShadderSlot = 0;
	currentTextureSlot = 0;
	currentUniformSlot = 0;
}

Render::Render()
{
	Scale = uniformHandiler.addVec3Uniform("scale");
	alpha = uniformHandiler.addFloatUniform("alpha");
	TranslationMatrix = uniformHandiler.addMat4Uniform("translation");
	RotationMatrix = uniformHandiler.addMat4Uniform("rotation");
	WorldTransform = uniformHandiler.addMat4Uniform("modelToWorld");
	scaleMatrix = uniformHandiler.addMat4Uniform("scaleMatrix");

	currentID = 0;

	moniter;

	//texture = uniformHandiler.addIntUniform("textureSelection");
	//normaltex = uniformHandiler.addIntUniform("normalSelection");
	//alphatex = uniformHandiler.addIntUniform("alphaSelection");
}

Render::~Render()
{
	//delete Scale;
	//delete alpha;
	//delete TranslationMatrix;
	//delete RotationMatrix;
	//delete WorldTransform;
	//delete scaleMatrix;
	//delete texture;
	//delete normaltex;
	//delete alphatex;
}
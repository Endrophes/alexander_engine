#pragma once

#include <vector>
#include <iostream>
#include <noise/noise.h>
#include "..\noiseutils\noiseutils.h"
#include "..\ExportHeader.h"

class Render;

class ENGINE_SHARED LibNoiseManager
{
	
	//std::vector<> storage;

	noise::module::Perlin perlin;
	utils::NoiseMap heightMap;
	utils::NoiseMapBuilderPlane heightMapBuilder;

public:

	struct RGBA
	{
		unsigned char red;
		unsigned char green;
		unsigned char blue;
		unsigned char alpha;
	};

	LibNoiseManager(int height, int width)
	{
		heightMapBuilder.SetSourceModule (perlin);
		heightMapBuilder.SetDestNoiseMap (heightMap);
		heightMapBuilder.SetDestSize (width, height);
		heightMapBuilder.SetBounds (2.0, 6.0, 1.0, 5.0);
	}

	LibNoiseManager(int height, int width, double boundA, double boundB, double boundC, double boundD)
	{
		heightMapBuilder.SetSourceModule (perlin);
		heightMapBuilder.SetDestNoiseMap (heightMap);
		heightMapBuilder.SetDestSize (width, height);
		heightMapBuilder.SetBounds (boundA, boundB, boundC, boundD);
		heightMapBuilder.Build ();
	}

	noise::module::Perlin getPerlinModule();

	double getPerlinValue(double a, double b, double c);

	unsigned int createNoiseMap(unsigned int height,unsigned int width, Render* render);
	//void createNoiseMap(unsigned int height,unsigned int width);

};
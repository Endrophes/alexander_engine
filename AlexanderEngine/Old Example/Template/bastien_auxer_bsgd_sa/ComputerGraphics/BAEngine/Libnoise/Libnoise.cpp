#include "Libnoise.h"
#include "..\Render\Render.h"

#ifdef _DEBUG
	#include "..\Debug Tools\Memmory Maniger\DebugMemory.h"
#endif

noise::module::Perlin LibNoiseManager::getPerlinModule()
{
	return noise::module::Perlin();
}

double LibNoiseManager::getPerlinValue(double a, double b, double c)
{
	return perlin.GetValue(a,b,c);
}

unsigned int LibNoiseManager::createNoiseMap(unsigned int height,unsigned int width, Render* render)
{
	/*double d1 = perlin.GetValue(0,0,0);
	double d2 = perlin.GetValue(0,0,0);
	double d3 = perlin.GetValue(0,0,0);
	double d4 = perlin.GetValue(0,0,0);*/

	//const unsigned int BYTES_PER_COLOR = 4;
	//int numPixels = height * width;
	//int numBytesRow = height * BYTES_PER_COLOR;
	//int numBytesCol = width * BYTES_PER_COLOR;
	RGBA texture[256][256];
	//RGBA red = {0xFF, 0x00, 0x00, 0x00};
	//texture[0][0] = red;
	//texture[0][1] = red;
	//texture[1][0] = red;
	//texture[1][1] = red;


	QImage nosieImage(2, 2, QImage::Format::Format_ARGB32);
	nosieImage.setPixel(0,0,0xFF0000);
	nosieImage.setPixel(0,1,0xFF0000);
	nosieImage.setPixel(1,0,0xFF0000);
	nosieImage.setPixel(1,1,0xFF0000);

	for(unsigned int col = 0; col < width; col++)
	{
		for(unsigned int row = 0; row < height; row++)
		{
			RGBA* pixel = &texture[row][col];
			float x = 0.0f;
			
			double rowHeight = (double)row / height;
			double colWidth = (double)col / width;
			
			perlin.SetOctaveCount(4);
			x = perlin.GetValue(rowHeight, colWidth, 0);
			x = ((x + 1)/2) * 255;
			pixel->red = x;
			
			perlin.SetOctaveCount(5);
			x = perlin.GetValue(rowHeight, colWidth, 0);
			x = ((x + 1)/2) * 255;
			pixel->green = x;
			
			perlin.SetOctaveCount(6);
			x = perlin.GetValue(rowHeight, colWidth, 0);
			x = ((x + 1)/2) * 255;
			pixel->blue  = x;
			
			perlin.SetOctaveCount(7);
			x = perlin.GetValue(rowHeight, colWidth, 0);
			x = ((x + 1)/2) * 255;
			pixel->alpha  = x;
		}
	}

	/*for(unsigned int col = 0; col < 2; col++)
	{
		for(unsigned int row = 0; row < 2; row++)
		{
			QRgb test = nosieImage.pixel(row,col);
			std::cout << "Red: " << qRed(test) << " Green: " << qGreen(test) << " Blue: " << qBlue(test) << std::endl;
		}
	}*/
	std::cout << "End of line" << std::endl;

	//nosieImage.save("D:\\qimageTest.bmp",(const char*)"bmp",1);
	//if(!nosieImage.load("D:\\TG.png","png"))
	//{
	//	std::cout<<"Image failed to load: "<< "D:\\TG.png" <<std::endl;
	//}

	//QImage nosieImage = QImage((uchar*)&texture[0][0], 256, 256, QImage::Format::Format_RGB32);
	//QRgb test = nosieImage.pixel(0,0);
	//nosieImage = QGLWidget::convertToGLFormat(nosieImage);

	/*for(unsigned int col = 0; col < width; col++)
	{
		for(unsigned int row = 0; row < height; row++)
		{
			RGBA* pixel = &texture[row][col];

			std::cout<<"pixel at [" << col <<"]" << " " <<  "[" << row << "] "  << " R: " << (int)pixel->red  << " G: " << (int)pixel->green << " B: " << (int)pixel->blue << " A: " << (int)pixel->alpha <<std::endl;
		}
	}*/

	//return render->addTexture((GLvoid*)nosieImage.bits(), nosieImage.width(), nosieImage.height());
	//return render->addTexture(texture,2,2);
	//return render->addTexture(nosieImage);

	return render->addTexture((GLvoid*)texture, (uint)256, (uint)256);
}
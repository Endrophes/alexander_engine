#pragma once

#ifndef RANDOM
#define RANDOM


#include <stdlib.h>
#include <Math\Vector2D.h>
#include <ExportHeader.h>

class RandomNumber{
public:	

	ENGINE_SHARED RandomNumber(){}

	ENGINE_SHARED ~RandomNumber(){}

	ENGINE_SHARED int randomInt();
	ENGINE_SHARED double randomDouble();
	ENGINE_SHARED float randomFloat();

	ENGINE_SHARED int randomIntInRange(int min, int max);
	ENGINE_SHARED double randomDoubleInRange(double min, double max);
	ENGINE_SHARED float randomFloatInRange(float min, float max);

	ENGINE_SHARED Vector2D randomVector2D();
	

};



#endif
#pragma once

#ifndef VECTOR3
#define VECTOR3

#include <iostream>
#include <math.h>

struct Vector3D{
public:
	float X;
	float Y;
	float Z;

	explicit Vector3D(float x = 0, float y = 0, float z = 0) : X(x), Y(y),  Z(z)
	{
		this -> X = x;
		this -> Y = y;
		this -> Z = z;
	}

	~Vector3D(){
	
	}

	inline operator const float*(){
		return &X;
	}

	inline Vector3D PerpCW(){
		return Vector3D(-Y, X);
	}

	inline Vector3D PerpCCW(){
		return Vector3D(Y, -X);
	}

	inline Vector3D Normalized(){
		Vector3D result;
		float length = this->Length();
		result.X = (this->X/length);
		result.Y = (this->Y/length);
		result.Z = (this->Z/length);
		return result;
	}

	inline float Length(){
		return sqrt((this->X * this->X) + (this->Y * this->Y) + (this->Z * this->Z));
	}

	inline float LengthSquared(){
		float length = Length(); 
		return (length * length);
	}

	inline float Dot(const Vector3D& b){
		// a * b
		return (this->X*b.X)+(this->Y*b.Y)+(this->Z*b.Z);
	}

};

inline float Dot(const Vector3D& a, const Vector3D& b){
	// a * b
	return (a.X*b.X)+(a.Y*b.Y)+(a.Z*b.Z);
}

inline std::ostream& operator<<(std::ostream& stream, const Vector3D& right){
	std::cout << "{" << right.X << ", " << right.Y << ", " << right.Z << "}" << std::endl;
	return stream;
}

inline Vector3D operator+(const Vector3D& left, const Vector3D& right){
	return Vector3D(left.X + right.X, left.Y + right.Y, left.Z + right.Z);
}

inline Vector3D operator-(const Vector3D& left, const Vector3D& right){
	return Vector3D(left.X - right.X, left.Y - right.Y, left.Z - right.Z);
}

inline Vector3D operator*(float scalar, const Vector3D& right){
	return Vector3D(scalar * right.X, scalar * right.Y, scalar * right.Z);
}

/*
inline Vector3D lerp(const float& beta, const Vector2D& point1, const Vector2D& point2){
	//Vector2D result = ((1-beta) * point1) + (beta * point2);

	Vector3D result;
	result.X = (1-beta) * point1.X + (beta * point2.X);
	result.Y = (1-beta) * point1.Y + (beta * point2.Y);
	return result;
}
*/

#endif
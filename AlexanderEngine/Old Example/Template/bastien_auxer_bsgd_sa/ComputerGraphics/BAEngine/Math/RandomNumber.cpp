#include <Math\RandomNumber.h>
#include <stdlib.h>

#ifdef _DEBUG
	#include <Debug Tools\Memmory Maniger\DebugMemory.h>
#endif

const float TWO_PIE = (float)(2 * 3.14159265);

inline int RandomNumber::randomInt(){
	return rand();
}

inline double RandomNumber::randomDouble(){
	return (double)rand() / RAND_MAX;
}

inline float RandomNumber::randomFloat(){
	return (float)rand() / RAND_MAX;
}
	
inline int RandomNumber::randomIntInRange(int min, int max){
	//int random = randomInt();
	//srand(5);
	return (rand()%(max - min) + min);
}

inline double RandomNumber::randomDoubleInRange(double min, double max){
	return (randomDouble()*(max - min) + min);
}

inline float RandomNumber::randomFloatInRange(float min, float max){
	return (randomFloat()*(max - min) + min);
}

inline Vector2D RandomNumber::randomVector2D(){
	float angle = randomFloat()*TWO_PIE;
	Vector2D V(cos(angle), sin(angle));
	return V;
}
#pragma once

#ifndef MATRIX2
#define MATRIX2

#include <iostream>
#include <math.h>
#include <Math\Vector2D.h>

class Matrix2D{
public: 
	Vector2D basis1;
	Vector2D basis2;

	Matrix2D(const Vector2D& one = Vector2D(1,0), const Vector2D& two = Vector2D(0,1)): basis1(one), basis2(two)
	{
		basis1 = one;
		basis2 = two;
	}

	~Matrix2D(){
	
	}

	inline Matrix2D Rotation(float angle){
		//Counter Clock Wise
		Vector2D part1(cos(angle), -sin(angle));
		Vector2D part2(sin(angle), cos(angle));
	
		Matrix2D result(part1, part2);

		return result;
	}

	inline Matrix2D Scale(float Scale){
		Vector2D part1(Scale, 0);
		Vector2D part2(0, Scale);
		
		Matrix2D result(part1, part2);
		
		return result;
	}

	inline Matrix2D ScaleX(float Scale){
		Vector2D part1(Scale, 0);
		Vector2D part2(0, 1);
		
		Matrix2D result(part1, part2);
		
		return result;
	}

	inline Matrix2D ScaleY(float Scale){
		Vector2D part1(1, 0);
		Vector2D part2(0, Scale);
		
		Matrix2D result(part1, part2);
		
		return result;
	}

};

inline Vector2D operator*(const Matrix2D& op, const Vector2D& victim){
	Vector2D result;
	result.X = (op.basis1.X * victim.X) + (op.basis2.Y * victim.Y);
	result.Y = (op.basis2.X * victim.X) + (op.basis2.Y * victim.Y);

	return result;
}

inline Matrix2D operator*(const Matrix2D& one, const Matrix2D& two){
	Matrix2D result(Vector2D(0,0), Vector2D(0,0));
	
	result.basis1.X = ( (one.basis1.X * two.basis1.X) + (one.basis2.X * two.basis1.Y) );
	result.basis2.X = ( (one.basis1.Y * two.basis1.X) + (one.basis2.Y * two.basis1.Y) );

	result.basis1.Y = ( (one.basis1.X * two.basis2.X) + (one.basis2.X * two.basis2.Y) );
	result.basis2.Y = ( (one.basis1.Y * two.basis2.X) + (one.basis2.Y * two.basis2.Y) );

	return result;
}




#endif
#pragma once

#ifndef RANDOM
#define RANDOM


#include <stdlib.h>
#include <Math\Vector2D.h>

#include <ExportHeader.h>
#include <core.h>

using Core::RGB;

class RandomGenerator{
public:	

	ENGINE_SHARED RandomGenerator(){}

	ENGINE_SHARED ~RandomGenerator(){}

	ENGINE_SHARED int randomInt();
	ENGINE_SHARED double randomDouble();
	ENGINE_SHARED float randomFloat();

	ENGINE_SHARED int randomIntInRange(int min, int max);
	ENGINE_SHARED double randomDoubleInRange(double min, double max);
	ENGINE_SHARED float randomFloatInRange(float min, float max);

	//////////////////////////////////////// CHANGE TO GLM vec2
	ENGINE_SHARED Vector2D randomVector2D();
	////////////////////////////////////////
	ENGINE_SHARED RGB randomColor();
	ENGINE_SHARED RGB randomColorInRange(int minRed, int maxRed, int minGreen, int maxGreen, int minBlue, int maxBlue);

};



#endif
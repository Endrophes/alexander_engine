#pragma once

#ifndef MATRIX3
#define MATRIX3

#include <iostream>
#include <math.h>
#include <Math\Vector2D.h>
#include <Math\Vector3D.h>

class Matrix3D{
public: 
	Vector3D basis1;
	Vector3D basis2;
	Vector3D basis3;

	Matrix3D(const Vector3D& one = Vector3D(1,0,0), const Vector3D& two = Vector3D(0,1,0), const Vector3D& three = Vector3D(0,0,1)):basis1(one), basis2(two), basis3(three){
		
	}

	Matrix3D(const Vector2D& one, const Vector2D& two )
	{
		basis1 = Vector3D(one.X, one.Y, 0);
		basis2 = Vector3D(two.X, two.Y, 0);
		basis3 = Vector3D(0,0,1);
	}

	~Matrix3D(){
	
	}

	inline Matrix3D Rotation(float angle){
		//Counter Clock Wise
		Vector3D part1(cos(angle), sin(angle), 0);
		Vector3D part2(-sin(angle), cos(angle), 0);
		Vector3D part3(0,0,1);
		
		Matrix3D result(part1, part2, part3);

		return result;
	}

	inline Matrix3D Scale(float Scale){
		Vector3D part1(Scale, 0, 0);
		Vector3D part2(0, Scale, 0);
		Vector3D part3(0,   0,   1);
		
		Matrix3D result(part1, part2, part3);

		return result;
	}

	inline Matrix3D ScaleX(float Scale){
		Vector3D part1(Scale, 0, 0);
		Vector3D part2(  0,   1, 0);
		Vector3D part3(  0,   0, 1);
		
		Matrix3D result(part1, part2, part3);

		return result;
	}

	inline Matrix3D ScaleY(float Scale){
		Vector3D part1(1,   0,   0);

		Vector3D part2(0, Scale, 0);

		Vector3D part3(0,   0,   1);
		
		Matrix3D result(part1, part2, part3);

		return result;
	}

	inline Matrix3D Translation(float x, float y){
		Vector3D part1(1, 0, x);
		Vector3D part2(0, 1, y);
		Vector3D part3(0, 0, 1);
		
		Matrix3D result(part1, part2, part3);
		
		return result;
	}

	inline Matrix3D Translation(Vector2D& t){
		
		Vector3D part1(1, 0, t.X);
		Vector3D part2(0, 1, t.Y);
		Vector3D part3(0, 0,  1 );

		Matrix3D result(part1, part2, part3);
		
		return result;
	}

	operator const float*() const{
		return &basis1.X;
	}

};

inline Vector2D operator*(const Matrix3D& op, const Vector2D& victim){
	Vector2D result;

	result.X = (op.basis1.X * victim.X) + (op.basis1.Y * victim.Y) + (op.basis1.Z * 1);
	result.Y = (op.basis2.X * victim.X) + (op.basis2.Y * victim.Y) + (op.basis2.Z * 1);

	return result;
}

inline Vector3D operator*(const Matrix3D& op, const Vector3D& victim){
	Vector3D result;
	result.X = (op.basis1.X * victim.X) + (op.basis2.X * victim.Y) + (op.basis3.X * victim.Z);
	result.Y = (op.basis1.Y * victim.X) + (op.basis2.Y * victim.Y) + (op.basis3.Y * victim.Z);
	result.Z = (op.basis1.Z * victim.X) + (op.basis2.Z * victim.Y) + (op.basis3.Z * victim.Z);

	return result;
}


inline Matrix3D operator*(const Matrix3D& one, const Matrix3D& two){
	Vector3D part1(0,0,0);
	Vector3D part2(0,0,0);
	Vector3D part3(0,0,0);

	Matrix3D result(part1, part2, part3);
	
	result.basis1.X = ( (one.basis1.X * two.basis1.X) + (one.basis1.Y * two.basis2.X) + (one.basis1.Z * two.basis3.X) );
	result.basis1.Y = ( (one.basis1.X * two.basis1.Y) + (one.basis1.Y * two.basis2.Y) + (one.basis1.Z * two.basis3.Y) );
	result.basis1.Z = ( (one.basis1.X * two.basis1.Z) + (one.basis1.Y * two.basis2.Z) + (one.basis1.Z * two.basis3.Z) );

	result.basis2.X = ( (one.basis2.X * two.basis1.X) + (one.basis2.Y * two.basis2.X) + (one.basis2.Z * two.basis3.X) );
	result.basis2.Y = ( (one.basis2.X * two.basis1.Y) + (one.basis2.Y * two.basis2.Y) + (one.basis2.Z * two.basis3.Y) );
	result.basis2.Z = ( (one.basis2.X * two.basis1.Z) + (one.basis2.Y * two.basis2.Z) + (one.basis2.Z * two.basis3.Z) );

	result.basis3.X = ( (one.basis3.X * two.basis1.X) + (one.basis3.Y * two.basis2.X) + (one.basis3.Z * two.basis3.X) );
	result.basis3.Y = ( (one.basis3.X * two.basis1.Y) + (one.basis3.Y * two.basis2.Y) + (one.basis3.Z * two.basis3.Y) );
	result.basis3.Z = ( (one.basis3.X * two.basis1.Z) + (one.basis3.Y * two.basis2.Z) + (one.basis3.Z * two.basis3.Z) );
	
	return result;
}

inline Matrix3D operator+(const Matrix3D& one, const Matrix3D& two){
	Matrix3D result;

	result.basis1 = one.basis1 + two.basis1;
	result.basis2 = one.basis2 + two.basis2;
	result.basis3 = one.basis3 + two.basis3;
	
	return result;
}




#endif
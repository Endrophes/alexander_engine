#pragma once
#ifndef VECTOR
#define VECTOR

#include <iostream>
#include <math.h>


struct Vector2D{
public:
	float X;
	float Y;

	explicit Vector2D(float x = 0, float y = 0) : X(x), Y(y)
	{
		this -> X = x;
		this -> Y = y;
	}

	~Vector2D(){
	
	}

	inline operator const float*(){
		return &X;
	}

	inline Vector2D PerpCW(){
		return Vector2D(Y, -X);
	}

	inline Vector2D PerpCCW(){
		return Vector2D(-Y, X);
	}

	inline Vector2D Normalized(){
		Vector2D result;
		float length = this->Length();
		result.X = (this->X/length);
		result.Y = (this->Y/length);
		return result;
	}

	inline float Length(){
		return sqrt((this->X * this->X) + (this->Y * this->Y));
	}

	inline float LengthSquared(){
		float length = Length(); 
		return (length * length);
	}

	inline float Dot(const Vector2D& b){
		// a * b
		return (this->X*b.X)+(this->Y*b.Y);
	}

};



inline float Dot(const Vector2D& a, const Vector2D& b){
	// a * b
	return (a.X*b.X)+(a.Y*b.Y);
}

inline std::ostream& operator<<(std::ostream& stream, const Vector2D& right){
	std::cout << "{" << right.X << ", " << right.Y << "}" << std::endl;
	return stream;
}

inline Vector2D operator+(const Vector2D& left, const Vector2D& right){
	return Vector2D(left.X + right.X, left.Y + right.Y);
}

inline Vector2D operator-(const Vector2D& left, const Vector2D& right){
	return Vector2D(left.X - right.X, left.Y - right.Y);
}

inline Vector2D operator*(float scalar, const Vector2D& right){
	return Vector2D(scalar * right.X, scalar * right.Y);
}

inline Vector2D lerp(const float& beta, const Vector2D& point1, const Vector2D& point2){
	//Vector2D result = ((1-beta) * point1) + (beta * point2);

	Vector2D result;
	result.X = (1-beta) * point1.X + (beta * point2.X);
	result.Y = (1-beta) * point1.Y + (beta * point2.Y);
	return result;
}

#endif
#include <Math\RandomGenerator.h>
#include <stdlib.h>

#ifdef _DEBUG
	#include <Debug Tools\Memmory Maniger\DebugMemory.h>
#endif

const float TWO_PIE = (float)(2 * 3.14159265);

inline int RandomGenerator::randomInt(){
	return rand();
}

inline double RandomGenerator::randomDouble(){
	return ((double)rand()) / RAND_MAX;;
}

inline float RandomGenerator::randomFloat(){
	return (float)rand() / RAND_MAX;
}
	
inline int RandomGenerator::randomIntInRange(int min, int max){
	return (rand()%(max - min) + min);
}

inline double RandomGenerator::randomDoubleInRange(double min, double max){
	return (randomDouble()*(max - min) + min);
}

inline float RandomGenerator::randomFloatInRange(float min, float max){
	return (randomFloat()*(max - min) + min);
}

inline Vector2D RandomGenerator::randomVector2D(){
	float angle = randomFloat()*TWO_PIE;
	Vector2D V(cos(angle), sin(angle));
	return V;
}

inline RGB RandomGenerator::randomColor(){
	RGB colorGenerated = RGB(0,0,0);

	int Red   = randomIntInRange(0,255);
	int Green = randomIntInRange(0,255);
	int Blue  = randomIntInRange(0,255);

	colorGenerated = RGB(Red, Green, Blue);

	return colorGenerated;
}

inline RGB RandomGenerator::randomColorInRange(int minRed, int maxRed, int minGreen, int maxGreen, int minBlue, int maxBlue){
	RGB colorGenerated = RGB(0,0,0);

	int Red   = randomIntInRange(minRed,maxRed);
	int Green = randomIntInRange(minGreen,maxGreen);
	int Blue  = randomIntInRange(minBlue,maxBlue);

	colorGenerated = RGB(Red, Green, Blue);

	return colorGenerated;

}
#include <sstream>
#include <string>
#include <String Convertion\String Converter.h>

#ifdef _DEBUG
	#include <Debug Tools\Memmory Maniger\DebugMemory.h>
#endif

std::string NumberStringConverter::intToString(int num){
	std::stringstream convertion;
	convertion << num;
	return convertion.str();
}
std::string NumberStringConverter::floatToString(float num){
	std::stringstream convertion;
	convertion << num;
	return convertion.str();
}
std::string NumberStringConverter::doubleToString(double num){
	std::stringstream convertion;
	convertion << num;
	return convertion.str();
}
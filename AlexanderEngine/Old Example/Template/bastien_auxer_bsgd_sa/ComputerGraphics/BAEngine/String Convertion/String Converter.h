
#pragma once

#ifndef STRING_CONVERTER_H
#define STRING_CONVERTER_H

#include <string>
#include <stdlib.h>

#include <ExportHeader.h>
#include <Core.h>

using std::string;

class ENGINE_SHARED NumberStringConverter{
public:

	NumberStringConverter(){}

	~NumberStringConverter(){}

	string intToString(int num);
	string floatToString(float num);
	string doubleToString(double num);

};



#endif
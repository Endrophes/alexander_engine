#include <Astar\AstarHandler.h>

#ifdef _DEBUG
	#include <Debug Tools\Memmory Maniger\DebugMemory.h>
#endif

void AstarHandler::getAstarNodes()
{

}

vec3 AstarHandler::Lerp(vec3 start, vec3 end)
{
	//(1 - Alpha) start + (Alpha * end)
	vec3 result = ((1 - currentLerpPoint) * start) + (currentLerpPoint * end);
	currentLerpPoint += increasseLerpBy;
	if(currentLerpPoint >= 1.0f)
	{
		currentLerpPoint = 0.1f;
		masterPathSteper++;
	}
	return  result;
}

int AstarHandler::getStorageSize()
{
	return aStarNodeStorage.size();
}

int AstarHandler::getNodesNumConnections(int nodeID)
{
	int temp = aStarNodeStorage.at(nodeID).gameNode->numberofConnections;
	return temp;
}

vec3 AstarHandler::getNodetragetPosition(int nodeID, int concetionID)
{
	char* thePoint = reinterpret_cast<char*>(aStarNodeStorage.at(nodeID).gameNode->connections);
	thePoint += (concetionID * sizeof(NodeHandler::gameConnection));
	NodeHandler::gameConnection* connectionWeWant = reinterpret_cast<NodeHandler::gameConnection*>(thePoint);
	vec3 targetPosition = connectionWeWant->target->position;
	return targetPosition;
}

vec3 AstarHandler::getNodePostionFromStorage(int nodeID)
{
	//openList.erase(openList.begin() + index);
	vec3 position = aStarNodeStorage.at(nodeID).gameNode->position;
	return position;
}

void AstarHandler::caluclateHeritic()
{
	//go through all the nodes and set the heritic value
	/// formula: currentNodesPosition to the TargetsNodes Position
	for(uint stepper = 0; stepper < sizeOfStorage; stepper++)
	{
		uint h = glm::length(aStarNodeStorage.at(stepper).gameNode->position - endingNode->gameNode->position);
		aStarNodeStorage.at(stepper).heritic = h;
	}
}

void AstarHandler::calculatePath()
{
	caluclateHeritic();

	proccessNode(startingNode);

	AstarNode* currentAstarNode = startingNode;

	//Go Algorthm
	bool pathFound = false;
	while(!pathFound)
	{
		//Process all the conections
		uint numConnections = currentAstarNode->gameNode->numberofConnections;
		char* currentNodesConections = reinterpret_cast<char*>(currentAstarNode->gameNode->connections);
		for(uint currentConection = 0; currentConection < numConnections; currentConection++)
		{
			NodeHandler::gameConnection* currentNodeConnection = reinterpret_cast<NodeHandler::gameConnection*>(currentNodesConections + (currentConection * sizeof(NodeHandler::gameConnection)));
			NodeHandler::gameNode* currentTargetNode = currentNodeConnection->target;
			uint nodeId = currentTargetNode - gameNodeController->gameNodeStorage;
			proccessNode(&aStarNodeStorage.at(nodeId));
		}
		
		AstarNode* newNode = evaluateTheOpenList();

		int index = findIndexOfElementInOpenList(currentAstarNode);

		openList.erase(openList.begin() + index);
		/*closedList.push_back(currentAstarNode);
		currentClosedAstar++;*/

		newNode->parentNode = currentAstarNode;
		
		path.push_back(currentAstarNode);
		currentPathSlot++;

		currentAstarNode = newNode;

		if(currentAstarNode == endingNode)
		{
			pathFound = true;
			path.push_back(currentAstarNode);
			currentPathSlot++;
		}

	}
	getPathPoitions();

}

void AstarHandler::calculatePath(AstarNode* start, AstarNode* target)
{
	startingNode = start;
	endingNode = target;

	caluclateHeritic();

	proccessNode(startingNode);

	AstarNode* currentAstarNode = startingNode;

	//Go Algorthm
	bool pathFound = false;
	while(!pathFound)
	{
		//Process all the conections
		uint numConnections = currentAstarNode->gameNode->numberofConnections;
		for(uint currentConection = 0; currentConection < numConnections; currentConection++)
		{
			NodeHandler::gameConnection* currentNodeConnection = reinterpret_cast<NodeHandler::gameConnection*>(currentAstarNode->gameNode->connections + currentConection * sizeof(NodeHandler::gameConnection));
			NodeHandler::gameNode* currentTargetNode = currentNodeConnection->target;
			uint nodeId = currentTargetNode - gameNodeController->gameNodeStorage;
			proccessNode(&aStarNodeStorage.at(nodeId));
		}
		
		AstarNode* newNode = evaluateTheOpenList();

		int index = findIndexOfElementInOpenList(currentAstarNode);

		openList.erase(openList.begin() + index);
		closedList.assign(currentClosedAstar++, currentAstarNode);

		newNode->parentNode = currentAstarNode;
		
		path.assign(currentPathSlot++, currentAstarNode);

		currentAstarNode = newNode;

		if(currentAstarNode == endingNode)
		{
			pathFound = true;
			path.assign(currentPathSlot++, currentAstarNode);
		}

	}

}

int AstarHandler::findIndexOfElementInOpenList(AstarNode* elementToFind)
{
	int index = -1;
	for(uint stepper = 0; stepper < openList.size() && index == -1; stepper++)
	{
		float h = openList.at(stepper)->heritic;
		float TEC = openList.at(stepper)->totalEstimatedCost;

		if(
			elementToFind->heritic == h &&
			elementToFind->totalEstimatedCost == TEC
		)
		{
			index = stepper;
		}
	}
	return index;
}

int AstarHandler::findIndexOfElementaAStarStorage(AstarNode* elementToFind)
{
	int index = -1;
	for(uint stepper = 0; stepper < aStarNodeStorage.size() && index == -1; stepper++)
	{
		float h = aStarNodeStorage.at(stepper).heritic;
		float TEC = aStarNodeStorage.at(stepper).totalEstimatedCost;

		if(
			elementToFind->heritic == h &&
			elementToFind->totalEstimatedCost == TEC
		)
		{
			index = stepper;
		}
	}
	return index;
}

AstarHandler::AstarNode* AstarHandler::evaluateTheOpenList()
{
	AstarNode* lowestTEC = openList.at(0);
	for(uint stepper = 0; stepper < openList.size(); stepper++)
	{
		if(openList.at(stepper)->totalEstimatedCost <= lowestTEC->totalEstimatedCost)
		{
			lowestTEC = openList.at(stepper);
		}
	}
	return lowestTEC;
}

void AstarHandler::proccessNode(AstarNode* nodeToProccess)
{
	nodeToProccess->totalEstimatedCost = nodeToProccess->CostSoFair + nodeToProccess->heritic;
	openList.push_back(nodeToProccess);
	currentOpenAstar++;
}

void AstarHandler::inisalizeAstar(NodeHandler* nodeController, float stepperValue, Render::Renderable* model)
{
	gameNodeController = nodeController;
	aStarNodeStorage.resize(nodeController->numberOfTotalGameNodes);
	sizeOfStorage = nodeController->numberOfTotalGameNodes;
	//currentAstarNode = aStarNodeStorage.begin();
	//currentOpenNode = openList.begin();
	//currentClosedNode = closedList.begin();

	ModelToManipulate = model;

	currentLerpPoint = 0.1f;
	increasseLerpBy = stepperValue;

	currentAstarNode = 0;
	currentOpenAstar = 0;

	defualt.CostSoFair = 0.0f;
	defualt.heritic = 0.0f;
	defualt.open = false;
	defualt.totalEstimatedCost = 0.0f;

	char* bankPointer = reinterpret_cast<char*>(nodeController->gameNodeStorage);

	for(uint stepper = 0; stepper < sizeOfStorage; stepper++)
	{
		defualt.gameNode = reinterpret_cast<NodeHandler::gameNode*>(bankPointer + ( stepper * sizeof(NodeHandler::gameNode)));
		aStarNodeStorage.at(stepper) = defualt;
	}
	
	//delete bankPointer;
}

void AstarHandler::inisalizeAstar(NodeHandler* nodeController, float stepperValue)
{
	gameNodeController = nodeController;
	aStarNodeStorage.resize(nodeController->numberOfTotalGameNodes);
	sizeOfStorage = nodeController->numberOfTotalGameNodes;

	currentLerpPoint = 0.1f;
	increasseLerpBy = stepperValue;

	currentAstarNode = 0;
	currentOpenAstar = 0;

	defualt.CostSoFair = 0.0f;
	defualt.heritic = 0.0f;
	defualt.open = false;
	defualt.totalEstimatedCost = 0.0f;

	char* bankPointer = reinterpret_cast<char*>(nodeController->gameNodeStorage);

	for(uint stepper = 0; stepper < sizeOfStorage; stepper++)
	{
		defualt.gameNode = reinterpret_cast<NodeHandler::gameNode*>(bankPointer + ( stepper * sizeof(NodeHandler::gameNode)));
		aStarNodeStorage.at(stepper) = defualt;
	}
}

void AstarHandler::update()
{
	//call lerp to get new position for player object
	//Set player object to new position
	//if reached node then set new lerp points

	//vec3 newPosition = vec3();
	//if(masterPathSteper < path.size() - 1)
	//{
	//	vec3 to = path.at(masterPathSteper)->gameNode->position;
	//	vec3 from = path.at(masterPathSteper + 1)->gameNode->position;

	//	ModelToManipulate->direction = from - to;
	//	newPosition = Lerp(to, from);
	//	translationMatrix = glm::translate(newPosition);

	//	vec3 theLine = from - to;
	//	vec3 xBasis = glm::normalize(theLine);
	//	mat4 theRotation(CreateBasisFromOneVector(&xBasis));

	//	///Plug in data
	//	ModelToManipulate->Position = newPosition;
	//	ModelToManipulate->rotationMatrix = theRotation;
	//	ModelToManipulate->translationMatrix = translationMatrix;
	//}
	//else
	//{
	//	startingNode = path.at(path.size()-1);
	//	path.clear();
	//	uint index = findIndexOfElementaAStarStorage(startingNode);
	//	newRandomPath(index);
	//	masterPathSteper = 0;
	//}

	//translationMatrix = glm::translate(newPosition);

}

std::vector<AstarHandler::AstarNode*>* AstarHandler::getPointerToPath()
{
	std::vector<AstarHandler::AstarNode*>* pathPointer = &path;
	return pathPointer;
}

mat4 AstarHandler::CreateBasisFromOneVector(vec3* basis)
{
	vec3 z = glm::normalize(glm::cross(*basis,vec3(0.01,-1.8f,0)));
	vec3 y = glm::normalize(glm::cross(*basis,z));

	mat4 result(vec4(*basis,0), vec4(y,0),vec4(z,0), vec4(0,0,0,1));
	return result;
}

void AstarHandler::newRandomPath(int startingNodesIndex)
{
	int firstNum = startingNodesIndex;
	int secoundNum = 0;
	bool findRandom = true;
	while(findRandom)
	{
		//firstNum = randomGen.randomIntInRange(0,aStarNodeStorage.size());
		secoundNum =  randomGen.randomIntInRange(0,aStarNodeStorage.size());
		if(firstNum != secoundNum)
		{
			findRandom = false;
		}
	}

	startingNode = &aStarNodeStorage.at(firstNum);
	endingNode = &aStarNodeStorage.at(secoundNum);

	calculatePath();
}

void AstarHandler::pathFromStartToEnd(vec3 start, vec3 end)
{
	startingNode = findClosestNode(start);
	endingNode = findClosestNode(end);
	path.clear();
	calculatePath();
	getPathPoitions();
}

AstarHandler::AstarNode* AstarHandler::findClosestNode(vec3 targetLocation)
{
	bool exit = false;
	AstarNode* closestNode = &aStarNodeStorage.at(0);
	for(uint stepper = 0; stepper < aStarNodeStorage.size() && !exit; stepper++)
	{
		if(aStarNodeStorage.at(stepper).gameNode->position == targetLocation)
		{
			closestNode = &aStarNodeStorage.at(stepper);
			exit = true;
		}
	}
	return closestNode;
}

void AstarHandler::pathToFlag(int indexInArray, vec3 newEndPosition)
{
	startingNode = &aStarNodeStorage.at(indexInArray);
	endingNode = findClosestNode(newEndPosition);

	path.clear();

	calculatePath();
	getPathPoitions();
}

void AstarHandler::recalculating(uint* stepperInPath, vec3 newEndPosition)
{
	AstarNode* firstPoint = path.at(*stepperInPath);
	AstarNode* secondPoint = path.at(*stepperInPath + 1);
	AstarNode* endPoint = findClosestNode(newEndPosition);
	stepperInPath = 0;

	path.clear();
	
	path.push_back(firstPoint);
	path.push_back(secondPoint);
	startingNode = secondPoint;
	endingNode = endPoint;

	calculatePath();
	getPathPoitions();

}

void AstarHandler::randomPath()
{
	int firstNum = 0;
	int secoundNum = 0;
	bool findRandom = true;
	while(findRandom)
	{
		firstNum = randomGen.randomIntInRange(0,aStarNodeStorage.size());
		secoundNum =  randomGen.randomIntInRange(0,aStarNodeStorage.size());
		if(firstNum != secoundNum)
		{
			findRandom = false;
		}
	}

	startingNode = &aStarNodeStorage.at(firstNum);
	endingNode = &aStarNodeStorage.at(secoundNum);

	calculatePath();
}

std::vector<vec3> AstarHandler::getAllNodesPosition()
{
	std::vector<vec3> nodePositions;
	for(uint stepper = 0; stepper < aStarNodeStorage.size(); stepper++)
	{
		nodePositions.push_back(aStarNodeStorage.at(stepper).gameNode->position);
	}
	return nodePositions;
}

std::vector<vec3> AstarHandler::getPathNodesPosition()
{
	std::vector<vec3> pathNodePositions;
	for(uint stepper = 0; stepper < path.size(); stepper++)
	{
		pathNodePositions.push_back(path.at(stepper)->gameNode->position);
	}
	return pathNodePositions;
}

void AstarHandler::getPathPoitions()
{
	pathPositions.clear();
	for(uint stepper = 0; stepper < path.size(); stepper++)
	{
		pathPositions.push_back(&path.at(stepper)->gameNode->position);
	}
}

std::vector<vec3*>* AstarHandler::getPointerToPositions()
{
	return &pathPositions;
}

AstarHandler::AstarHandler()
{
	masterPathSteper = 0;
	randomGen = RandomGenerator();
}

AstarHandler::~AstarHandler()
{

}
#pragma once

#include <Qt\qlist.h>
#include <stdlib.h>

#include <ExportHeader.h>
#include <Render\Render.h>
#include <Node\NodeHandler.h>
#include <Random Generator\RandomGenerator.h>

class ENGINE_SHARED AstarHandler
{
public:

	class AstarNode
	{
	public:
		float CostSoFair;
		float heritic;
		float totalEstimatedCost;
		bool open;
		AstarNode* parentNode;
		NodeHandler::gameNode* gameNode;

		AstarNode()
		{
			CostSoFair = 0.0f;
			heritic = 0.0f;
			open = false;
			totalEstimatedCost = 0.0f;
		}
	};
	AstarNode defualt;

	AstarNode* startingNode;
	AstarNode* endingNode;

	NodeHandler* gameNodeController;
	Render::Renderable* ModelToManipulate;
	mat4 translationMatrix;
	mat4 rotationMatrix;

	uint sizeOfStorage;
	uint currentAstarNode;
	uint currentOpenAstar;
	uint currentClosedAstar;
	uint currentPathSlot;
	float currentLerpPoint;

	RandomGenerator randomGen;

	int masterPathSteper;
	
	std::vector<AstarNode*> path;
	//std::vector<AstarNode*>::iterator currentPathSlot;

	std::vector<vec3*> pathPositions;

	std::vector<AstarNode*> openList;
	//std::vector<AstarNode*>::iterator currentOpenNode;

	std::vector<AstarNode*> closedList;
	//std::vector<AstarNode*>::iterator currentClosedNode;

	std::vector<AstarNode> aStarNodeStorage;
	//std::vector<AstarNode>::iterator currentAstarNode;

	char* gameNodeStorage;

	vec3 startPosition;
	vec3 endPosition;

	//calculate Total Estimated cost TEC when you need it
	//Lerp along the path

	float increasseLerpBy;

	int getStorageSize();

	vec3 getNodePostionFromStorage(int nodeID);

	int getNodesNumConnections(int nodeID);

	vec3 getNodetragetPosition(int nodeID, int concetionID);

	int findIndexOfElementInOpenList(AstarNode* elementToFind);

	int findIndexOfElementInClosedList(AstarNode* elementToFind);

	int findIndexOfElementaAStarStorage(AstarNode* elementToFind);

	void getAstarNodes();

	vec3 Lerp(vec3 start, vec3 end);

	void caluclateHeritic();

	void caluclateHeritic(NodeHandler::gameNode* start, NodeHandler::gameNode* target);

	void calculatePath();

	void calculatePath(AstarNode* start, AstarNode* target);

	void inisalizeAstar(NodeHandler* nodeController, float stepperValue);

	void inisalizeAstar(NodeHandler* nodeController, float stepperValue, Render::Renderable* model);

	void randomPath();

	void newRandomPath(int startingNodesIndex);

	void pathFromStartToEnd(vec3 start, vec3 end);

	AstarNode* findClosestNode(vec3 targetLocation);

	void pathToFlag(int indexInArray, vec3 newEndPosition);

	void recalculating(uint* stepperInPath, vec3 newEndPosition);

	void getPathPoitions();

	std::vector<vec3*>* getPointerToPositions();

	std::vector<vec3> getAllNodesPosition();

	std::vector<vec3> getPathNodesPosition();

	AstarNode* evaluateTheOpenList();

	void proccessNode(AstarNode* nodeToProccess);

	mat4 CreateBasisFromOneVector(vec3* basis);

	std::vector<AstarNode*>* getPointerToPath();

	void update();

	AstarHandler();

	~AstarHandler();

};
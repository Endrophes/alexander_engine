#pragma once
#ifndef ENGINE_RENDERER_COMPONENT_H
#define ENGINE_RENDERER_COMPONENT_H

#include <Component\Component.h>
#include <Render\Render.h>

//namespace Render {class Renderable;}

namespace Entites
{
	class ENGINE_SHARED RendereComponent : public Component
	{
	public:
		Render::Renderable* renderable;
		void setData(Render::Renderable* renderable);
		void update();

	};
}

#endif
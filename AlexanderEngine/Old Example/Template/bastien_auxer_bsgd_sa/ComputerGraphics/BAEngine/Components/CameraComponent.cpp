#include "CameraComponent.h"
#include <gtx\transform.hpp>
#include <Enity\Entity.h> 

namespace Entites
{

	CameraComponent::CameraComponent(void)
	{
		UP = glm::vec3(0.0f,1.0f,0.0f);
		positionOffSet = glm::vec3(0.0f,0.0f,0.0f);
		viewDirection = glm::vec3(0.0f,0.0f,-1.0f);
	}

	CameraComponent::~CameraComponent(void)
	{

	}

	void CameraComponent::setValues(glm::vec3 positionOffSet, glm::vec3 viewDirection, glm::vec3 up)
	{
		this->UP = up;
		this->positionOffSet = positionOffSet;
		this->viewDirection = viewDirection;
	}

	void CameraComponent::setPositionOffSet(glm::vec3 positionOffSet)
	{
		this->positionOffSet = positionOffSet;
	}

	void CameraComponent::update()
	{
		this->position = getParent()->position + positionOffSet;
	}

}
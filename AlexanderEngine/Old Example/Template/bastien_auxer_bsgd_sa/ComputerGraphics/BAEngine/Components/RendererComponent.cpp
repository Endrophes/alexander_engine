#include <Components\RendererComponent.h>
#include <Render\Render.h>
#include <glm.hpp>
#include <Enity\Entity.h>

#ifdef _DEBUG
	#include <Debug Tools\Memmory Maniger\DebugMemory.h>
#endif

namespace Entites
{

	void RendereComponent::setData(Render::Renderable* renderable)
	{
		this->renderable = renderable;
		//Entites::Entity* pot = getParent();
		//pot->position = renderable->Position;
	}

	void RendereComponent::update() 
	{
		renderable->translationMatrix = glm::translate(getParent()->position);
		renderable->rotationMatrix = getParent()->getRotation();
		//getParent()->orientationX;
		//getParent()->orientationY;
		//getParent()->orientationZ;

		//renderable->rotationMatrix
	}

}


#include <Components\PhysicsComponent.h>
#include <Enity\Entity.h>

#ifdef _DEBUG
	#include <Debug Tools\Memmory Maniger\DebugMemory.h>
#endif

namespace Entites
{

	PhysicsComponent::PhysicsComponent()
	{
		angularVelocity = 0;
		velocity = glm::vec3();
	}

	void PhysicsComponent::update()
	{
		getParent()->position += velocity;
		//getParent()->orientation += angularVelocity;
	}
}
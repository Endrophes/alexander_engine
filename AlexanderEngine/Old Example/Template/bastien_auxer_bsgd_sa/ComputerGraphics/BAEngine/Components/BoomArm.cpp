#include "BoomArm.h"
#include <Enity\Entity.h> 
#include <Components\RendererComponent.h>

namespace Entites
{

	BoomArm::BoomArm(void)
	{

	}

	BoomArm::~BoomArm(void)
	{

	}

	/*
	bool BoomArm::addComponentToArm(Component* addon, glm::vec3 positionOffset)
	{
		//Build Object
		TrackComponent newBoomarmObject;
		newBoomarmObject.addon = addon;
		newBoomarmObject.positionOffset = positionOffset;

		//Add to storage
		ComponentOnBoomArm.push_back(newBoomarmObject);
	}
	*/

	bool BoomArm::setup(Entity* targetToAttachTo)
	{
		attachedObject = targetToAttachTo;

		return true;
	}

	bool BoomArm::addEnitieToArm(Entity* object, glm::vec3 positionOffset)
	{
		//Build Object
		TrackEnity newBoomarmObject;
		newBoomarmObject.object = object;
		newBoomarmObject.positionOffset = positionOffset;

		//Add to storage
		EnitiesOnBoomArm.push_back(newBoomarmObject);

		return true;
	}

	void BoomArm::update()
	{
		int storgeLength = EnitiesOnBoomArm.size();
		glm::vec3 masterPosition = attachedObject->position;

		for(int step = 0; step < storgeLength; step++)
		{
			TrackEnity* currentObj = &EnitiesOnBoomArm[step];
			glm::vec3 newPosition = currentObj->positionOffset + masterPosition;
			currentObj->object->position = newPosition;
		}

		/*
		//add componets to boomarm
		//*/
	}

}
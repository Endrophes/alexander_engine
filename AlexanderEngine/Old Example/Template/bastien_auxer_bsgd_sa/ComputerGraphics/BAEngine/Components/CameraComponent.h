#pragma once
#ifndef ENGINE_CAMERA_COMPONENT_H
#define ENGINE_CAMERA_COMPONENT_H

#include <glm.hpp>
#include <ExportHeader.h>
#include <Component\Component.h>
#include <Camera\Camera.h>

#ifdef _DEBUG
	#include <Debug Tools\Memmory Maniger\DebugMemory.h>
#endif

namespace Entites
{
	class ENGINE_SHARED CameraComponent : public Component, public Camera
	{
		glm::vec3 positionOffSet;

	public:

		void setValues(glm::vec3 positionOffSet, glm::vec3 viewDirection, glm::vec3 up);
		void setPositionOffSet(glm::vec3 positionOffSet);

		void update();

		CameraComponent(void);
		~CameraComponent(void);
	};
}
#endif
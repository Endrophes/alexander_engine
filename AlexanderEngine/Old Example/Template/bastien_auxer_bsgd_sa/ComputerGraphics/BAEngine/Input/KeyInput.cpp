#include <Windows.h>
#include <Input\IKeyMapper.h>
#include <Input\KeyInput.h>

#ifdef _DEBUG
	#include <Debug Tools\Memmory Maniger\DebugMemory.h>
#endif

namespace Input
{

	KeyInput KeyInput::instance;

	bool KeyInput::initialize(IKeyMapper* keyMapper, int maxActionValue)
	{
		bool result = true;

		if(keyMapper != 0 && maxActionValue >= 0)
		{
			this->keyMapper = keyMapper;
			this->maxActionValue = maxActionValue;
		}
		else
		{
			result = false;
		}


		return result;
	}

	bool KeyInput::shutdown()
	{
		return true;
	}

	void KeyInput::update()
	{
		actionsThisFrame = 0;

		int possibleAction = 1;

		while(possibleAction != maxActionValue)
		{
			int key = keyMapper->getKeyFor(possibleAction);

			if(GetAsyncKeyState(key))
			{
				actionsThisFrame |= possibleAction;
			}

			possibleAction <<= 1;
		}

	}

	bool KeyInput::activeActionThisFrame(int actions) const
	{
		return (actionsThisFrame & actions) == actions;
	}

	KeyInput& KeyInput::getInstance()
	{
		return instance;
	}

}
#ifndef ENGINE_IKEY_MAPPER_H
#define ENGINE_IKEY_MAPPER_H

namespace Input
{
	class IKeyMapper
	{
	public:
		//virtual int getActionFor(int key) const = 0;
		virtual int getKeyFor(int action) const = 0;
	};
}

#endif
#pragma once

#ifndef ENGINE_KEY_INPUT_H
#define ENGINE_KEY_INPUT_H

#include <ExportHeader.h>

namespace Input
{
	class IKeyMapper;
	class ENGINE_SHARED KeyInput
	{
		IKeyMapper* keyMapper;
		int maxActionValue;
		int actionsThisFrame;
		KeyInput() {}
		KeyInput(const KeyInput&);
		KeyInput& operator=(const KeyInput&);
		static KeyInput instance;

	public:
		bool initialize(IKeyMapper* keyMapper, int maxActionValue);
		bool shutdown();
		void update();
		int	 activeActions() const {return actionsThisFrame;}
		bool activeActionThisFrame(int actions) const;
		static KeyInput& getInstance();
	};

	#define input Input::KeyInput::getInstance()
}

#endif

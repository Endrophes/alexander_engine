#include <Sliders\DebugSlider.h>
#include <QtGui\qvboxlayout>
#include <QtGui\qslider>
#include <QtGui\qlabel>

#ifdef _DEBUG
#include <Debug Tools\Memmory Maniger\DebugMemory.h>
#endif

DebugSlider::DebugSlider(float min, float max, bool textOnLeft, float granularity)
{
	QLayout* layout;
	this->min = min;
	this->max = max;
	sliderGranularity = granularity;
	setLayout(layout = textOnLeft ? (QLayout*)new QHBoxLayout : new QVBoxLayout);
	layout->addWidget(label = new QLabel);
	label->setMinimumWidth(35);
	layout->addWidget(slider = new QSlider);
	label->setAlignment(Qt::AlignCenter);
	slider->setOrientation(Qt::Horizontal);
	slider->setMinimum(0);
	slider->setMaximum(sliderGranularity);
	connect(slider, SIGNAL(valueChanged(int)), 
	this, SLOT(sliderValueChanged()));
	sliderValueChanged();
}

void DebugSlider::adgustSlider(float minValue,float maxValue, int newHight)
{
	min = minValue;
	max = maxValue;
	setFixedHeight(newHight);
	setValue(minValue);
}
float DebugSlider::value() const
{
	float value = min + (max - min) * (slider->value() / sliderGranularity);
	return value;
}
void DebugSlider::setValue(float newValue)
{
	float spot = (newValue - min) / (max - min);
	slider->setValue(spot * sliderGranularity);
	sliderValueChanged();
}
void DebugSlider::sliderValueChanged()
{
	label->setText(QString::number(value()));
	emit valueChanged(value());
}

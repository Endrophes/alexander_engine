#pragma once
#ifndef DEBUG_SLIDER_H
#define DEBUG_SLIDER_H
#include <GL\glew.h>
#include <QtGui\qwidget.h>
#include <QtGui\qwidget>
class QSlider;
class QLabel;
class DebugSlider : public QWidget
{
	Q_OBJECT
	QSlider* slider;
	QLabel* label;
	float sliderGranularity;	
	float min;
	float max;
	private slots:
	void sliderValueChanged();
	signals:
	void valueChanged(float newValue);
	
public:
	void adgustSlider(float minValue = 0.0f,float maxValue = 1.0f, int newHight = 3);
	DebugSlider(
	float min = 0.0f, float max = 1.0f, 
	bool textOnLeft = false, float granularity = 40.0);
	float value() const;
	void setValue(float newValue);
};
#endif

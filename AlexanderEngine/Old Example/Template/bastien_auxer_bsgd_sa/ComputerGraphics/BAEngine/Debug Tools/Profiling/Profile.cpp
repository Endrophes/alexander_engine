#ifdef PROFILING_ON

#include "Profile.h"
#include "Profiler.h"

namespace Profiling{

	Profile::Profile(const char* category) : entryName(category)
	{
		entryName = category;
		clock.startTimer();
	}
	Profile::~Profile(){
		clock.stopTimer();
		profiler.addEntry(entryName, (float)clock.getElapsedTime());
	}

}
#endif


#pragma once

#ifndef DEBUG_PROFILER_H
#define DEBUG_PROFILER_H

#include "..\..\Timer\Timer.h"

#ifdef PROFILING_ON
#include <cassert>
#include <fstream>
#endif // PROFILING_ON

namespace Profiling{

	class _declspec(dllexport) Profiler
	{
	public:
		static Profiler& getInstance();
		Profiler(void){}
		Profiler(const Profiler&);
		Profiler operator = (const Profiler&);
		~Profiler(void){}

		static Profiler theInstance;
		

#if PROFILING_ON
		bool status;
		const char* fileName;
		static const unsigned int MAX_FRAME_SAMPLES = 100;
		static const unsigned int MAX_FILE_CATEGORIES = 30;
		int frameIndex;
		unsigned int categoryIndex;
		unsigned int numUsedCategories;


		struct ProfileCategory{
			const char* name;
			float samples[MAX_FRAME_SAMPLES];
		} categories[MAX_FILE_CATEGORIES];
		char getDelimiter(unsigned int index) const;

	public:
		
		void writeData() const;
		void writeFrame(unsigned int index) const;
		bool wrapped() const;
		bool currentFramComplete() const;
		unsigned int getMaxFrameSamples();
		unsigned int getMaxFileCategories();
#endif

public:
#if PROFILING_ON
		void initialize(const char* fileName);
		void shutDown();
		void newFrame();
		void addEntry(const char* category, float time);
		void checkStatus(bool* status) const;
		void checkForDuplicateCategory(const char* category);
#else
		void initialize(const char* fileName){}
		void shutDown(){}
		void newFrame(){}
		void addEntry(const char* category, float time){}
		void checkStatus(bool* status) const{}
		void checkForDuplicateCategory(const char* category) const{}
#endif
	};

#define profiler Profiling::Profiler::getInstance()

}
#endif
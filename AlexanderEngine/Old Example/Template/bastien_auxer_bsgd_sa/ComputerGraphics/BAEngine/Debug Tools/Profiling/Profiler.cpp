#ifdef PROFILING_ON
#include "Profiler.h"
#include <cassert>
#include <fstream>
#endif

#if PROFILING_ON

using Profiling::Profiler;
using std::ofstream;

Profiler Profiler::theInstance;
Profiler& Profiler::getInstance(){
	return theInstance;
}

static ofstream outStream;

namespace Profiling{

	using Profiling::Profiler;
	using std::ifstream;
	using std::ofstream;

	unsigned int Profiler::getMaxFrameSamples(){
		return MAX_FRAME_SAMPLES;
	}

	unsigned int Profiler::getMaxFileCategories(){
		return MAX_FILE_CATEGORIES;
	}

	void Profiler::initialize(const char* fileName){
		this->fileName = fileName;
		frameIndex = -1;
		categoryIndex = 0;
		numUsedCategories = 0;
		status = true;
	}

	void Profiler::newFrame(){
		//assert(status);
		if(frameIndex > 0){
			assert(categoryIndex == numUsedCategories);
		}
		frameIndex++;
		categoryIndex = 0;
	}
	
	void Profiler::checkForDuplicateCategory(const char* category){
		for(unsigned int i = 0; i < categoryIndex; i++){
			status &= strcmp(categories[i].name, category) !=0;
		}
	}

	void Profiler::addEntry(const char* categoryName, float time){
		assert(categoryIndex < MAX_FILE_CATEGORIES);

		ProfileCategory& pc = categories[categoryIndex];
		if(frameIndex == 0){
			pc.name = categoryName;
			numUsedCategories++;
			checkForDuplicateCategory(categoryName);
		}
		else{
			assert(pc.name == categoryName && categoryName != NULL);
			assert(categoryIndex < numUsedCategories);
		}
		categoryIndex++;
		pc.samples[frameIndex % MAX_FRAME_SAMPLES] = time;
	}

	bool Profiler::wrapped() const{
		return frameIndex >= MAX_FRAME_SAMPLES && frameIndex != -1;
	}

	void Profiler::writeFrame(unsigned int index) const{
		for(unsigned int cat = 0; cat < numUsedCategories; cat++){
			outStream << categories[cat].samples[index];
			outStream << getDelimiter(cat);
		}
	}
	
	bool Profiler::currentFramComplete() const{
		return categoryIndex == numUsedCategories;
	}

	void Profiler::writeData() const{
		outStream.open(fileName, std::ios::trunc);

		//write category headers
		for(unsigned int i =0; i < numUsedCategories; i++){
			outStream << categories[i].name;
			char debug = getDelimiter(i);
			outStream << debug;
		}

		unsigned int endIndex;
		unsigned int startIndex;

		if(wrapped()){
			endIndex = frameIndex % MAX_FRAME_SAMPLES;
			startIndex = (endIndex + 1) % MAX_FRAME_SAMPLES;
			while(startIndex != endIndex){
				writeFrame(startIndex);
				startIndex = (startIndex + 1) % MAX_FRAME_SAMPLES;
			}
			
			if(currentFramComplete()){
				writeFrame(startIndex);
			}

		}
		else{

			unsigned int numActualFrames = frameIndex;
			if(currentFramComplete()){
				numActualFrames++;
			}

			endIndex = numActualFrames;
			startIndex = 0;
			while(startIndex < endIndex){
				writeFrame(startIndex++);
			}
		}

		categoryIndex == numUsedCategories ? frameIndex +1 : frameIndex;

		outStream.close();
	}

	void Profiler::shutDown(){
		writeData();
	}

	char Profiler::getDelimiter(unsigned int index) const{
		return ((index+1) < numUsedCategories) ? ',' : '\n';
	}

	void Profiler::checkStatus(bool* status) const{
		*status = this->status;

	}

}
#endif
#ifndef DEBUG_PROFILE_H
#define DEBUG_PROFILE_H

#include "..\..\Timer\Timer.h"
#include <string>
#include "..\..\ExportHeader.h"

using std::string;

namespace Profiling{

	class ENGINE_SHARED Profile{
#if PROFILING_ON
	public:	
		Timer clock;
		const char* entryName;
#endif
#if PROFILING_ON
		Profile(const char* category);
		~Profile();
#else
		Profile(const char* category){}
		~Profile(){}
#endif
	};

}

#if PROFILING_ON
#define PROFILE(category) Profiling::Profile p(category)
#else
#define PROFILE(category)
#endif

#endif
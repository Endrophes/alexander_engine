#pragma warning ( disable : 4005 )
#pragma once

#include "..\..\Render\Render.h"
//#include <ShapeData.h>
//#include <ShapeGenerator.h>
#include <list>
#include "..\..\Astar\AstarHandler.h"
#include "..\..\ExportHeader.h"

using glm::vec2;
using glm::vec3;
using glm::vec4;
using glm::mat3;
using glm::mat4;
//using Neumont::ShapeData;
//using Neumont::ShapeGenerator;

#define NUMBER_OF_RENDERABLES2 30
#define NUMBER_OF_TEXTURES 10
#define NUMBER_OF_GEOMITRYS 10
#define NUMBER_OF_SHADERS 10
#define NUMBER_OF_BUFFERINFOS 10
#define NUMBER_OF_LINES	 50
#define NUMBER_OF_CROSSHAIRS 10

class ENGINE_SHARED DebugShapes
{
	 Render* workingRender;
	 GLuint ByteOffSet;

	 int currentLineSlot;
	 int currentDebugShapeSlot;
	 int currentCrossHairSlot;

	 class Vetecies
	 {
	 public:
		vec3 position;
		vec3 color;
	 };

	 class Crosshair
	 {
	 public:
		 Vetecies vertexData[6];
		 GLushort indexs[6];
		 Render::GeomitryInfo shapeData;
		 int duration;
		 bool depthEnabled;
	 }crosshairStorage[NUMBER_OF_CROSSHAIRS];

	 class Line
	{
	public:
		Render::GeomitryInfo shapeData;
		Vetecies verts[2];
		GLushort indexs[2];
		float width;
		vec3 fromPosition;
		vec3 toPosition;
		vec4 color;
		float duration;
		bool permanet;
		bool depthEnabled;

		Line()
		{

		}

		Line(float thinkness, vec3 start, vec3 end, vec4 setColor, 
			float durationTime = 0.0f, bool permanetShape = false, bool disableDebth = false)
		{
			width = thinkness;
			fromPosition = start;
			toPosition = end;
			color = setColor;
			duration = durationTime;
			permanet = permanetShape;
			depthEnabled = disableDebth;
		}
	}lineStorage[NUMBER_OF_LINES];

	 class  DebugRenderable
	{
	public:
		Render::GeomitryInfo* gemomitry;
		Render::shaderInfo* shaders;
		vec3 position;
		vec4 color;
		vec3 rotation;
		float lifeTime;
		float scale;
		bool depthEnabled;
		bool isVisable;
		bool permanet;
	}debugRenderableStorage[NUMBER_OF_RENDERABLES2];

	 Crosshair* createCrosshair(vec3 position);
	 
	 Line addLineInfo(vec3 position);

	 GLuint addTexture(char* file);

	 void setActiveTexture(int texture = 0);

	 Render::GeomitryInfo sphere;
	 Render::GeomitryInfo cube;

	 Render::shaderInfo debugShadders;

	 void createDebugRenderable(Render::GeomitryInfo* shape, Render::shaderInfo* shaders, vec3 position, vec4 color, float size, float duration, vec3 rotation, bool permanet, bool enableDepth);

	 void drawDebugRenderables();

public:
	#ifdef DEBUGMENU_ON
		bool showPath;
	#endif


	std::vector<AstarHandler::AstarNode*>* path;

	//To the debug drawing queue:
	// Adds a line segment to the debug drawing queue.
	 void AddLine(
		const float width,
		const vec3& fromPosition,
		const vec3& toPosition,
		vec4 color = vec4(0,1,0,1),
		float duration = 1.0f,
		bool permanetShape = false,
		bool depthEnabled = true);
	// Adds an axis-aligned cross/POINT (3 lines converging at a point) 
	 void AddCross(
		const float width,
		const vec3& position,
		float duration = 1.0f,
		bool depthEnabled = true);
	// Adds a wireframe sphere to the debug drawing queue.
	 void AddSphere( 
		const vec3& centerPosition,
		vec4 color,
		vec3 rotation,
		float size,
		float duration = 1.0f,
		bool permanet = false,
		bool depthEnabled = true);
	//add a Cube
	 void AddCube(
		const vec3& centerPosition,
		vec4 color,
		vec3 rotation,
		float duration = 1.0f,
		bool depthEnabled = true);
	 //add a Vector
	 void DebugShapes::AddVector(
		const float width,
		const vec3& fromPosition,
		const vec3& toPosition,
		vec4 color,
		float duration,
		bool depthEnabled);

	 DebugShapes();

	 void searchPathForPosition(vec3* positionToFind, vec4* colorToSet);
	 void addPointerToPath(std::vector<AstarHandler::AstarNode*>* AstarManigersPath);

	 void setupDebug(Render* currentRender);
	 void drawLines();
	 void drawShape();
	 void drawCrossHair();
	 void updateDebug();
};

//#pragma warning ( restore : 4005)
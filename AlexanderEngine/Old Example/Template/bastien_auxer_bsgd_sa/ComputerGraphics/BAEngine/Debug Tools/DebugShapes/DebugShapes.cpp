#ifdef DEBUG_SHAPES_ON

#include "DebugShapes.h"
#include <iostream>
#include <fstream>

#ifdef _DEBUG
	#include "..\..\Debug Tools\Memmory Maniger\DebugMemory.h"
#endif

//bring back middle name
//glDrawPixels
void DebugShapes::AddVector(
	const float width,
	const vec3& fromPosition,
	const vec3& toPosition,
	vec4 color,
	float duration,
	bool depthEnabled)
{
	Line newLine;

	newLine.verts[0].position = vec3(fromPosition.x, fromPosition.y, fromPosition.z);
	newLine.verts[1].position = vec3(toPosition.x, toPosition.y, toPosition.z);

	newLine.verts[0].color = vec3(color.x, color.y, color.z);
	newLine.verts[1].color = vec3(0, 0, 0);

	newLine.indexs[0] = 0;
	newLine.indexs[1] = 1;

	int sizeOfVetecies = sizeof(Vetecies);
	int sizeOfIndexes = sizeof(GLushort);

	int vetexbufferSize = 2 * sizeOfVetecies;
	int indexbufferSize = 2 * sizeOfIndexes;

	newLine.shapeData.vertexBufferSize = vetexbufferSize;
	newLine.shapeData.indexBufferSize = indexbufferSize;
	newLine.shapeData.numIndices = 2;

	newLine.shapeData.vertsData = newLine.verts;
	newLine.shapeData.indicesData = newLine.indexs;

	newLine.shapeData = workingRender->createGeomitry2(vetexbufferSize, indexbufferSize, 2, newLine.verts, newLine.indexs, false, true);

	newLine.width = width;
	newLine.fromPosition = fromPosition;
	newLine.toPosition = toPosition;
	newLine.color = color;
	newLine.duration = duration;
	newLine.depthEnabled = depthEnabled;

	lineStorage[currentLineSlot++] = newLine;
}

//To the debug drawing queue:
// Adds a line segment to the debug drawing queue.
void DebugShapes::AddLine(
	const float width,
	const vec3& fromPosition,
	const vec3& toPosition,
	vec4 color,
	float duration,
	bool permanetShape,
	bool depthEnabled)
{
	Line newLine;

	newLine.verts[0].position = vec3(fromPosition.x, fromPosition.y, fromPosition.z);
	newLine.verts[1].position = vec3(toPosition.x, toPosition.y, toPosition.z);

	newLine.verts[0].color = vec3(color.x, color.y, color.z);
	newLine.verts[1].color = vec3(color.x, color.y, color.z);

	newLine.indexs[0] = 0;
	newLine.indexs[1] = 1;

	int sizeOfVetecies = sizeof(Vetecies);
	int sizeOfIndexes = sizeof(GLushort);

	int vetexbufferSize = 2 * sizeOfVetecies;
	int indexbufferSize = 2 * sizeOfIndexes;

	newLine.shapeData.vertexBufferSize = vetexbufferSize;
	newLine.shapeData.indexBufferSize = indexbufferSize;
	newLine.shapeData.numIndices = 2;

	newLine.shapeData.vertsData = newLine.verts;
	newLine.shapeData.indicesData = newLine.indexs;

	newLine.shapeData = workingRender->createGeomitry2(vetexbufferSize, indexbufferSize, 2, newLine.verts, newLine.indexs, false, true);

	newLine.width = width;
	newLine.fromPosition = fromPosition;
	newLine.toPosition = toPosition;
	newLine.color = color;
	newLine.duration = duration;
	newLine.depthEnabled = depthEnabled;
	newLine.permanet = permanetShape;

	lineStorage[currentLineSlot++] = newLine;
}
// Adds an axis-aligned cross/POINT (3 lines converging at a point) 
void DebugShapes::AddCross(
	const float width,
	const vec3& position,
	float duration,
	bool depthEnabled)
{
	width;
	Crosshair* temp = createCrosshair(position);
	temp->duration = duration;
	temp->depthEnabled = depthEnabled;
}

// Adds a wireframe sphere to the debug drawing queue.
void DebugShapes::AddSphere( 
	const vec3& centerPosition,
	vec4 color,
	vec3 rotation,
	float size,
	float duration,
	bool permanet,
	bool depthEnabled)
{
	createDebugRenderable(&sphere,&debugShadders,centerPosition,color,size,duration,rotation, permanet, depthEnabled);
}

//add a Vector
void DebugShapes::AddCube(
	const vec3& centerPosition,
	vec4 color,
	vec3 rotation,
	float duration,
	bool depthEnabled)
{
	createDebugRenderable(&cube,&debugShadders,centerPosition,color,1.0f,duration,rotation, false, depthEnabled);
}

void DebugShapes::addPointerToPath(std::vector<AstarHandler::AstarNode*>* AstarManigersPath)
{
	path = AstarManigersPath;
}

DebugShapes::DebugShapes()
{
	#ifdef DEBUGMENU_ON
		showPath = false;
	#endif
}

DebugShapes::Crosshair* DebugShapes::createCrosshair(vec3 position)
{
	Crosshair temp;

	temp.vertexData[0].position = vec3(position.x - 1, position.y, position.z);
	temp.vertexData[1].position = vec3(position.x + 1, position.y, position.z);
	temp.vertexData[2].position = vec3(position.x, position.y - 1, position.z);
	temp.vertexData[3].position = vec3(position.x, position.y + 1, position.z);
	temp.vertexData[4].position = vec3(position.x, position.y, position.z - 1);
	temp.vertexData[5].position = vec3(position.x, position.y, position.z + 1);

	temp.vertexData[0].color = vec3(1, 0, 0);
	temp.vertexData[1].color = vec3(0, 0, 0);
	temp.vertexData[2].color = vec3(0, 1, 0);
	temp.vertexData[3].color = vec3(0, 0, 0);
	temp.vertexData[4].color = vec3(0, 0, 1);
	temp.vertexData[5].color = vec3(0, 0, 0);

	temp.indexs[0] = 0;
	temp.indexs[1] = 1;
	temp.indexs[2] = 2;
	temp.indexs[3] = 3;
	temp.indexs[4] = 4;
	temp.indexs[5] = 5;

	temp.shapeData.numIndices = 6;
	
	temp.shapeData.vertsData = temp.vertexData;
	temp.shapeData.indicesData = temp.indexs;

	temp.shapeData = workingRender->createGeomitry2(6 * sizeof(Vetecies), 6 * sizeof(GLushort), 6, temp.vertexData, temp.indexs, false, true);
	

	crosshairStorage[currentCrossHairSlot] = temp;
	return &crosshairStorage[currentCrossHairSlot++];
}

void DebugShapes::searchPathForPosition(vec3* positionToFind, vec4* colorToSet)
{
	int pathSize = path->size();
	int foundAt = 0;
	bool nodeFound = false;
	for(int stepper = 0; stepper < pathSize && !nodeFound; stepper++)
	{
		if(path->at(stepper)->gameNode->position == *positionToFind)
		{
			nodeFound = true;
			foundAt = stepper;
		}
	}

	if(nodeFound)
	{
		if(foundAt == pathSize-1)
		{
			*colorToSet = vec4(0,1,0,1);
		}
		else
		{
			*colorToSet = vec4(1,1,0,1);
		}
	}
	else
	{
		*colorToSet = vec4(1,0,0,1);
	}
}

void DebugShapes::createDebugRenderable(Render::GeomitryInfo* shape, Render::shaderInfo* shaders, vec3 position, vec4 color, float size, float duration, vec3 rotation, bool permanet, bool enableDepth)
{
	DebugRenderable temp;
	temp.gemomitry = shape;
	temp.shaders = shaders;
	temp.position = position;
	temp.color = color;
	temp.scale = size;
	temp.lifeTime = duration;
	temp.rotation = rotation;
	temp.depthEnabled = enableDepth;
	temp.permanet = permanet;

	debugRenderableStorage[currentDebugShapeSlot++] = temp;
}

void DebugShapes::setupDebug(Render* currentRender)
{
	workingRender = currentRender;
	debugShadders = currentRender->createShaderInfo("ShaderCode\\debugVertexShader.LOL", "ShaderCode\\debugFragmentShader.LOL");
	
	ShapeData Cube = ShapeGenerator::makeCube();
	ShapeData Shpere = ShapeGenerator::makeSphere(20);
	
	cube = currentRender->createGeomitry(Cube.vertexBufferSize(), Cube.indexBufferSize(), Cube.numIndices, Cube.verts, Cube.indices, true);
	sphere = currentRender->createGeomitry(Shpere.vertexBufferSize(), Shpere.indexBufferSize(), Shpere.numIndices, Shpere.verts, Shpere.indices, true);

	currentLineSlot = 0;
	currentDebugShapeSlot = 0;
	currentCrossHairSlot = 0;
}

void DebugShapes::drawLines()
{
		for (int steper = 0; steper < currentLineSlot; steper++)
		{
			Line* currentRenderable = &lineStorage[steper];
			if(currentRenderable->permanet)
			{
				//draw shape
				workingRender->drawGeomotry(&currentRenderable->shapeData,&debugShadders,
					vec3(0,0,0), vec4(1,1,1,1),
					1.0f,
					0.0f,0.0f,0.0f,0);
				if(currentRenderable->depthEnabled)
				{
					glEnable(GL_DEPTH_TEST);
				}
				else
				{
					glDisable(GL_DEPTH_TEST);
				}
			}
			else if(currentRenderable->duration > 0)
			{
				//draw shape
				workingRender->drawGeomotry(&currentRenderable->shapeData,&debugShadders,
					vec3(0,0,0), vec4(1,1,1,1),
					1.0f,
					0.0f,0.0f,0.0f,0);
				currentRenderable->duration -= 1;
				if(currentRenderable->depthEnabled)
				{
					glEnable(GL_DEPTH_TEST);
				}
				else
				{
					glDisable(GL_DEPTH_TEST);
				}
			}
		}
}

void DebugShapes::drawShape()
{
	for (int steper = 0; steper < currentDebugShapeSlot; steper++)
	{
		DebugRenderable* currentRenderable = &debugRenderableStorage[steper];

		showPath;
		if(currentRenderable->permanet && showPath)
		{
			searchPathForPosition(&currentRenderable->position, &currentRenderable->color);

			workingRender->drawGeomotry(currentRenderable->gemomitry,&debugShadders,
				currentRenderable->position, currentRenderable->color,
				currentRenderable->scale,
				currentRenderable->rotation.x,currentRenderable->rotation.y,currentRenderable->rotation.z,0,true);

			if(currentRenderable->depthEnabled)
			{
				glEnable(GL_DEPTH_TEST);
			}
			else
			{
				glDisable(GL_DEPTH_TEST);
			}
		}
		else if(currentRenderable->permanet)
		{
			workingRender->drawGeomotry(currentRenderable->gemomitry,&debugShadders,
				currentRenderable->position, currentRenderable->color,
				currentRenderable->scale,
				currentRenderable->rotation.x,currentRenderable->rotation.y,currentRenderable->rotation.z,0,true);

			if(currentRenderable->depthEnabled)
			{
				glEnable(GL_DEPTH_TEST);
			}
			else
			{
				glDisable(GL_DEPTH_TEST);
			}
		}
		else if(currentRenderable->lifeTime > 0)
		{
			//draw shape
			workingRender->drawGeomotry(currentRenderable->gemomitry,&debugShadders,
				currentRenderable->position, currentRenderable->color,
				currentRenderable->scale,
				currentRenderable->rotation.x,currentRenderable->rotation.y,currentRenderable->rotation.z,0,true);
			currentRenderable->lifeTime -= 1;

			if(currentRenderable->depthEnabled)
			{
				glEnable(GL_DEPTH_TEST);
			}
			else
			{
				glDisable(GL_DEPTH_TEST);
			}
		}
	}
}

void DebugShapes::drawCrossHair()
{
	for (int steper = 0; steper < currentCrossHairSlot; steper++)
	{
		Crosshair* currentRenderable = &crosshairStorage[steper];
		if(currentRenderable->duration > 0)
		{
			//draw shape
			workingRender->drawGeomotry(&currentRenderable->shapeData,&debugShadders,
				vec3(0,0,0), vec4(1,1,1,1),
				1.0f,
				0.0f,0.0f,0.0f,0);
			currentRenderable->duration -= 1;
			if(currentRenderable->depthEnabled)
			{
				glEnable(GL_DEPTH_TEST);
			}
			else
			{
				glDisable(GL_DEPTH_TEST);
			}
		}
	}
}

void DebugShapes::updateDebug()
{
	//Draw Lines
	if(currentLineSlot > 0)
	{
		drawLines();
	}

	//Draw shapes
	if(currentDebugShapeSlot > 0)
	{
		drawShape();
	}

	if(currentCrossHairSlot > 0)
	{
		drawCrossHair();
	}
}

#endif
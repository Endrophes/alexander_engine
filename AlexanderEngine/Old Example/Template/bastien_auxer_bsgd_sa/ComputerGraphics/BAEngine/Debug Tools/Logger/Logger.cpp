

#ifdef LOG_ON
#include <string>
#include "Logger.h"


	vector <std::string> Logger::logList;
	vector <Severity> Logger::severityList;

	Logger::Logger(void){}
	Logger::~Logger(void){}

	void Logger::StringReplace(string& str, const string& from, const string& to)
	{
		size_t pos = 0;
		do 
		{
			pos = str.find(from, pos);
			if( pos != string::npos )
			{
				str.replace( pos, from.length(), to);
				pos += to.length();
			}
		} while (pos != string::npos);
	}

	string Logger::Sanitize(string str)
	{
		//location will cause inf loop  Log(Info,"Sanitize the log","test",__LINE__);

		StringReplace( str, ">" , "&lt" );
		StringReplace( str, "<" , "&gt" );

		return str;
	}

	void Logger::Log(Severity severity, const char* message, const char * logFile, int logLine)
	{
		std::stringstream ss;
		ss << logFile << "(" << logLine << ")"<<"<br>"; 
		ss << Sanitize(message);
		ss << "<br>";
		std::string logEntry;
		logEntry = ss.str();
	
		logList.push_back(logEntry);
		severityList.push_back(severity);
	}

	void Logger::shutDown()
	{
		WriteFile();
	}
	
	void Logger::WriteFile()
{
	std::ofstream myFile;
	
	const char* fileName = "logger_test.html";
	myFile.open(fileName);

	Log(Info,"writing the log","test",__LINE__);

	myFile << "<!DOCTYPE html>" << std::endl << "<html>" 
		<< std::endl << "<head>" << std::endl << "<title>Log File</title>" << std::endl << "</head>"
		<< std::endl << "<body bgcolor = '#FFFFFF' >" << std::endl << "<h2>Log File</h2>"
		<< std::endl << THE_LIST;
	for(unsigned int i = 0; i < logList.size(); i ++)
	{
		switch (severityList[i])
		{
			case Info:
				myFile << "<font color=\"#666666\">";
				myFile << "\n";
				myFile << "\n";
				break;

			case Warning:
				myFile << "<font color=\"#FFCC00\">";
				myFile << "\n";
				myFile << "\n";
				break;

			case Error:
				myFile << "<font color=\"#990000\">";
				myFile << "\n";
				myFile << "\n";
				break;

			case Severe:
				myFile << "<font color=\"#FF0000\">";
				myFile << "\n";
				myFile << "\n";
				break;
		}
		myFile << logList[i].c_str() << "</font><br>";
	}
	myFile << "</body>" << std::endl << "</html>";	
	myFile.close();
}



#endif
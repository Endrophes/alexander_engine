#ifdef DEBUGMENU_ON

#include <Debug Tools\DebugMenu\DebugMenu.h>

#ifdef _DEBUG
	#include <Debug Tools\Memmory Maniger\DebugMemory.h>
#endif

DebugMenu::DebugMenu()
{
	debugLayout = new QVBoxLayout();
	tabWidget = new QTabWidget();
	debugLayout->addWidget(tabWidget);
	currentFlaotSlot = 0;
	currentIntSlot   = 0;
	currentBoolSlot  = 0;
	currentVec3Slot  = 0;
	currentCheckBoxSlot = 0;
	currentSliderSlot = 0;
	numberOfQWidgets =0;
}

DebugMenu::~DebugMenu()
{

}

std::string DebugMenu::convertFloatToString(float* value)
{
	std::ostringstream ss;
	ss << *value;
	return ss.str();
}

std::string DebugMenu::convertIntToString(int* value)
{
	std::ostringstream ss;
	ss << *value;
	return ss.str();
}

std::string DebugMenu::convertBoolToString(bool* value)
{
	std::ostringstream ss;
	ss << *value;
	return ss.str();
}

std::string DebugMenu::convertVec3ToString(vec3* value)
{
	std::ostringstream ss;
	ss << value->x;
	ss << ", ";
	ss << value->y;
	ss << ", ";
	ss << value->z;
	return ss.str();
}

void DebugMenu::addFloat(char* nameOfVariable, float &value, int tabIndex)
{
	floatDisplay temp;
	temp.name = nameOfVariable;
	temp.value = &value;
	std::string addOn = nameOfVariable;
	addOn += ": ";
	temp.nameOfVarible = new QLabel(addOn.c_str());
	std::string valueInString = convertFloatToString(temp.value);
	temp.valueOfVarible = new QLabel(valueInString.c_str());
	temp.tabIndex = tabIndex;
	floatHolder[currentFlaotSlot++] = temp;

}

void DebugMenu::addInt(char* nameOfVariable, int &value, int tabIndex)
{
	intDisplay temp;
	temp.name = nameOfVariable;
	temp.value = &value;
	std::string addOn = nameOfVariable;
	addOn += ": ";
	temp.nameOfVarible = new QLabel(addOn.c_str());
	std::string valueInString = convertIntToString(temp.value);
	temp.valueOfVarible = new QLabel(valueInString.c_str());
	temp.tabIndex = tabIndex;
	intHolder[currentIntSlot++] = temp;
}

void DebugMenu::addBool(char* nameOfVariable, bool &value, int tabIndex)
{
	boolDisplay temp;
	temp.name = nameOfVariable;
	temp.value = &value;
	std::string addOn = nameOfVariable;
	addOn += ": ";
	temp.nameOfVarible = new QLabel(addOn.c_str());
	temp.tabIndex = tabIndex;
	if(&value)
	{
		temp.valueOfVarible = new QLabel("True");
	}
	else
	{
		temp.valueOfVarible = new QLabel("False");
	}

	boolHolder[currentBoolSlot++] = temp;
}

void DebugMenu::addVec3(char* nameOfVariable, vec3 &value, int tabIndex)
{
	vec3Display temp;
	temp.name = nameOfVariable;
	temp.value = &value;
	std::string addOn = nameOfVariable;
	addOn += ": ";
	temp.nameOfVarible = new QLabel(addOn.c_str());
	std::string valueInString = convertVec3ToString(temp.value);
	temp.valueOfVarible = new QLabel(valueInString.c_str());
	temp.tabIndex = tabIndex;
	vec3Holder[currentVec3Slot++] = temp;
}

void DebugMenu::addCheckBox(char* nameOfVariable, bool &value, int tabIndex)
{
	boolCheckBox temp;
	temp.name = nameOfVariable;
	temp.value = &value;
	std::string addOn = nameOfVariable;
	addOn += ": ";
	temp.nameOfVarible = new QLabel(addOn.c_str());
	temp.tabIndex = tabIndex;
	temp.checkBox = new QCheckBox();
	temp.checkBox->setFixedHeight(100);

	checkBoxHolder[currentCheckBoxSlot++] = temp;
}

void DebugMenu::addFlaotSlider(char* nameOfVariable, float &value, float min, float max, int tabIndex, float startValue, float grandularity)
{
	floatSlider temp;
	temp.name = nameOfVariable;
	temp.value = &value;
	temp.tabIndex = tabIndex;
	std::string addOn = nameOfVariable;
	addOn += ": ";
	temp.nameOfVarible = new QLabel(addOn.c_str());
	temp.oldValue = value;

	temp.slider = new DebugSlider(min, max, false, grandularity);
	temp.slider->setFixedHeight(50);
	if(startValue >= min && startValue <= max)
	{
		temp.slider->setValue(startValue);
	}
	else
	{
		temp.slider->setValue(min);
	}

	sliderHolder[currentSliderSlot++] = temp;
}

int DebugMenu::addTab(char* nameOfTab)
{
	QWidget* tron = new QWidget();
	qWidgetHolder.push_back(tron);
	
	int index = tabWidget->addTab(tron, nameOfTab);
	numberOfQWidgets++;
	return index;
}

QHBoxLayout* DebugMenu::getFlaotValues(int tabIndex)
{
	QHBoxLayout* floatDisplayBar = new QHBoxLayout();

	for(int currentFloat = 0; currentFloat < currentFlaotSlot; currentFloat++)
	{
		if(floatHolder[currentFloat].tabIndex == tabIndex)
		{
			floatDisplayBar->addWidget(floatHolder[currentFloat].nameOfVarible);
			floatDisplayBar->addWidget(floatHolder[currentFloat].valueOfVarible);
		}
	}

	return floatDisplayBar;
}

QHBoxLayout* DebugMenu::getIntValues(int tabIndex)
{
	QHBoxLayout* intDisplayBar = new QHBoxLayout();

	for(int currentInt = 0; currentInt < currentIntSlot; currentInt++)
	{
		if(intHolder[currentInt].tabIndex == tabIndex)
		{
			intDisplayBar->addWidget(intHolder[currentInt].nameOfVarible);
			intDisplayBar->addWidget(intHolder[currentInt].valueOfVarible);
		}
	}
	return intDisplayBar;
}

QHBoxLayout* DebugMenu::getBoolValues(int tabIndex)
{
	QHBoxLayout* BoolDisplayBar = new QHBoxLayout();
	
	for(int currentBool = 0; currentBool < currentBoolSlot; currentBool++)
	{
		if(boolHolder[currentBool].tabIndex == tabIndex)
		{
			BoolDisplayBar->addWidget(boolHolder[currentBool].nameOfVarible);
			BoolDisplayBar->addWidget(boolHolder[currentBool].valueOfVarible);
		}
	}

	return BoolDisplayBar;
}

QHBoxLayout* DebugMenu::getVec3Values(int tabIndex)
{
	QHBoxLayout* vec3DisplayBar = new QHBoxLayout();

	for(int currentVec3 = 0; currentVec3 < currentVec3Slot; currentVec3++)
	{
		if(vec3Holder[currentVec3].tabIndex == tabIndex)
		{
			vec3DisplayBar->addWidget(vec3Holder[currentVec3].nameOfVarible);
			vec3DisplayBar->addWidget(vec3Holder[currentVec3].valueOfVarible);
		}
	}

	return vec3DisplayBar;
}

QHBoxLayout* DebugMenu::getCheckBoxes(int tabIndex)
{
	QHBoxLayout* checkBoxDisplayBar = new QHBoxLayout();

	for(int currentCheckBox = 0; currentCheckBox < currentCheckBoxSlot; currentCheckBox++)
	{
		if(checkBoxHolder[currentCheckBox].tabIndex == tabIndex)
		{
			checkBoxDisplayBar->addWidget(checkBoxHolder[currentCheckBox].nameOfVarible);
			checkBoxDisplayBar->addWidget(checkBoxHolder[currentCheckBox].checkBox);
		}
	}

	return checkBoxDisplayBar;
}

QHBoxLayout* DebugMenu::getSLiders(int tabIndex)
{
	QHBoxLayout* sliderDisplayBar = new QHBoxLayout();

	for(int currentSlider = 0; currentSlider < currentSliderSlot; currentSlider++)
	{
		if(sliderHolder[currentSlider].tabIndex == tabIndex)
		{
			sliderDisplayBar->addWidget(sliderHolder[currentSlider].nameOfVarible);
			sliderDisplayBar->addWidget(sliderHolder[currentSlider].slider);
		}
	}

	return sliderDisplayBar;
}

QVBoxLayout* DebugMenu::getAllValues(int tabIndex)
{
	QVBoxLayout* debugValues = new QVBoxLayout();

	debugValues->addLayout(getSLiders(tabIndex));
	debugValues->addLayout(getFlaotValues(tabIndex));
	debugValues->addLayout(getIntValues(tabIndex));
	debugValues->addLayout(getBoolValues(tabIndex));
	debugValues->addLayout(getCheckBoxes(tabIndex));
	debugValues->addLayout(getVec3Values(tabIndex));

	return debugValues;
}

void DebugMenu::displayValues()
{
	//debugLayout->addLayout(getAllValues());
	for(int stepper = 0; stepper < numberOfQWidgets; stepper++)
	{
		qWidgetHolder[stepper]->setLayout(getAllValues(stepper));
	}


}

void DebugMenu::update()
{
	for(int currentBool = 0; currentBool < currentBoolSlot; currentBool++)
	{
		boolHolder[currentBool].updateValue();
	}

	for(int currentVec3 = 0; currentVec3 < currentVec3Slot; currentVec3++)
	{
		vec3Holder[currentVec3].updateValue();
	}

	for(int currentFloat = 0; currentFloat < currentFlaotSlot; currentFloat++)
	{
		floatHolder[currentFloat].updateValue();
	}

	for(int currentInt = 0; currentInt < currentIntSlot; currentInt++)
	{
		intHolder[currentInt].updateValue();
	}

	for(int currentCheckBox = 0; currentCheckBox < currentCheckBoxSlot; currentCheckBox++)
	{
		checkBoxHolder[currentCheckBox].updateValue();
	}

	for(int currentSlider = 0; currentSlider < currentSliderSlot; currentSlider++)
	{
		sliderHolder[currentSlider].updateValue();
	}
}

#endif
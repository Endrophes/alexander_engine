#include "NodeHandler.h"
#include <QtGui\QMouseEvent>
#include <QtGui\QKeyEvent>


char* NodeHandler::grabNodeData(char* pointerToData, uint numNodes, uint numConections)
{

	numberOfTotalGameNodes = numNodes;
	numberOfTotalConnections = numConections;

	gameNodeStorage = reinterpret_cast<gameNode*>(pointerToData);
	gameConnectionStorage = reinterpret_cast<gameConnection*>(pointerToData + (numNodes * sizeof(gameNode)));

	char* pointerToNodeStorage = pointerToData;
	char* pointerToConnections = pointerToData + (numNodes * sizeof(gameNode));

	char* connectionPointer = pointerToConnections;
	char* gameNodePointer = pointerToNodeStorage;

	//fix pointers to connections
	for(uint loadNode = 0; loadNode < numNodes; loadNode++)
	{
		gameNode* currentNode = reinterpret_cast<gameNode*>(gameNodePointer);
		if(currentNode->numberofConnections != 0)
		{
			currentNode->connections = reinterpret_cast<gameConnection*>(connectionPointer);
			connectionPointer += (sizeof(gameConnection) * currentNode->numberofConnections);
		}
		gameNodePointer += sizeof(gameNode);
	}
	gameNodePointer = pointerToNodeStorage;
	connectionPointer = pointerToConnections;

	//Fix conections To Targets
	for(uint loadConnection = 0; loadConnection < numConections; loadConnection++)
	{
		gameConnection* currentConection = reinterpret_cast<gameConnection*>(connectionPointer);
		uint stepperToTarget = *reinterpret_cast<uint*>(connectionPointer + sizeof(float));
		currentConection->target = reinterpret_cast<gameNode*>(gameNodePointer + (stepperToTarget * sizeof(gameNode)));
		connectionPointer += sizeof(gameConnection);
	}
	connectionPointer = pointerToConnections;

	pointerToData += (numberOfTotalGameNodes * sizeof(gameNode)) + (numberOfTotalConnections *  sizeof(gameConnection));

	return pointerToData;
}

uint NodeHandler::getNumberOfConnections()
{
	return numberOfTotalConnections;
}

uint NodeHandler::getNumberOfNodes()
{
	return numberOfTotalGameNodes;
}

NodeHandler::NodeHandler()
{
	//nodeStrage[];

}

//NodeHandler::NodeHandler(mat4* projection, mat4* cameraview, Render* currentRenderer)
//{
//	projectionMatrix = projection;
//	cameraViewMatrix = cameraview;
//	worldCreater = currentRenderer;
//	Threshold = vec3(0,0,0);
//
//	masterNodeSelected = false;
//
//	currentNode = 0;
//	currentArrow = 0;
//
//	mainNode = worldCreater->uniformHandiler.addIntUniform("mainNode");
//	seconedNode = worldCreater->uniformHandiler.addIntUniform("seconedNode");
//
//	nodeShader = worldCreater->createShaderInfo("ShaderCode\\NodeVertexShader.lol", "ShaderCode\\NodeFragmentShader.lol");
//
//	shpereSize = 0.3f;
//	shpere = worldCreater->binaryLoader.loadShape("bin\\shpere.bin");
//	worldCreater->addGeometryData(&shpere);
//	nodeRenderable = worldCreater->createRenderable(&shpere,&nodeShader, vec3(0,0,0),shpereSize, 0.0f,0.0f,0.0f, GL_TRIANGLES, 1);
//	
//	cylenderGI = worldCreater->binaryLoader.loadShape("bin\\cylender.bin");
//	worldCreater->addGeometryData(&cylenderGI);
//	arrowRenderable = worldCreater->createRenderable(&cylenderGI,&nodeShader, vec3(0,0,0),1.0f, 0.0f,0.0f,0.0f, GL_TRIANGLES, 1);
//
//	coneGI = worldCreater->binaryLoader.loadShape("bin\\cone.bin");
//	worldCreater->addGeometryData(&coneGI);
//	coneRenderable = worldCreater->createRenderable(&coneGI,&nodeShader, vec3(0,0,0),1.0f, 0.0f,0.0f,0.0f, GL_TRIANGLES, 1);
//
//	maxnumberOfNodes = 10;
//}

NodeHandler::~NodeHandler()
{
	//delete nodeStorage;
}
#pragma once

#ifndef NODEHADLER
#define NODEHADLER


#define NODE_STORAGE_SIZE 1000
#define LINKED_NODE_STORAGE_SIZE 50
#define ARROW_STORAGE_SIZE 50

#include "..\Render\Render.h"
#include "..\ExportHeader.h"
#include <vector>

using glm::vec3;
using glm::mat4;

class ENGINE_SHARED NodeHandler
{
public:
	class gameNode;

	//in game info
	class gameConnection
	{
	public:
		float cost;
		gameNode* target;
	};

	class gameNode
	{
	public:
		vec3 position;
		int numberofConnections;
		gameConnection* connections;
	};

	//gameNode nodeStrage[];

	uint numberOfTotalGameNodes;
	uint numberOfTotalConnections;

	gameNode* gameNodeStorage;
	gameConnection* gameConnectionStorage;

public:
	uint getNumberOfConnections();
	uint getNumberOfNodes();

	char* grabNodeData(char* pointerToData, uint numNodes, uint numConections);

	NodeHandler();
	NodeHandler(mat4* projection, mat4* cameraview, Render* currentRenderer);
	~NodeHandler();
};

#endif
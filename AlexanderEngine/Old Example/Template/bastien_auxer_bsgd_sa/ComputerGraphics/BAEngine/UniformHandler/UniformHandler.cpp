#pragma once

#include <UniformHandler\UniformHandler.h>
#include <iostream>

#ifdef _DEBUG
	#include <Debug Tools\Memmory Maniger\DebugMemory.h>
#endif

UniformHandler::UniformHandler()
{
	currentFloat = 0;
	currentInt = 0;
	currentVec2 = 0;
	currentVec3 = 0;
	currentVec4 = 0;
	currentMat3 = 0;
	currentMat4 = 0;
}

float* UniformHandler::addFloatUniform(std::string locationName)
{
	FloatUnifrom temp;
	temp.nameInUniform = locationName;
	temp.Value = 0.0f;
	temp.location = -1;
	
	FloatUnifromHolder[currentFloat] = temp;

	return &FloatUnifromHolder[currentFloat++].Value; 
}

int*   UniformHandler::addIntUniform(std::string locationName)
{
	IntUnifrom temp;
	temp.nameInUniform = locationName;
	temp.Value = 0;
	temp.location = -1;
	
	IntUnifromHolder[currentInt] = temp;

	return &IntUnifromHolder[currentInt++].Value; 
}

vec2*  UniformHandler::addVec2Uniform(std::string locationName)
{
	Vec2Unifrom temp;
	temp.nameInUniform = locationName;
	temp.Value = vec2(0.0f,0.0f);
	temp.location = -1;
	
	Vec2UnifromHolder[currentVec2] = temp;

	return &Vec2UnifromHolder[currentVec2++].Value; 
}

vec3*  UniformHandler::addVec3Uniform(std::string locationName)
{
	Vec3Unifrom temp;
	temp.nameInUniform = locationName;
	temp.Value = vec3(0.0f,0.0f,0.0f);
	temp.location = -1;
	
	Vec3UnifromHolder[currentVec3] = temp;

	return &Vec3UnifromHolder[currentVec3++].Value; 
}

vec4*  UniformHandler::addVec4Uniform(std::string locationName)
{
	Vec4Unifrom temp;
	temp.nameInUniform = locationName;
	temp.Value = vec4(0.0f,0.0f,0.0f,0.0f);
	temp.location = -1;
	
	Vec4UnifromHolder[currentVec4] = temp;

	return &Vec4UnifromHolder[currentVec4++].Value; 
}

mat3*  UniformHandler::addMat3Uniform(std::string locationName, bool trasposeMatrix)
{
	Mat3Unifrom temp;
	temp.nameInUniform = locationName;
	temp.Value = mat3();
	temp.location = -1;
	temp.traspose = trasposeMatrix;
	
	Mat3UnifromHolder[currentMat3] = temp;

	return &Mat3UnifromHolder[currentMat3++].Value; 
}

mat4*  UniformHandler::addMat4Uniform(std::string locationName, bool trasposeMatrix)
{
	Mat4Unifrom temp;
	temp.nameInUniform = locationName;
	temp.Value = mat4();
	temp.location = -1;
	temp.traspose = trasposeMatrix;
	
	Mat4UnifromHolder[currentMat4] = temp;

	return &Mat4UnifromHolder[currentMat4++].Value; 
}

void UniformHandler::sendIntUniform(GLuint programID, int value, std::string name)
{
	int location = glGetUniformLocation(programID, name.c_str());
	glUniform1i(location, value);
}

void UniformHandler::getUniformLocations(GLuint programID)
{
	for(int selectedFloat = 0; selectedFloat < currentFloat; selectedFloat++)
	{
		FloatUnifromHolder[selectedFloat].location = glGetUniformLocation(programID, FloatUnifromHolder[selectedFloat].nameInUniform.c_str());
	}

	for(int selectedInt = 0; selectedInt < currentInt; selectedInt++)
	{
		IntUnifromHolder[selectedInt].location = glGetUniformLocation(programID, IntUnifromHolder[selectedInt].nameInUniform.c_str());
	}

	for(int selectedVec2 = 0; selectedVec2 < currentVec2; selectedVec2++)
	{
		Vec2UnifromHolder[selectedVec2].location = glGetUniformLocation(programID, Vec2UnifromHolder[selectedVec2].nameInUniform.c_str());
	}

	for(int selectedVec3 = 0; selectedVec3 < currentVec3; selectedVec3++)
	{
		Vec3UnifromHolder[selectedVec3].location = glGetUniformLocation(programID, Vec3UnifromHolder[selectedVec3].nameInUniform.c_str());
	}

	for(int selectedVec4 = 0; selectedVec4 < currentVec4; selectedVec4++)
	{
		Vec4UnifromHolder[selectedVec4].location = glGetUniformLocation(programID, Vec4UnifromHolder[selectedVec4].nameInUniform.c_str());
	}

	for(int selectedMat3 = 0; selectedMat3 < currentMat3; selectedMat3++)
	{
		Mat3UnifromHolder[selectedMat3].location = glGetUniformLocation(programID, Mat3UnifromHolder[selectedMat3].nameInUniform.c_str());
	}

	for(int selectedMat4 = 0; selectedMat4 < currentMat4; selectedMat4++)
	{
		Mat4UnifromHolder[selectedMat4].location = glGetUniformLocation(programID, Mat4UnifromHolder[selectedMat4].nameInUniform.c_str());
		if(Mat4UnifromHolder[selectedMat4].nameInUniform == "Bison")
		{
			//std::cout << Mat4UnifromHolder[selectedMat4].location << std::endl;
		}

	}
}

void UniformHandler::sendUniforms(GLuint programID)
{
	getUniformLocations(programID);

	for(int selectedFloat = 0; selectedFloat < currentFloat; selectedFloat++)
	{
		glUniform1f(FloatUnifromHolder[selectedFloat].location, FloatUnifromHolder[selectedFloat].Value);
	}

	for(int selectedInt = 0; selectedInt < currentInt; selectedInt++)
	{
		glUniform1i(IntUnifromHolder[selectedInt].location, IntUnifromHolder[selectedInt].Value);
	}

	for(int selectedVec2 = 0; selectedVec2 < currentVec2; selectedVec2++)
	{
		glUniform2fv(Vec2UnifromHolder[selectedVec2].location,1, &Vec2UnifromHolder[selectedVec2].Value[0]);
	}

	for(int selectedVec3 = 0; selectedVec3 < currentVec3; selectedVec3++)
	{
		glUniform3fv(Vec3UnifromHolder[selectedVec3].location, 1, &Vec3UnifromHolder[selectedVec3].Value[0]);
	}

	for(int selectedVec4 = 0; selectedVec4 < currentVec4; selectedVec4++)
	{
		glUniform4fv(Vec4UnifromHolder[selectedVec4].location, 1, &Vec4UnifromHolder[selectedVec4].Value[0]);
	}

	for(int selectedMat3 = 0; selectedMat3 < currentMat3; selectedMat3++)
	{
		glUniformMatrix3fv(Mat3UnifromHolder[selectedMat3].location, 1, Mat3UnifromHolder[selectedMat3].traspose, &Mat3UnifromHolder[selectedMat3].Value[0][0]);
	}

	for(int selectedMat4 = 0; selectedMat4 < currentMat4; selectedMat4++)
	{
		glUniformMatrix4fv(Mat4UnifromHolder[selectedMat4].location, 1, Mat4UnifromHolder[selectedMat4].traspose, &Mat4UnifromHolder[selectedMat4].Value[0][0]);
	}
}

void UniformHandler::reset()
{
	currentFloat = 0;
	currentInt = 0;
	currentMat3 = 0;
	currentMat4 = 0;
	currentVec2 = 0;
	currentVec3 = 0;
	currentVec4 = 0;
}
#pragma once

#include <glm.hpp>
#include <GL\glew.h>
#include <gtx\transform.hpp>
#include <sstream>
#include <ExportHeader.h>

#define NUMBER_OF_FLOAT_UNIFORMS 30
#define NUMBER_OF_INT_UNIFORMS 30
#define NUMBER_OF_VEC2_UNIFORMS 30
#define NUMBER_OF_VEC3_UNIFORMS 30
#define NUMBER_OF_VEC4_UNIFORMS 30
#define NUMBER_OF_MAT3_UNIFORMS 30
#define NUMBER_OF_MAT4_UNIFORMS 30

using glm::vec2;
using glm::vec3;
using glm::vec4;
using glm::mat3;
using glm::mat4;

class ENGINE_SHARED UniformHandler
{
	class FloatUnifrom
	{
	public:
		int location;
		std::string nameInUniform;
		float Value;
	};

	class IntUnifrom
	{
	public:
		int location;
		std::string nameInUniform;
		int Value;
	};

	class Vec2Unifrom
	{
	public:
		int location;
		std::string nameInUniform;
		vec2 Value;
	};

	class Vec3Unifrom
	{
	public:
		int location;
		std::string nameInUniform;
		vec3 Value;
	};
	
	class Vec4Unifrom
	{
	public:
		int location;
		std::string nameInUniform;
		vec4 Value;
	};

	class Mat3Unifrom
	{
	public:
		bool traspose;
		int location;
		std::string nameInUniform;
		mat3 Value;
	};

	class Mat4Unifrom
	{
	public:
		bool traspose;
		int location;
		std::string nameInUniform;
		mat4 Value;
	};
	
	int currentFloat;
	int currentInt;
	int currentVec2;
	int currentVec3;
	int currentVec4;
	int currentMat3;
	int currentMat4;

	FloatUnifrom FloatUnifromHolder[NUMBER_OF_FLOAT_UNIFORMS];
	IntUnifrom   IntUnifromHolder[NUMBER_OF_INT_UNIFORMS];
	Vec2Unifrom  Vec2UnifromHolder[NUMBER_OF_VEC2_UNIFORMS];
	Vec3Unifrom  Vec3UnifromHolder[NUMBER_OF_VEC3_UNIFORMS];
	Vec4Unifrom  Vec4UnifromHolder[NUMBER_OF_VEC4_UNIFORMS];
	Mat3Unifrom  Mat3UnifromHolder[NUMBER_OF_MAT3_UNIFORMS];
	Mat4Unifrom  Mat4UnifromHolder[NUMBER_OF_MAT4_UNIFORMS];

public:

	float* addFloatUniform(std::string locationName);
	int*   addIntUniform(std::string locationName);
	vec2*  addVec2Uniform(std::string locationName);
	vec3*  addVec3Uniform(std::string locationName);
	vec4*  addVec4Uniform(std::string locationName);
	mat3*  addMat3Uniform(std::string locationName, bool trasposeMatrix = false);
	mat4*  addMat4Uniform(std::string locationName, bool trasposeMatrix = false);
	
	void sendIntUniform(GLuint programID, int value, std::string name);

	void getUniformLocations(GLuint programID);
	void sendUniforms(GLuint programID);
	void reset();

	UniformHandler();
};
#pragma once

#ifndef ENGINE_CAMERA_ENITY_H
#define ENGINE_CAMERA_ENITY_H

#include <glm.hpp>
#include <Enity\Entity.h>
#include <Camera\Camera.h>

namespace Entites
{
	class ENGINE_SHARED CameraEnity : public Entity, public Camera
	{
	public:
		CameraEnity(void);
		~CameraEnity(void);

		void update();
	};
}

#endif
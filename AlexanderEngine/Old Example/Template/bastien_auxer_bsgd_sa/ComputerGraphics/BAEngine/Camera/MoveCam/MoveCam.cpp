#include "MoveCam.h"
#include <gtx\transform.hpp>

#ifdef _DEBUG
	#include <Debug Tools\Memmory Maniger\DebugMemory.h>
#endif

MoveCam::MoveCam(void)
{
	MOVEMENT_SPEED = 0.03f;
	cameraMove = false;
}

MoveCam::~MoveCam(void)
{

}

void MoveCam::setMoveSpeed(float newMoveSpeed)
{
	MOVEMENT_SPEED = newMoveSpeed;
}

void MoveCam::moveForward()
{
	position += MOVEMENT_SPEED * viewDirection;
}

void MoveCam::moveBack()
{
	position += -MOVEMENT_SPEED * viewDirection;
}

void MoveCam::moveLeft()
{
	moveDirection = glm::cross(viewDirection, UP);
	position += MOVEMENT_SPEED * moveDirection;
}

void MoveCam::moveRight()
{
	moveDirection = glm::cross(viewDirection, UP);
	position += -MOVEMENT_SPEED * moveDirection;
}

void MoveCam::moveUp()
{
	position += MOVEMENT_SPEED * UP;
}

void MoveCam::moveDown()
{
	position += -MOVEMENT_SPEED * UP;
}

void MoveCam::mouseUpdate(const glm::vec2& newMousePosition)
{
	glm::vec2 mouseDelta = newMousePosition - oldMousePosition;
	if(glm::length(mouseDelta) > 50.0f)
	{
		oldMousePosition = newMousePosition;
	}
	else
	{
		const float ROTATIONAL_SPEED = 0.5f;
	    moveDirection = glm::cross(viewDirection, UP);
		glm::mat4 rotator = glm::rotate(-mouseDelta.x * ROTATIONAL_SPEED,UP) * 
							glm::rotate(-mouseDelta.y * ROTATIONAL_SPEED, moveDirection);
		viewDirection = glm::mat3(rotator) * viewDirection;
		oldMousePosition = newMousePosition;
	}
}
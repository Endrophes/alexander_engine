#pragma once

#include <Camera\Camera.h>
#include <gtx\transform.hpp>

#ifdef _DEBUG
	#include <Debug Tools\Memmory Maniger\DebugMemory.h>
#endif

Camera::Camera(void) : 
	///*
	viewDirection(0.0f,0.0f,-1.0f),
	position(0.0f,0.0f,0.0f),
	////*/
	/*
	viewDirection(-0.597715557f,-0.342019409f,-0.725087523f),
	position(5.91914845f,3.13708806f,5.10574102f),
	////*/
	/*
	viewDirection(0.0173033f, -0.414693f, -0.909406f),
	position(0.0f,2.0f,5.0f),
	////*/
	UP(0.0f,1.0f,0.0f)
{
}

Camera::~Camera(void)
{

}

glm::mat4 Camera::getWorldToViewMatrix() const
{
	return glm::lookAt(position, position + viewDirection, UP);
}

glm::vec3 Camera::getViewDirection()
{
	return viewDirection;
}

void Camera::setViewDirection(glm::vec3 newViewDirection)
{
	viewDirection = newViewDirection;
}

glm::vec3 Camera::getPosition()
{
	return position;
}

void Camera::setPosition(glm::vec3 newPosition)
{
	position = newPosition;
}
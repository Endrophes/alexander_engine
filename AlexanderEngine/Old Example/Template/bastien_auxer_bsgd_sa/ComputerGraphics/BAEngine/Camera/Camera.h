#pragma once

#ifndef ENGINE_CAMERA_H
#define ENGINE_CAMERA_H

#include <glm.hpp>
#include <ExportHeader.h>

class ENGINE_SHARED Camera
{
public:

	glm::vec3 UP;
	glm::vec3 position;
	glm::vec3 viewDirection;

	Camera(void);
	~Camera(void);

	glm::vec3 getViewDirection();
	void setViewDirection(glm::vec3 newViewDirection);

	glm::vec3 getPosition();
	void setPosition(glm::vec3 newPosition);

	glm::mat4 getWorldToViewMatrix() const;
};

#endif
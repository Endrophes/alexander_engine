#pragma once

#include <ExportHeader.h>

class ENGINE_SHARED FileLoader
{

public:
	FileLoader();
	~FileLoader();

	char* loadFileIntoBuffer(const char* filePath, const char* fileMode);
	char* loadBinaryIntoBuffer(const char* filePath);
};
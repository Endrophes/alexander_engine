#include <Loader\FileLoader.h>
#include <fstream>
#include <Windows.h>

#ifdef _DEBUG
	#include <Debug Tools\Memmory Maniger\DebugMemory.h>
#endif

char* FileLoader::loadFileIntoBuffer(const char* filePath, const char* fileMode)
{
	char* buffer;

	FILE * file;
	unsigned long size;
	
	#pragma warning( disable: 4996)

	file = fopen(filePath, fileMode);
	if(file == NULL)
	{
		fputs("could not load file ", stderr);
		exit(1);
	}

	//get file size
	fseek(file, 0, SEEK_END);
	size = ftell (file);
	rewind(file);

	//Allocate memory
	buffer = (char*)malloc(sizeof(char)*size);
	if (buffer == NULL) 
	{
		fputs ("Memory error ",stderr); 
		exit(2);
	}

	//copyinfo
	size_t result = fread(buffer,1, size, file);
	if (result != size) 
	{
		//DARN
		fputs ("Reading error ",stderr); 
		exit(3);
	}

	//close file
	fclose (file);

	return buffer;
}

char* FileLoader::loadBinaryIntoBuffer(const char* filePath)
{
	char* buffer;

	FILE * file;
	unsigned long size;
	
	#pragma warning( disable: 4996)

	file = fopen(filePath, "rb");
	if(file == NULL)
	{
		fputs("could not load file ", stderr);
		exit(1);
	}

	//get file size
	fseek(file, 0, SEEK_END);
	size = ftell (file);
	rewind(file);

	//Allocate memory
	buffer = new char[sizeof(char)*size];
	//buffer = (char*)malloc(sizeof(char)*size);
	if (buffer == NULL) 
	{
		fputs ("Memory error ",stderr); 
		exit(2);
	}

	//copyinfo
	size_t result = fread(buffer,1, size, file);
	if (result != size) 
	{
		//DARN
		fputs ("Reading error ",stderr); 
		exit(3);
	}

	//close file
	fclose (file);

	//delete[] buffer;

	return buffer;
}

FileLoader::FileLoader()
{

}

FileLoader::~FileLoader()
{

}
#pragma once
#include <Audio\Audio.h>
#include <irrKlang.h>
#include <string.h>

#ifdef _DEBUG
	#include <Debug Tools\Memmory Maniger\DebugMemory.h>
#endif

namespace Sounds{

	Audio Audio::instance;

	Audio::Audio()
	{
		
	}

	void Audio::playFile(std::string key, bool Loop)
	{
		if (!engine)
		{
			engine = irrklang::createIrrKlangDevice();
			if (!engine)
			{
				printf("Could not startup engine\n");
			}
		}

		if(audioCollection.find(key) != audioCollection.end())
		{
			//is in map
			engine->play2D(audioCollection[key].fileLocation.c_str(), Loop);
		}
		else
		{
			printf("Key not found");
		}
	}




	void Audio::addAudioFile(std::string key, std::string fileLocation, bool loop)
	{
		AudioFile newFile;
		newFile.fileLocation = fileLocation;
		newFile.loop = loop;
		newFile.id = audioCollection.size();

		audioCollection[key] = newFile;
	}

	Audio& Audio::getInstance()
	{
		return instance;
	}

	Audio::~Audio()
	{
		if (engine)
		{
			engine->drop();
		}
	}

}

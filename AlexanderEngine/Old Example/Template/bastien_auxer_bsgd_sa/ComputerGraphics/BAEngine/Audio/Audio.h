#pragma once

#ifndef ENGINE_AUDIO_H
#define ENGINE_AUDIO_H


//#include 
#include <iostream>
#include <string>
#include <Windows.h>
#include <MMSystem.h>
#include <ExportHeader.h>

#include <stdio.h>

#include <map>

// include console I/O methods (conio.h for windows, our wrapper in linux)
#if defined(WIN32)
	#include <conio.h>
#else
	#include "../common/conio.h"
#endif

namespace irrklang{

	class ISoundEngine;

}

namespace Sounds{


	class AudioFile
	{

	public:
		std::string fileLocation;
		bool loop;
		int id;
	};

	class ENGINE_SHARED Audio
	{

		static Audio instance;
		std::map<std::string, AudioFile> audioCollection;

	public: 
		irrklang::ISoundEngine* engine;
		void playFile(std::string key, bool Loop);
		void addAudioFile(std::string key, std::string fileLocation, bool loop);
		static Audio& getInstance();

		Audio();
		~Audio();
	};

	#define audio Sounds::Audio::getInstance()

}

#endif
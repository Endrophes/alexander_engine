#pragma once
#ifndef ENGINE_ENTITY_H
#define ENGINE_ENTITY_H

#include <iostream>
#include <vector>
#include <glm.hpp>
#include <ExportHeader.h>

namespace Entites
{
	class Component;
	class ENGINE_SHARED Entity
	{
		std::vector<Component*> components;
		unsigned int numComponents;
	public:

		glm::vec3 position;
		glm::vec3 rotation;
		glm::vec3 forwardVector;
		float velocity;

		bool hit;
		
		bool initialize();
		bool shutdown();
		void addComponent(Component* newComponent);
		

		glm::mat4 getRotation();

		Entity();
		void update();

		template<class T>
		T* getComponent() const;

	};

	template<class T>
	T* Entity::getComponent() const
	{
		T* result = NULL;

		for(std::vector<Component*>::size_type step = 0; step != components.size(); step++) 
		{
			result = dynamic_cast<T*>(components[step]);
			if (result != 0)
			{
				break;
			}
		}

		return result;
	}


};

#endif
#pragma once

#include <vector>
#include <ExportHeader.h>

class Render;

class MoniterBuffer
{
	unsigned int frameID;
	unsigned int colorTextureID;
	unsigned int depthTextureID;

public:
	int createFrameBuffers(int textureID, int height = 256, int width = 256);
	void createFrameBufferWithTexture(unsigned int* bufferID, unsigned int* textureID, int textureSlot, unsigned int attachment, unsigned int internalFormat, unsigned int format, unsigned int type, int height = 256, int width = 256);
	void createFrametexture(unsigned int* textureID, int textureSlot, unsigned int attachment, unsigned int internalFormat, unsigned int format, unsigned int type, int height = 256, int width = 256);
	unsigned int getFrameId();
};
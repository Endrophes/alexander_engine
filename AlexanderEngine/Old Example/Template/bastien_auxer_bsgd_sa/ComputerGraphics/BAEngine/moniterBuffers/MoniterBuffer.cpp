#include <moniterBuffers\MoniterBuffer.h>
#include <gl\glew.h>
#include <glm.hpp>
#include <iostream>

#ifdef _DEBUG
	#include <Debug Tools\Memmory Maniger\DebugMemory.h>
#endif

void MoniterBuffer::createFrametexture(unsigned int* textureID, int textureSlot, unsigned int attachment, unsigned int internalFormat, unsigned int format, unsigned int type, int height, int width)
{
	glActiveTexture(GL_TEXTURE0 + textureSlot);

	glGenTextures(1, textureID);
	glBindTexture(GL_TEXTURE_2D, *textureID);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, width, height, 0, format, type, 0);
	glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, attachment, GL_TEXTURE_2D, *textureID, 0);
}

int MoniterBuffer::createFrameBuffers(int textureID, int height, int width)
{
	glGenFramebuffers(1, &frameID);
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, frameID);

	createFrametexture(&colorTextureID, textureID, GL_COLOR_ATTACHMENT0, GL_RGBA, GL_RGBA, GL_UNSIGNED_BYTE, height, width);
	createFrametexture(&depthTextureID, (textureID + 1), GL_DEPTH_ATTACHMENT, GL_DEPTH_COMPONENT32, GL_DEPTH_COMPONENT, GL_FLOAT, height, width);
	
	//glActiveTexture(GL_TEXTURE0 + textureID);
	//
	//glGenTextures(1, &colorTextureID);
	//glBindTexture(GL_TEXTURE_2D, colorTextureID);
	//glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	//glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	//glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
	//glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, colorTextureID, 0);
	//
	//glActiveTexture(GL_TEXTURE0 + (textureID + 1));
	//
	//glGenTextures(1, &depthTextureID);
	//glBindTexture(GL_TEXTURE_2D, depthTextureID);
	//glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	//glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	//glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32, width, height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);
	//glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthTextureID, 0);
	
	unsigned int status = glCheckFramebufferStatus(GL_DRAW_FRAMEBUFFER);
	if(status != GL_FRAMEBUFFER_COMPLETE)
	{
		std::cout << "Frame buffer not complete..." << std::endl;
	}

	return textureID;
}

void MoniterBuffer::createFrameBufferWithTexture(unsigned int* bufferID, unsigned int* textureID, int textureSlot, unsigned int attachment, unsigned int internalFormat, unsigned int format, unsigned int type, int height, int width)
{
	glGenFramebuffers(1, bufferID);
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, *bufferID);

	createFrametexture(textureID, textureSlot, attachment, internalFormat, format, type, height, width);

	unsigned int status = glCheckFramebufferStatus(GL_DRAW_FRAMEBUFFER);
	if(status != GL_FRAMEBUFFER_COMPLETE)
	{
		std::cout << "Frame buffer not complete..." << std::endl;
	}
}

unsigned int MoniterBuffer::getFrameId()
{
	return frameID;
}
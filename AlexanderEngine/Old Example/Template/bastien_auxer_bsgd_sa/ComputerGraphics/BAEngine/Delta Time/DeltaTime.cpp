#include <Delta Time\DeltaTime.h>

#ifdef _DEBUG
	#include <Debug Tools\Memmory Maniger\DebugMemory.h>
#endif

void DeltaTime::initialize()
{
	QueryPerformanceFrequency(&clockFrequency);
	startTimer();
}

void DeltaTime::startTimer()
{
	QueryPerformanceCounter(&startTime);
}

void DeltaTime::newTick()
{
	QueryPerformanceCounter(&endTime);
	delta.QuadPart = endTime.QuadPart - startTime.QuadPart;
	deltaTimeValue = ((float)delta.QuadPart)/clockFrequency.QuadPart;
	startTimer();
}

float DeltaTime::getDelta()
{
	return deltaTimeValue;
}

DeltaTime::DeltaTime()
{

}

void DeltaTime::shutdown()
{
	
}

DeltaTime::~DeltaTime()
{

}

/*
connect(&myTimer, SIGNAL(timeout()), this, SLOT(myUpdate()) );
Q_OBJECT
myTimer.start(0);

private slots:
	void functions();

// MOC IT
*/

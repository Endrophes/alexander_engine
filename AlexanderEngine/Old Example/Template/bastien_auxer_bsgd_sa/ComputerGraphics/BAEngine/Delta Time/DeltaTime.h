#pragma once

#include <Qt\qtimer.h>
#include <Windows.h>

#include <ExportHeader.h>

class ENGINE_SHARED DeltaTime
{
	LARGE_INTEGER startTime;
	LARGE_INTEGER endTime;
	LARGE_INTEGER delta;
	LARGE_INTEGER clockFrequency;

	float deltaTimeValue;

public:

	void initialize();

	void newTick();

	void shutdown();

	void startTimer();

	float getDelta();

	DeltaTime();
	~DeltaTime();

};
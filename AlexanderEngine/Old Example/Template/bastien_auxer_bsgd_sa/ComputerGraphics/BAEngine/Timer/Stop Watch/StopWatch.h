#pragma once

#ifndef STOPWATCH
#define STOPWATCH

#include <ExportHeader.h>

class StopWatch{

public:
	float senseLastInterval;
	float time;
	bool timerRun;
	int frame;


	StopWatch(){
		time = 0.00f;
		frame = 1;
	}

	~StopWatch(){
	
	}
	ENGINE_SHARED float Stop();
	ENGINE_SHARED float Interval();
	ENGINE_SHARED void Start();
	ENGINE_SHARED void PauseTimer();
	ENGINE_SHARED void updateTime();
	ENGINE_SHARED void updateFrame();

};

#endif
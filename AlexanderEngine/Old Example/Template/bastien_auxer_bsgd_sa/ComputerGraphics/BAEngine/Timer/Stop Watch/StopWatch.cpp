#include <Timer\Stop Watch\StopWatch.h>

#ifdef _DEBUG
	#include <Debug Tools\Memmory Maniger\DebugMemory.h>
#endif

void StopWatch::updateTime(){
	time += 0.1f; 
}

void StopWatch::Start(){
	timerRun = true;
	time = 0.00f;
}

void StopWatch::PauseTimer(){
	if(timerRun){
		timerRun = false;
	}
	else{
		timerRun = true;
	}
}

float StopWatch::Stop(){
	timerRun = false;
	return time;
}

float StopWatch::Interval(){
	senseLastInterval = time - senseLastInterval;
	return senseLastInterval;
}

void StopWatch::updateFrame(){
	frame += 1;
}
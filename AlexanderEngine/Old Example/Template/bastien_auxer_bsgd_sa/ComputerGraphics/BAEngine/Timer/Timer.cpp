#include <windows.h>
#include <Timer\Timer.h>

#ifdef _DEBUG
	#include <Debug Tools\Memmory Maniger\DebugMemory.h>
#endif

 double Timer::LIToSecs( LARGE_INTEGER & L) {
     return ((double)L.QuadPart /(double)frequency.QuadPart) ;
 }
 
 Timer::Timer(){
     timer.start.QuadPart=0;
     timer.stop.QuadPart=0; 
     QueryPerformanceFrequency( &frequency ) ;
 }
 
 void Timer::startTimer( ) {
     QueryPerformanceCounter(&timer.start) ;
 }
 
 void Timer::stopTimer( ) {
     QueryPerformanceCounter(&timer.stop) ;
 }
 
 double Timer::lap(){
	 stopTimer();
	 double result = getElapsedTime();
	 startTimer();
	 return result;
 }
 
 double Timer::getElapsedTime() {
     LARGE_INTEGER time;
     time.QuadPart = timer.stop.QuadPart - timer.start.QuadPart;
	 double result = LIToSecs(time);
     return result;
 }
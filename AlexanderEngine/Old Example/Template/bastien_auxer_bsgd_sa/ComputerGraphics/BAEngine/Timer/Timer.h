#pragma once

#ifndef TIMER
#define TIMER

#include <ExportHeader.h>
#include <Windows.h>

 typedef struct {
     LARGE_INTEGER start;
     LARGE_INTEGER stop;
 } stopWatch;

class ENGINE_SHARED Timer{

 private:
     stopWatch timer;
     LARGE_INTEGER frequency;
     double LIToSecs( LARGE_INTEGER & L) ;
 public:
     Timer();
     void startTimer();
     void stopTimer();
	 double lap();
     double getElapsedTime();

};


#endif
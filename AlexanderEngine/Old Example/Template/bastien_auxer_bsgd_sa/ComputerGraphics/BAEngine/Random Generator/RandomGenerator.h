#pragma once

#ifndef RANDOM
#define RANDOM

#include <ExportHeader.h>
#include <glm.hpp>
#include <core.h>

using Core::RGB;
using glm::vec2;
using glm::vec3;
using glm::vec4;

class ENGINE_SHARED RandomGenerator{

public:

	int randomInt();
	double randomDouble();
	float randomFloat();

	int randomIntInRange(int min, int max);
	double randomDoubleInRange(double min, double max);
	float randomFloatInRange(float min, float max);

	vec2 randomVector2();
	vec3 randomVector3();
	vec4 randomVector4();
	
	vec2 randomVector2InRange(vec2 min, vec2 max);
	vec3 randomVector3InRange(vec3 min, vec3 max);
	vec4 randomVector4InRange(vec4 min, vec4 max);

	RGB randomColor();
	RGB randomColorInRange(int minRed, int maxRed, int minGreen, int maxGreen, int minBlue, int maxBlue);

	static RandomGenerator* getInstance();

};

	#define Random RandomGenerator::getInstance()
#endif
#pragma once

#ifndef ENGINE_COMPONENT_H
#define ENGINE_COMPONENT_H

#include <ExportHeader.h>

namespace Entites
{
	class Entity;
	class ENGINE_SHARED Component
	{
		friend class Entity;
		Entity* parent;
	public:
		Entity* getParent() const
		{
			return parent;
		}
		virtual bool initialize() { return true; }
		virtual bool shutDown() { return true; }
		virtual void update() {}

	};

};

#endif

#pragma once

#ifndef ENGINE_PARTICLESYSTEM_H
#define ENGINE_PARTICLESYSTEM_H

#include <glm.hpp>
#include <vector>
#include <Enity\Entity.h>
#include <ExportHeader.h>
#include <Particles\Particle\Particle.h>

namespace Entites
{

	class ENGINE_SHARED ParticleSystem : public Entity
	{
		int numParticles;

		float minTime;
		float maxTime;

		float maxSpeed;
		float minSpeed;

		glm::vec3 minSpread;
		glm::vec3 maxSpread;

	public:

		std::vector<Particle> particles;

		void setup(int numParticles, float maxSpeed, float minSpeed, glm::vec3 minSpread, glm::vec3 maxSpread, float minTime, float maxTime);

		void resetParticle(Particle* targetParticle);

		void update();

		ParticleSystem(void);
		~ParticleSystem(void);
	};

}

#endif
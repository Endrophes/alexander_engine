#include "ParticleSystem.h"
#include <Random Generator\RandomGenerator.h>

namespace Entites
{

	ParticleSystem::ParticleSystem(void)
	{

	}


	ParticleSystem::~ParticleSystem(void)
	{

	}

	void ParticleSystem::setup(int numParticles, float maxSpeed, float minSpeed, glm::vec3 minSpread, glm::vec3 maxSpread, float minTime, float maxTime)
	{
		this->numParticles = numParticles;
		this->maxSpeed = maxSpeed;
		this->minSpeed = minSpeed;
		this->minSpread = minSpread;
		this->maxSpread = maxSpread;
		this->minTime = minTime;
		this->maxTime = maxTime;

		for(int step = 0; step < numParticles; step++)
		{
			resetParticle(&particles[step]);
		}

	}

	void ParticleSystem::resetParticle(Particle* targetParticle)
	{
		glm::vec3 direction = Random->randomVector3InRange(minSpread, maxSpread);
		int lifeTime = Random->randomFloatInRange(minTime, maxTime);
		float speed = Random->randomFloatInRange(minSpeed, maxSpeed);
		targetParticle->reset(direction,lifeTime, this->position, speed);
	}

	void ParticleSystem::update()
	{
		for(int step = 0; step < numParticles; step++)
		{
			Particle* currentParticle = &particles[step];
			if(currentParticle->isAlive())
			{
				currentParticle->update();
			}
			else
			{
				resetParticle(&particles[step]);
			}
		}
	}

}
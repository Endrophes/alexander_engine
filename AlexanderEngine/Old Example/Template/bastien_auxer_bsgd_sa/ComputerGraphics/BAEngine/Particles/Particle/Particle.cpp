#include "Particle.h"

namespace Entites
{

	Particle::Particle(void)
	{
		alive = true;
		lifeTime = 0.0f;
		speed = 0.0f;
		direction = glm::vec3();
	}

	Particle::~Particle(void)
	{

	}

	void Particle::setData(Render::Renderable* renderable, glm::vec3 direction, glm::vec3 position, float lifeTime, float speed)
	{
		this->renderable = renderable;
		this->direction = direction;
		this->lifeTime = lifeTime;
		this->speed = speed;
	}

	void Particle::update()
	{
		if(lifeTime <= 0)
		{
			alive = false;
		}
		else
		{
			lifeTime -= 0.1f;
		}

		position += speed * direction;
		renderable->Position = position;
	}

	void Particle::reset(glm::vec3 direction, int lifeTime, glm::vec3 position, float speed)
	{
		this->alive = true;
		this->direction = direction;
		this->lifeTime = lifeTime;
		this->position = position;
		this->speed = speed;
		renderable->Position = position;
	}

	bool Particle::isAlive()
	{
		return alive;
	}

	Render::Renderable* Particle::getRenderable()
	{
		return renderable;
	}

}
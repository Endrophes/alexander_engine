#pragma once

#ifndef ENGINE_PARTICLE_H
#define ENGINE_PARTICLE_H

#include <glm.hpp>
#include <Enity\Entity.h>
#include <ExportHeader.h>
#include <Render\Render.h>


namespace Entites
{

	class ENGINE_SHARED Particle : public Entity
	{
		bool alive;
		float lifeTime;
		float speed;
		glm::vec3 direction;
		Render::Renderable* renderable;

	public:
		Particle(void);
		~Particle(void);

		void setData(Render::Renderable* renderable, glm::vec3 direction, glm::vec3 position, float lifeTime, float speed);

		void update();

		void reset(glm::vec3 direction, int lifeTime, glm::vec3 position, float speed);

		Render::Renderable* getRenderable();

		bool isAlive();
	};

}

#endif
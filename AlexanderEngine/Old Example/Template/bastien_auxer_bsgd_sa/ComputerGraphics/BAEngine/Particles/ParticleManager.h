#pragma once

#ifndef ENGINE_PARTICLEMANAGER_H
#define ENGINE_PARTICLEMANAGER_H

#include <ExportHeader.h>

namespace Entites
{

	class ENGINE_SHARED ParticleManager
	{
	public:
		ParticleManager(void);
		~ParticleManager(void);
	};

}

#endif
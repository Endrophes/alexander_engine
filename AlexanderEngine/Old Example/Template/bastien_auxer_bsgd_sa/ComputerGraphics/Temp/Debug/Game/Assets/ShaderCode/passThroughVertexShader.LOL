#version 400
layout(location=0) in vec3 position;
layout(location=1) in vec3 normal;
layout(location=2) in vec2 uv;

uniform mat4 projectionMatrix;
uniform mat4 translation;
uniform mat4 rotation;
uniform mat4 view;

uniform float scale;

out vec4 theColor;

void main()
{
	mat4 transformation = translation * rotation;
	vec4 scaledPosition = vec4(scale * position, 1);
	vec4 finalPosition = transformation * scaledPosition;
	gl_Position = projectionMatrix * (view * finalPosition);
	theColor = vec4(position,1);
}